// 최근 본 목록 옵션 페이지 분류에 따른 위치 변경 변수
var page = 1;

/**
 * 대분류 category 선택 후 카테고리 리스트 중분류 소분류 hide, show delay 기능 이용 클릭하면 해당 카테고리 글씨 색
 * 변하기 및 다시 되돌리기
 */
var hideClass;
var subClass;
var chageFontColor;
var red;
var before;
var cateClass;

function dropDown(menu) {
	var i = document.getElementById(menu.id);

	if (i.textContent == "▼") {
		if (cateClass == null) {
			cateClass = $(".category_List_subject_child");
		}
		if (chageFontColor != null) {
			before.textContent = "▼";
			label.style = "color:black";

			chageFontColor = null;
			red = null;
			before = null;
		}

		before = i;
		chageFontColor = i;
		label = chageFontColor.parentNode.childNodes[1].firstChild;

		red = i.parentNode.childNodes[1].firstChild;
		red.style = "color:red";

		i.textContent = "▲";

		cateClass.hide("200");

		$("." + menu.id).show(200);
	} else {
		red.style = "color:black";
		i.textContent = "▼";
		$("." + menu.id).hide(200);
	}
}

// //////////////////////////////////////////////////////////////////////////////

$(function() {
	var search = 600;
	if(page==3){
		search=0;
	}
	$(".footer").css("top",$(".category_contents_pruduct").height()-search+"px")
	
	$(".category_List_subject_child").on("click", function() {
		var lcode = $(this).children(".lscode").val()
		var mcode = $(this).children(".mscode").val()
		var scode = $(this).children(".sscode").val()
		var scode = $(this).children(".sscode").val()
		$("#lfilter").val(lcode);
		$("#mfilter").val(mcode);
		$("#sfilter").val(scode);
		$("#scntnum").val(0);
		
		ajaxlist(lcode, mcode, scode);

	});

	$("#regdate").on("click", function() {
		var lcode = $("#lfilter").val();
		var mcode = $("#mfilter").val();
		var scode = $("#sfilter").val();
		var scnt = 1;
		$("#scntnum").val(1);
		ajaxlist(lcode, mcode, scode,scnt);
	});

	$("#scnt").on("click", function() {
		var lcode = $("#lfilter").val();
		var mcode = $("#mfilter").val();
		var scode = $("#sfilter").val();
		var scnt = 2;
		$("#scntnum").val(2);
		ajaxlist(lcode, mcode, scode,scnt);
	});

	$("#lowprice").on("click", function() {
		var lcode = $("#lfilter").val();
		var mcode = $("#mfilter").val();
		var scode = $("#sfilter").val();
		var scnt = 3;
		$("#scntnum").val(3);
		ajaxlist(lcode, mcode, scode,scnt);
	});

	$("#hiprice").on("click", function() {
		var lcode = $("#lfilter").val();
		var mcode = $("#mfilter").val();
		var scode = $("#sfilter").val();
		var scnt = 4;
		$("#scntnum").val(4);
		ajaxlist(lcode, mcode, scode,scnt);
	});
	$("#star").on("click", function() {
		var lcode = $("#lfilter").val();
		var mcode = $("#mfilter").val();
		var scode = $("#sfilter").val();
		var scnt = 5;
		$("#scntnum").val(5);
		ajaxlist(lcode, mcode, scode,scnt);
	});
});



function ajaxlist(lcode, mcode, scode, scnt) {
	$("#pruduct_list_ajax").empty();
	$("#countbox").empty();
	$("#pageDiv").empty();
	
	$(".footer").css("top",$(".category_contents_pruduct").height()+"px")
	
	console.log(lcode);
	console.log(mcode);
	console.log(scode);
	var mainsearch = $("#mainsearch").val();
	var currentPage = $("#currentPage").val();
	var listajax = "";
	var countbox = ""
	var pageDiv = "";
	if ($("#mainsearch").val() == "") {
		var dataval = "currentPage=" + currentPage +"&lcode=" + lcode +"&scnt=" + $("#scntnum").val()+ "&mcode=" + mcode + "&scode=" + scode;
	}else {
		
		var dataval = "currentPage=" + currentPage +"&pname=" + mainsearch+"&scnt=" + scnt;
	}
					
	$.ajax({
		url : "lcodeserch.do",
		type : "get",
		data : dataval,
		dataType : "json",
		success : function(data) {
			
			if (data.count == 0) {
				listajax += "<h3> 현재 상품이 없습니다. </h3>";
			}else {
				
			data.list.forEach(function(list) {
				listajax += "<div class='list'>";
				listajax += "<a href='view.do?no=" + list.pid + "'>";
				listajax += "<img src='" + list.path + "' alt='' width=100px; height='150px'/>";
				listajax += " </a>";
				listajax += "<a href='view.do?no=" + list.pid + "'> ";
				listajax += "	<span>" + list.factory + "</span> ";
				listajax += "</a> ";
				listajax += "<a href='view.do?no=" + list.pid + "'> ";
				listajax += "<span>" + list.pname + "</span> ";
				listajax += "</a> ";
				listajax += "<a href='view.do?no=" + list.pid + "'> ";
						if (list.sale == 0) {
							listajax += "<span>" + list.price + "원</span>";
						}else {
							listajax += "<span style='text-decoration:line-through;'>"+list.price +" 원</span>";
							listajax += "<span> "+list.sprice  +"원 ("+list.sale +"% DC ) </span>";
						}
				listajax += "</a> ";
				listajax += " </div>";
			});
			}
			
			for (var i = data.beginpagenum; i <= data.endpagenum; i++) {
				if (i == currentPage) {
					pageDiv += " <a href='javascript:;' onclick='pagechange("+i+");' style='color: red'>"+i+"</a> ";
				}
				if (i != currentPage) {
					pageDiv += " <a href='javascript:;' onclick='pagechange("+i+");'>"+i+"</a> ";
				}
			}
			countbox += data.count + "";
			$("#pruduct_list_ajax").append(listajax);
			$("#category_contents_navy").append();
			$("#countbox").html(countbox);
			$("#pageDiv").append(pageDiv);
		},
		error : function(jqXHR, textStatus, errorThrown) {
			alert("에러 발생~~ \n" + textStatus + " : " + errorThrown);
		}
	});
}

function pagechange(cu) {
	$("#currentPage").val(cu) ;
	var lcode = $("#lfilter").val();
	var mcode = $("#mfilter").val();
	var scode = $("#sfilter").val();
	
	var scnt = $("#scntnum").val();
	
	ajaxlist(lcode, mcode, scode,scnt);
}