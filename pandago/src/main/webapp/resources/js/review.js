$(function () {
	$( ".star_rating a" ).click(function() {
	     $(this).parent().children("a").removeClass("on");
	     $(this).addClass("on").prevAll("a").addClass("on");
	     $(this).parent(".star_rating").children("input").val($(this).children("input").val());
	     return false;
	});
})
	
function reviewWrite(payid) {
	$("#" + payid + "form").css("display", "");
}

function reviewWriteOk(payid, pid) {
	var content = $("#" + payid + "text").val();
	var title = $("#" + payid + "title").val();

	location.href = "myreviewinsert.do?payid=" + payid + "&pid=" + pid
			+ "&content=" + encodeURI(encodeURIComponent(content))
			+ "&title=" + encodeURI(encodeURIComponent(title))
			+"&star="+$("#"+payid+"star").val();
}

function reviewWritecancle(payid, pid) {
	$("#" + payid + "text").val("");
	$("#" + payid + "title").val("");
	$("#" + payid + "form").css("display", "none");	
}	

function reviewdelete(payid){
	location.href = "myreviewdelete.do?payid="+payid;
}


function tab(type) {
	var activity = document.getElementsByClassName("activity");
	var type = document.getElementsByClassName(type);
	
	for (var i = 0; i < activity.length; i++) {
		activity[i].classList.remove("activity");
	}
		type[0].classList.add("activity");
}