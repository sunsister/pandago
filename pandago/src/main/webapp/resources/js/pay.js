$(function() {
	var ostate = "결제완료"; // 주문처리상태
	$("#div_card").show();
	$("#div_cash").hide();

	// 총합계
	var size = $(".dev_order").length;
	// dev_item_totalprice dev_all_price
	var sum = 0;
	for (var i = 0; i < size; i++) {
		sum += Number($(".dev_order:eq(" + i + ")").find(".dev_item_totalprice").html());
	}
	
	$("#dev_all_price").html("총 합계 :" + sum);
	console.log("전체합계2 : " + sum);
	$("#buyprice").html(sum);
	
	console.log(sum);
	var dprice= 0;
	
	var dp = parseInt(sum);
	console.log(dp);
	if(dp < 50000){
		$("#ddprice").html(3500);
	}
	dprice=$("#ddprice").html();
	var dpp = parseInt(dprice);
	$("#totalprice").html(dp+dpp);
	

	// dev_item_totalprice
	$(".coupon").on("click", function() {
		var cpprice = Number($(this).parent().children(".cpprice").val());
		var buyprice = $("#buyprice").html();
		var usedpoint = Number($("#usedpoint").val());
		var ddprice=Number($("#ddprice").html());
		console.log("buyprice : " + buyprice);
		console.log("cpprice : " + cpprice);
		console.log(usedpoint);
		// 현재 할인 가격
		$("#discount").html(cpprice);
		var discount = $("#discount").html();

		// 전체 합계
		$("#totalprice").html(buyprice - discount-usedpoint+ddprice);

	});

	$("#usedpoint").keyup(function() {
		var cpprice = Number($(".coupon:checked").parent().children(".cpprice").val());
		var buyprice = $("#buyprice").html();
		var usedpoint = Number($("#usedpoint").val());
		var point = Number($("#point").val());
		var ddprice=Number($("#ddprice").html());
		var dprice=	Number($("#ddprice").html());
		
		if (usedpoint > point) {
			$("#usedpoint").val(point);
			alert("최대 " + point + "P 사용가능합니다.");
		} else if (usedpoint < 0) {
			$("#usedpoint").val(0);
			alert("최소 0P 사용가능합니다.");
		}
		console.log(usedpoint);
		// 현재 할인 가격
		$("#cpoint").html(usedpoint);
		var discount = $("#cpoint").html();
		
		// 전체 합계
		$("#totalprice").html(buyprice - usedpoint+dprice-cpprice);
	});

	$(".pmethod").on("click", function() {
		var pmethod = $(this).val();
		if (pmethod == "신용체크카드") {
			$("#div_card").show();
			$("#div_cash").hide();
			ostate = "결제완료";
		} else if (pmethod == "현금") {
			$("#div_card").hide();
			$("#div_cash").show();
			ostate = "입금대기중";
		}
	});

	$("#btn_pay").on("click", function() {
		console.log("결제하기");

		var order = new Array();
		var pprice = $("#totalprice").html();
		var data = "";
		data += "ostate=" + ostate; // 주문처리상태
		data += "&pprice=" + pprice;
		data += "&pmethod=" + $(".pmethod:checked").val();
		data += "&dprice=" + dprice;
		var mcid = $(".coupon:checked").val();
		if (mcid != 'notused') {
			var cpid = $(".coupon:checked").parent().children(".cpid").val();
			data += "&cpid=" + cpid;
			data += "&mcid=" + mcid;;
		}

		var point = $("#usedpoint").val();
		if (point != 0) {
			data += "&point=" + point;
		}

		for (var i = 0; i < $(".dev_order").length; i++) {
			var ddata = new Array();
			var pdid = $(".dev_order:eq(" + i + ")").find(".dev_pdid").val();
			var pid = $(".dev_order:eq(" + i + ")").find(".dev_pid").val();
			var cnt = $(".dev_order:eq(" + i + ")").find(".dev_cnt")	.html();
			var iprice = $(".dev_order:eq(" + i + ")").find(".dev_iprice").html();
			ddata.push(pdid);
			ddata.push(pid);
			ddata.push(cnt);
			ddata.push(iprice);
			order.push(ddata);
		}
		data += "&order=" + order;
		location.href = "/pandago/pay.do?action=step2&" + data;
	});

	$("#btn_cancle").on("click", function() {
		console.log("취소하기");
		location.href = document.referrer;
	});

});