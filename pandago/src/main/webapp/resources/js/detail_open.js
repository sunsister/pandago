var detail;
var area;
var body;
var btnFlag;

window.onload = function() {
	body = document.body;
	body.onscroll = scrollEvent
	document.getElementById("xbtn").onclick = close;	
	document.getElementById("detail_p").onclick = move;
}

function close(){
	btnFlag=false;
	this.parentNode.classList.add("hide");
}


function move(){
	if(btnFlag){
		alert("test");
	}
}

function scrollEvent(e){
	//console.dir(e);
	//console.log(e.target.body.scrollTop);
}

// open detail DIV location margin-left value setting
function open_detail(obj) {
	btnFlag=true;
	area = document.getElementsByClassName("area");
	detail = document.getElementById("detail_p");
	detail.className = "";
	
	var openBodyWidth = body.clientWidth;

	if(openBodyWidth>1000) openBodyWidth = (openBodyWidth-detail.clientWidth)/2;	
	else openBodyWidth = 0;	
	
	detail.style.left = openBodyWidth + "px"
	detail.style.top = area[0].offsetTop + "px";
	
	// [0] input [1] image [2] span [3] label user id
	var elements = obj.parentNode.parentNode.children;

	
	var copy_elements = detail_p.children[1].children;
	console.log(elements);
	copy_elements[0].value=elements[0].value;
	copy_elements[1].setAttribute('src',elements[1].src);
	copy_elements[2].innerText = elements[2].children[0].innerText;
	copy_elements[3].textContent = elements[3].textContent;
}

// window resize - detail DIV : margin-left value setting
window.onresize = function(event){
	if(detail!=null){
		var resize_left = event.target.innerWidth;	
		
		if(resize_left > 1000){
			resize_left = (resize_left-detail.clientWidth)/2;
		}else {
			resize_left = 0;
		}
		detail.style.left = resize_left+"px";
	}
}
