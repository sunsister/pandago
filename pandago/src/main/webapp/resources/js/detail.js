var buyFlag;
var page = 2;

$(function() {
	$("#pdoption").on("change", price);
	$("#pdcount").on("change", price);
	$("#cartatag").on("click", option);
}); // ready function end

function buy(cid){
	console.log(cid);
	if (cid!="") {
		buyFlag=true;
		option();
		
	}else {
		alert("로그인을 하셔야합니다.");
		location.href="login.do";
	}
}

function pwClose(id){
	$("#"+id).css("display","none");
}

function option() {
	if ($("#pdoption option:selected").val()!= null) {	
		var data = $("#pdoption option:selected").val().split(",");
		var cnt = $("#pdcount option:selected").val()
		var pdid = data[0];
	}else {
		var cnt = $("#pdcount option:selected").val();
		var pdid = $("#cntpdid").val();	
	}

	if (pdid == null || pdid == 0) {
		alert("옵션을 선택해주세요");
	} else {
		if(buyFlag){document.order.submit(); return;}
		$.ajax({
			url : "cartin.do",
			type : "get",
			data : "pid=" + $("#pid").val() + "&pdid=" + pdid+ "&scnt=" + cnt,
			success : function(data) {
				if (data.id != undefined) {
					//console.log(data.id)
					var result = confirm('선택하신 상품을 장바구니에 담았습니다. 장바구니로 이동하시겠습니까?');
						if (result) {
							location.href = "cart.do";
							console.log("확인");
						} else {
							console.log("취소");
						}
				} else {
					var login = confirm('로그인이 필요합니다. 로그인 하시겠습니까?');
					if (login) {
						location.href = "login";
					}
				}
			}, // success function end
				error : function(jqXHR, textStatus, errorThrown) {
					alert("에러 발생~~ \n" + textStatus + " : " + errorThrown);
				}// error function end
		});// ajax function end
	}// else end
}// option function end

function price() {
	$(".price").empty();
	var price = "";
	if ($("#pdoption option:selected").val() == 0 || $("#pdcount option:selected").val()==0) {
		price = "<span> 총 상품 금액 : </span>";
		price += "<h2> 0 </h2>";
		price += "<span>원</span>";
	} else {
		if ($("#pdoption option:selected").val() != null) {

			console.dir($("#pdoption option:selected"))
			var data = $("#pdoption option:selected").val().split(",");
			var pdid = data[0];
			var op = data[1];
			var addprice = data[2];
			var opprice = $("#price").val();
			var pdcount = $("#pdcount option:selected").val();
		} else {
			var opprice = $("#price").val();
			var pdid = $("#cntpdid").val();
			var pdcount = $("#pdcount").val();
			var addprice = 0;
		}		
		var total = "(" + opprice + "+" + addprice + ")*" + pdcount;
		// console.log(total);
		// console.log(eval(total));
		price = "<span> 총 상품 금액 : </span>"
		price += "<h2>" + eval(total) + "</h2>"
		price += "<span>원</span>"
	}
	$(".price").append(price);

}
$(function() {	
	$('input:radio[name="pw"]').on("change",function(){	
		if ($('input:radio[name="pw"]:checked').val()==1) {		
			 $("#pwwrite").css("display","block");
		}else if ($('input:radio[name="pw"]:checked').val()==0) {
			 $("#pwwrite").css("display","none");
			 $("#pwwriteinput").val(null);
		}	
	});
	
$("#isinsert").on("click",function(){
	var listajax="";
	$.ajax({
		url : "isinsert.do",
		type : "POST",
		data : "title="+$("#titlepQna").val()+"&content="+$("#contentpQna").val()+"&pid="+$("#pid").val()+"&pwwriteinput="+$("#pwwriteinput").val()+"&insecret="+$('input:radio[name="pw"]:checked').val(),
		datatype : "json",
		success : function(data) {
			if (data.cid==null) {
				alert("로그인을 해야 작성이 가능합니다.")
			}else{
				$("#inquirytable").empty();
				listajax +="<table class='p_Qna_board'>";
				listajax +="<tr>";
				listajax +="<th style='width: 200px;'>고객ID</th>";
				listajax +="<th id='title123' style='width: 200px;'>제목</th>";
				listajax +="<th style='width: 320px;' >내용</th>";
				listajax +="<th style='width: 150px;'>작성일</th>";
				listajax +="<th style='width: 70px;'></th>";
				listajax +="</tr>";
			
				data.list.forEach(function(islist) {
				
				var inid ="'"+islist.inid+"'";
				var inpw ="'"+islist.inpw+"'";
				var incontent ="'"+islist.incontent+"'";
				var inaok ="'"+islist.inaok+"'";
				var islistcid ="'"+islist.cid+"'";
				var datacid ="'"+data.cid+"'";
				
				listajax +="<tr>";
				//listajax +="<td>"+islist.rn+"</td>";
				//listajax +="<td>"+islist.inid+"</td>";
				//listajax +="<td>"+islist.pid+"</td>";
				listajax +="<td>"+islist.cid+"</td>";
				listajax +="<td>"+islist.intitle+"</td>";
			
			
				if (islist.inpw != null) {	
				
					listajax +="<td ><p id="+islist.inid+"con>";
					listajax +="<input type='password' placeholder='Password & Enter'  style='width: 90%;' id='"+islist.inid+"pwinid' onkeypress=pwcheck("+inid+","+inpw+","+incontent+","+inaok+");";
					listajax +=" /> </p></td>";
				}
				
				if (islist.inpw == null) {
					if (islist.incontent!=null) {
						
						listajax +="<td>"+islist.incontent+"</td>";
					}else {
						
						listajax +="<td> 내용이 없습니다. </td>";
					}
					
				}
				
				listajax +="<td>"+islist.indate+"</td>";
				
				if (islistcid==datacid) {
					listajax +="<td><a href='javascript:;' onclick=deleteis("+inid+","+islist.pid+"); > 삭제하기 </a></td>";
				}
				
				listajax +="</tr>";
					
				listajax +="<tr id="+islist.inid +"acon style='display:none'>";
				listajax +="<td></td>";
				listajax +="<td></td>";
				listajax +="<td>└담당자</td><td></td>";						
				listajax +="<td colspan='2'>"+islist.inacontent+"</td>";							
				listajax +="</tr>";
				
				console.log();
				console.log();
				if (islist.inpw == 'null' || islist.inaok==1) {		
					listajax +="<tr>";
					listajax +="<td></td>";
					listajax +="<td></td>";
					listajax +="<td>└담당자</td><td></td>";						
					listajax +="<td colspan='2'>"+islist.inacontent+"</td>";							
					listajax +="</tr>";
				}		
			});
				listajax +="</table>";
				$("#inquirytable").append(listajax);
				$("#titlepQna").val("");
				$("#contentpQna").val("");
				$("input:radio[name='pw']:radio[value='0']").prop("checked", true);
				
				if ($('input:radio[name="pw"]:checked').val()==1) {	
					 $("#pwwrite").css("display","");
				}else if ($('input:radio[name="pw"]:checked').val()==0) {
					 $("#pwwrite").css("display","none");
					 $("#pwwriteinput").val(null);
				}
			}
		},
		error : function(jqXHR, textStatus, errorThrown) {
			alert("에러 발생~~ \n" + textStatus + " : " + errorThrown);
		}
		}) ;
	});
});

function deleteis(inid,pid) {
	var listajax="";
	
	$.ajax({
		url : "iniddelete.do",
		type : "get",
		data : "inid="+inid+"&pid="+pid,
		datatype : "json",
		success : function(data) {
			if (data.cid==null) {
				alert("로그인을 해야 작성이 가능합니다.")
			}else{
				$("#inquirytable").empty();
				listajax +="<table class='p_Qna_board'>";
				listajax +="<tr>";
				listajax +="<th style='width: 200px;'>고객ID</th>";
				listajax +="<th id='title123' style='width: 200px;'>제목</th>";
				listajax +="<th style='width: 320px;' >내용</th>";
				listajax +="<th style='width: 150px;'>작성일</th>";
				listajax +="<th style='width: 70px;'></th>";
				listajax +="</tr>";
			
				data.list.forEach(function(islist) {
				
				var inid ="'"+islist.inid+"'";
				var inpw ="'"+islist.inpw+"'";
				var incontent ="'"+islist.incontent+"'";
				var inaok ="'"+islist.inaok+"'";
				var islistcid ="'"+islist.cid+"'";
				var datacid ="'"+data.cid+"'";
				
				listajax +="<tr>";
				//listajax +="<td>"+islist.rn+"</td>";
				//listajax +="<td>"+islist.inid+"</td>";
				//listajax +="<td>"+islist.pid+"</td>";
				listajax +="<td>"+islist.cid+"</td>";
				listajax +="<td>"+islist.intitle+"</td>";
			
			
				if (islist.inpw != null) {	
				
					listajax +="<td ><p id="+islist.inid+"con>";
					listajax +="<input type='password' placeholder='Password & Enter' style='width: 90%;' id='"+islist.inid+"pwinid' onkeypress=pwcheck("+inid+","+inpw+","+incontent+","+inaok+");";
					listajax +=" /> </p></td>";
				}
				
				if (islist.inpw == null) {
					if (islist.incontent!=null) {
						
						listajax +="<td>"+islist.incontent+"</td>";
					}else {
						
						listajax +="<td> 내용이 없습니다. </td>";
					}
					
				}
				
				listajax +="<td>"+islist.indate+"</td>";
				
				if (islistcid==datacid) {
					listajax +="<td><a href='javascript:;' onclick=deleteis("+inid+","+islist.pid+"); > 삭제하기 </a></td>";
				}
				
				listajax +="</tr>";
					
				listajax +="<tr id="+islist.inid +"acon style='display:none'>";
				listajax +="<td></td>";
				listajax +="<td></td>";
				listajax +="<td>└담당자</td><td></td>";						
				listajax +="<td colspan='2'>"+islist.inacontent+"</td>";							
				listajax +="</tr>";
				
				console.log();
				console.log();
				if (islist.inpw == 'null' || islist.inaok==1) {		
					listajax +="<tr>";
					listajax +="<td></td>";
					listajax +="<td></td>";
					listajax +="<td>└담당자</td><td></td>";						
					listajax +="<td colspan='2'>"+islist.inacontent+"</td>";							
					listajax +="</tr>";
				}		
			});
				listajax +="</table>";
				$("#inquirytable").append(listajax);
				$("#titlepQna").val("");
				$("#contentpQna").val("");
				$("input:radio[name='pw']:radio[value='0']").prop("checked", true);
				
				if ($('input:radio[name="pw"]:checked').val()==1) {	
					 $("#pwwrite").css("display","");
				}else if ($('input:radio[name="pw"]:checked').val()==0) {
					 $("#pwwrite").css("display","none");
					 $("#pwwriteinput").val(null);
				}
			}
		},
		error : function(jqXHR, textStatus, errorThrown) {
			alert("에러 발생~~ \n" + textStatus + " : " + errorThrown);
		}
	}); 
}

function pwcheck(inid,pw,cont,inok) {
	e =  window.event;
	console.log(e);
	console.log($("#"+inid+"pwinid").val());
		if (e.keyCode==13) {
			if (pw == $("#"+inid+"pwinid").val() ) {
				
				$("#"+inid+"con").empty();
				$("#"+inid+"con").html(cont);
				if (inok=='1') {	
					$("#"+inid+"acon").css("display","");
				}
				}else{
					alert("비밀번호가 틀렸습니다.");
					$("#"+inid+"pwinid").val("");
				}		
		}
}

function hide(){
	$(".view").css("display","none");
}
function show(){
	$(".view").css("display","inline-block");
}

function tab(type){
	var add_Value=0;
	if (type=="typeOne") {
		tabChangeColor("lightgray","white","white")	
		$(".footer").css("top",$(".dtinfiimg").height()+"px");
		add_Value=0;
	}else if (type=="typeTwo") {
		tabChangeColor("white","lightgray","white")
		add_Value = 200;
	}else if(type=="typeThree"){
		tabChangeColor("white","white","lightgray")
		add_Value = 200;
	}
	if(add_Value!=0){		
		//outerHeight
		//category_contents
		$(".footer").css("top",$(".area").offset().top+$("."+type).height()-add_Value+"px");
	}

	var activity = document.getElementsByClassName("activity");
	var type = document.getElementsByClassName(type);
	
	for(var i=0; i<activity.length; i++){
		activity[i].classList.remove("activity");		
	}
	type[0].classList.add("activity");	
		
}

function tabChangeColor(col1,col2,col3){
	$(".type1").css("background-color",col1);
	$(".type2").css("background-color",col2);
	$(".type3").css("background-color",col3);
}

function changeimg(imgpath) {
	$("#mainimg").attr("src",imgpath);	
}
