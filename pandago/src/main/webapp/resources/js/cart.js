$(function() {

	//# check all button
	var checkFlag;
	var value;
	
	// Check box initialize = true
	$("input[name=cartcb]").prop("checked", true);
	
	// total price value
	totalprice();

	// Check delete
	$(document).on("click", "#sdelete", function() {
		var really = confirm('선택한 장바구니 상품을 삭제하겠습니까?');
		if(really){
			var deleteno = new Array();
			$("input[name=cartcb]:checked").each(function() {
				deleteno.push($(this).val());
			});
			location.href = "cartdelete.do?deleteno=" + deleteno;
		}
	});

	//# check all button
	$(document).on("click", "#checkall", function() {
			if ($("#checkallv").val() == 0) {
				checkFlag = true;
				value = 1;
			} else {
				checkFlag = false;
				value = 0;
			}	
			
			$("input[name=cartcb]").prop("checked", checkFlag);
			$("#checkallv").val(value);
			
			totalprice();
	});
	
	// Check Box : change State - Change Total Price
	$("input[name=cartcb]").on("click", totalprice);
	
	// 결제하기
	$("#spay").on("click", function(){
			var cartlist = new Array();
			var len = $(".dev_cartlist").length;
			console.log(len);
			
	/*		var item = new Object();
			item.action = 'step1cart';
			cartlist.push(item);*/
			
			$(".dev_cartlist").each(function(index){
				var item = new Object();
				console.log("----->");
				console.log($(".dev_cartlist:eq("+index+")").find("input[name=cartcb]").prop("checked"));
				if ($(".dev_cartlist:eq("+index+")").find("input[name=cartcb]").prop("checked")) {
					
				item.cartid = $(".dev_cartlist:eq("+index+")").find(".dev_cartid").val();
				item.pid = $(".dev_cartlist:eq("+index+")").find(".dev_pid").val();
				item.cnt = $(".dev_cartlist:eq("+index+")").find(".dev_cnt").val();
				item.dimg= $(".dev_cartlist:eq("+index+")").find(".dev_dimg").val();
				item.sprice= $(".dev_cartlist:eq("+index+")").find(".dev_sprice").val();
				item.psize =  $(".dev_cartlist:eq("+index+")").find(".dev_psize").val();
				item.color=  $(".dev_cartlist:eq("+index+")").find(".dev_color").val();
				item.pname= encodeURI(encodeURIComponent($(".dev_cartlist:eq("+index+")").find(".dev_pname").val()));
				item.pdid = $(".dev_cartlist:eq("+index+")").find(".dev_pdid").val();
				cartlist.push(item);
				}
			});
			
			var str = JSON.stringify(cartlist);
			//str=encodeURI(encodeURIComponent(str));
			console.log(str);
			//location.href ="/pandago/pay.do?cartlist="+str;
			var frm = document.createElement("form");
			frm.setAttribute("method","post");                    
			frm.setAttribute("action","/pandago/pay.do"); 
			frm.setAttribute("accept-charset","UTF-8");
			
			document.body.appendChild(frm); 
			
			var input_id = document.createElement("input");  
			input_id.setAttribute("type", "hidden");                 
			input_id.setAttribute("name", "action");                        
			input_id.setAttribute("value", "step1cart");
			frm.appendChild(input_id);
			
			input_id = document.createElement("input");  
			input_id.setAttribute("type", "hidden");                 
			input_id.setAttribute("name", "cartlist");                        
			input_id.setAttribute("value", str);
			frm.appendChild(input_id);
			
			frm.submit();
	//		$.post("/pandago/pay.do", "action=step1cart&cartlist="+str, function(){
	//			console.log("post");
	//		});

		});
});

function totalprice() {
	var list = new Array();
	var pricelist = new Array();
	var price = "";

	$("input[name=cartcb]:checked").each(function() {
		list.push($(this).val())
	});

	for (var i = 0; i < list.length; i++) {
		pricelist.push($("#" + list[i] + "tp").val());
		if (i == 0) {
			price += $("#" + list[i] + "tp").val();
		} else {
			price += "+" + $("#" + list[i] + "tp").val();   
		}
	}

	if (eval(price) == undefined) {
		$("#total").html("<h1>총 결제 금액 : 0 원</h1>");
	} else {
		$("#total").html("<h1>총 결제 금액 : " + eval(price) + " 원</h1>");
		$("#payprice").val(eval(price));
	}
}// function total price end

// UI Change - Product quantity
function cntchange(cnt, cartid, sprice, tp, tprice) {
	var cnt = $(cnt).val();
	var cartid = cartid;
	var sprice = sprice;

	if (cnt != "") {
		$("#" + tp).val(eval(sprice + "*" + cnt));
		$("#" + tprice).html(eval(sprice + "*" + cnt) + "원");
		totalprice();
	}
}// function change end
