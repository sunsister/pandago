var imageSize=0; 	// imageSize
var current=0; 		// main Slider Image [장]
var max=0;			// global variable
var container; 		// 메인 화면 slider에서 움직일 UL 

$(function() {
	// 이미지 사이즈는 변경될 수 있으나 그 경우 슬라이더 움직이는 값을 수정해야한다.
	imageSize=1920;
	
	$(".main_slider_img").css("width",imageSize+"px");
	
	container=$(".main_Contents_img_slider ul");
	
	max = container.children().length;
	
	container.css("width",imageSize*max+"px");
	
	$("button.prev").on("click",prev);
	$("button.next").on("click",next);
	
	setInterval(function(){next();},4000);
	
})

function prev( ){ current--; if(current < 0) current=max-1; animate(); }
function next( ){ 	current++; if ( current > max-1) current =0; animate();}
function animate(){ var moveX = current * imageSize; container.animate({"margin-left" : "-"+moveX}, 'faster');}