$(function() {
	
	$(document).on("click", "#btnJoin", okJoin);
	$(document).on("click", "#validate", valiDate);
	$(document).on("click", "#btnEmailSend", emailSend);
	

});

// 아이디 중복
function valiDate() {
	console.log("아이디 중복 클릭");
	var email = $("#txtemail").val();
	
	var pattern = /[a-zA-Z0-9_\-\.]+@[a-zA-Z0-9_\-]+\.[a-zA-Z0-9]{2,}/;
	var result = email.match(pattern);
	if(!result){
		alert("올바른 이메일 형식이 아닙니다");		
	}// result if end
	
	if (email != "") {
		$.ajax({
			url : 'valiDate',
			type : 'post',
			data : "email=" + email,
			dataType : "text",
			success : function(data) {
				if (data.trim() == 'false') {
					alert("중복된 이메일")
				} else if (data.trim() == 'true') {
					alert("사용가능한 이메일")
				}// else if end
			}// success end
		})// ajax end
	}// if end
}

function emailSend() {
	console.log("이메일 인증") 
	var email = $("#txtemail").val();
	if (email != "") {
		$.ajax({
			url : 'sendEmailKey',
			type : 'post',
			data : "email=" + email,
			dataType : "text",
			success : function(email) {
				
					$(".divEmailBtn").remove();
					$("#appendTd").append(
							"<div class='divInputCode'>"
							+ "<input type='text' name='code' id='txtcode'>"
							+ "<input  type='button' id='emailOk' value='확인'>"
							+ "</div>");
					$("#emailOk").addClass('joinbtn');
							
			}
		})// ajax end
	}// if end
}// emailSend function end



// / / 아이디/비밀번호로 가입 버튼을 눌렀을 때 작동하는 함수로써 / / /
// / / / 페이지 이동 없이 link-CSS & joinForm.jsp load한다. f
function pandaoJoin() {
	$(".formBox").empty();
	$("head").children("link").next().remove();
	$("head").append("<link>");
	var css = $("head").children(":last");
	css.attr({
		rel : "stylesheet",
		type : "text/css",
		href : "/pandago/resources/css/join/joinForm.css"
	});
	$(".formBox").load("resources/jForm.jsp");
}

function okJoin() {
	console.log("회원가입 버튼")
	var name = $("#txtname").val().trim();
	var email = $("#txtemail").val().trim();
	var pw = $("#txtpw").val().trim();
	var hp = $("#txthp").val().trim();
	var pnum = $("#txtpnum").val()
	var addr = $("#txtaddr").val().trim()+$("#txtaddr2").val().trim();
	var repw = $("#txtrepw").val().trim();
	
	if (email != ""  && pw != "" && name != "" && hp != "" && pnum != ""
		&& addr != "" && pw==repw && pw.length >= 5 ) {
	
		
		var info = email + "/" + pw + "/" + name + "/" + hp + "/" + pnum + "/"
				+ addr;
		console.log(info);
		$.ajax({
			url : 'joinOk',
			type : 'post',
			data : "join=" + info,
			dataType : 'text',
			success : function(data) {

				alert("회원가입 완료");
				location.href = "/pandago/";
			}
		});
		
	}else{
		console.log("AAa");
	}
	if(pw != repw) {
		alert("비밀번호가 일치하지 않습니다.")
	}
	else if(email == "" || pw == "" || name == "" || hp == "" || pnum == ""
			|| addr == ""){
		alert("필수 입력칸이 비었습니다")
	}
	 else if (pw.length < 5) {
			alert("비밀번호는 최소 5자리 입니다")
		} 

}




