<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<link href="https://fonts.googleapis.com/css?family=Pontano+Sans" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Pacifico" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Orbitron" rel="stylesheet">
<style>
	html {
		font-family: 'Pontano Sans', sans-serif;
	}
	.logo {
		font-family: 'Orbitron', cursive;
		font-size: 45px;
	}
</style>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<script type="text/javascript">
	$(function() {
		$("#searchimg").on("click", mainsearchm);
		$("#mainsearch").keyup(mainsearchm);
	});

	function mainsearchm(event) {
		if (event.keyCode == 13 || event.type == "click") {
			location.href = "selectsearch.do?pname=" + $("#mainsearch").val()
					+ "&currentPage=1";
		}
	}
	function logout() {
		var url1 = new Array();
		url1.push(location.href);
		//url1.push("asdsadsa");
		console.log("url : " + url1);
		location.href = "logout?referrer=" + url1;
	}
</script>
<title>PANDAGO</title>
<div class="header">
	<div id="header_sec01">
		<h1 class="logo">
			<a href="/pandago">PANDAGO</a>
		</h1>

		<div id="search_box">
			<input type="text" placeholder="검색어를 입력하세요." id="mainsearch" value="${pname}" /> <input
				type="image" class="btn_search"
				src="${pageContext.request.contextPath}/resources/header_images/btn_search.gif"
				id="searchimg" />
		</div>

		<ul id="header_ul">
			<c:if test="${sessionScope.cid == null }">
				<li><a href="login">LOGIN</a></li>
				<li><a href="join" >JOIN</a></li>
			</c:if>

			<c:if test="${sessionScope.cid ne null }">
				<li><a href="javascript:;" onclick="logout();">LOGOUT</a></li>
			</c:if>

			<li><a href="cart.do">CART</a></li>
			<c:if test="${sessionScope.cid ne null }">
				<li><a href="purchase.do">ORDER</a></li>
			</c:if>
			<c:if test="${sessionScope.cid==null }">
				<li><a href="login">ORDER</a></li>
			</c:if>


			<c:if test="${sessionScope.cid ne null }">
				<li><a href="mypage.do?cid=<%=session.getAttribute("cid")%>">MYPAGE</a></li>
			</c:if>
			<c:if test="${sessionScope.cid==null }">
				<li><a href="login">MY PAGE</a></li>
			</c:if>
			<li><a href="coupon.do">COUPON</a></li>
		</ul>

		<div id="header_sec02">
			<ul id="menu_ul1">
				<li><a
					href="category.do?lcode=CODE00000001&currentPage=1&lcodename=<%=java.net.URLEncoder.encode("Smart & Play")%>">Smart
						& Play</a></li>
				<li><a
					href="category.do?lcode=CODE00000002&currentPage=1&lcodename=<%=java.net.URLEncoder.encode("Aesthetic & Interior")%>">Aesthetic
						& Interior</a></li>
				<li><a
					href="category.do?lcode=CODE00000003&currentPage=1&lcodename=<%=java.net.URLEncoder.encode("Coll & Care")%>">Coll
						& Care</a></li>
				<li><a
					href="category.do?lcode=CODE00000004&currentPage=1&lcodename=<%=java.net.URLEncoder.encode("Good Relation & GIft")%>">Good
						Relation & GIft</a></li>
			</ul>


		</div>
		<!-- header_sec02 end  -->
	</div>
	<!-- header_sec01 end  -->
</div>
<!-- header end  -->
<hr
	style="position: relative; bottom: -10px; background-color: #cac9c9:"
	width="1200px;" />