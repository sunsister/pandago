<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<div class="mypage_side_menu" >
	<ul class="side_menu_title">
		<!-- 	<li><a href="/pandago/mypage.do"> <span>MY PAGE</span>
		</a></li> -->

		<li><a href="mypage.do?cid=<%=session.getAttribute("cid")%>"><span>MYPAGE</span></a></li>
		
	</ul>

	<ul class="side_menu_ul">
		<li><a href="/pandago/purchase.do"><span>주문배송조회</span></a>
		</li>

		<li><a href="/pandago/umodify.do"><span>개인정보수정</span></a></li>

		<li><a href="myreview.do"><span>REVIEW</span></a></li>

		<li><a href="mycoupon.do" onclick="loadDocument();">
				<span>MY COUPON</span>
		</a></li>

		<li><a href="qna.do?action=list" onclick="loadDocument();"> <span>1:1 문의하기</span></a></li>
	</ul>
</div>
