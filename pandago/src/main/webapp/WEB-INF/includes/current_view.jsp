<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/userView/view.css?1">
<script type="text/javascript">
	var initHeight = 340;
	
	$(function(){	
		if(page == 1){	
			initHeight = 700;
			$(".view").css({
				top : initHeight +"px",
				left :  $(".category_List").offset().left+1200+ "px"
			});		
		}else if(page == 2){	
			initHeight = 1000;
			$(".view").css({
				top : initHeight +"px",
				left : $(".type1").offset().left + "px"
			});	
		}
		
		$("#at").attr("href","#top");
		var body = window.document.getElementsByTagName("body");
		body[0].onscroll = viewScrollTopPosition;
		
		if(body[0].scrollTop>=initHeight){
			$(".view").css("top",100+"px");	
		}else{
			$(".view").css("top",initHeight+"px");
		}
			
 		var cook = $.cookie('CURPRODUCT');
		if(cook != undefined){
 			$("#dev_cur_product").empty();
			var ckdata = cook.split(",");
			for(var i in ckdata){
				var ckdatadetail = ckdata[i].split("*");
				if(ckdatadetail[0] != '')
					$("#dev_cur_product").append("<li><a href='view.do?no="+ckdatadetail[0]+"'><img class='cur_img' src='"+ckdatadetail[1]+"' alt=''></a></li>");
			} 
 		}
	});
	
	function viewScrollTopPosition(e){
		var value=0;
		if(e.target.scrollingElement.scrollTop+100>initHeight){
			value+=100;
		}else{
			value+=initHeight-e.target.scrollingElement.scrollTop;
		}
			$(".view").css("top",value+"px");
	}
	
	$(window).resize(function(e){
		if(page==1){
			$(".view").css("left",$(".category_List").offset().left+1200+ "px");
		}
		if(page==2){
			$(".view").css("left",$(".type1").offset().left	+ "px");
				
		}
	});
</script>
<div class="view" style="border-radius: 10px; background-color: rgb(52,52,52); color: white;">
	<div><h4 style="text-align: center; ">TODAY VIEW</h4></div>
	<div style="text-align: center; margin-bottom: 10px;">
		<ul id="dev_cur_product" style="background-color: white; padding-bottom: 15px;">
		</ul>
	</div>
	<div style="text-align: center; margin-bottom: 5px; ">
		<a id="at" href="#" style="color: white; font-weight: bold; display: block;">TOP</a>
	</div>
</div>