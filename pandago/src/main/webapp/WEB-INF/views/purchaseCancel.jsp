<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@  taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<link rel="stylesheet" type="text/css" href="/pandago/resources/css/header_footer/header.css?1">
<link rel="stylesheet" type="text/css" href="/pandago/resources/css/mypage/mypage.css?1">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<style type="text/css">
body{ width: 100%;}
table {
	border-top: 1px rgb(77, 144, 254) solid;
	border-bottom: 1px rgb(77, 144, 254) solid;
	width: 98%;
	margin: 0 auto;
}

img {
	width: 200px;
	height: 200px;
}

.buttons{
	text-align: right;
	margin-right : 10px;
	margin-top: 5px;
}
</style>

</head>
<body>
	<%@ include file="../includes/header.jsp"%>
	<div class="mypage_contanier">
		<div class="mypage_box">
			<%@ include file="../includes/mypage_sidemenu.jsp"%>
			<div class="mypage_menu_contents"
				style="float: left; width: 80%; padding: 10px; margin-top: 75px;">
				<h3>주문취소</h3>
				<form action="/pandago/purchase.do">
					<input type="hidden" name="ac" value="cancelOk" />
					<div>
						<c:set var="size" value="${fn:length(list)}" />
						<table>
							<c:forEach var="list" items="${list }" varStatus="i">

								<c:if test="${i.index==0 }">
									<input type="hidden" name="payid" value=${list.payid }>
									<tr>
										<td>주문번호:${list.payid }</td>
										<td>주문일자:${list.pdate }</td>
									</tr>
								</c:if>

								<tr>

									<td><img src="${list.path }" alt="" /></td>
									<td><p>${list.pname }</p>
										<p>${list.cnt }개/${list.price }원</p></td>

								</tr>

								<c:if test="${i.index==size-1}">
									<tr>
										<td>상품취소금액:${list.pprice }원</td>
										<td>결제수단:${list.pmethod }</td>
									</tr>
								</c:if>

							</c:forEach>

						</table>
						<div class="buttons">
							<input type="submit" id="btnJoin" value="신청하기" class="mypagebtn_color btnw120"/> 
							<input type="reset" id="btnReset" value="취소하기" class="mypagebtn_white btnw120"/>
						</div>

					</div>
				</form>
			</div>
		</div>
	</div>
	<%@ include file="../includes/footer.jsp"%>
</body>
</html>