<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/header_footer/header.css?2">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/detail/detail.css?11">
<style>
	#pay2_div {
		width: 80%;
   		margin : auto; 
   		text-align: center;
   	}
   	
   	img {
   		width: 120px;
   		height:120px;
   	}
   	
   	.orderul {
   		display: inline-flex;
   		flex-direction: column;
   	}
   	
   	#pay2_title{
   		margin-left: 55px;
   	}
   	
   	#pay2_table{
   		width:90%;
   		border-top: 1px #4d90fe solid;
   		border-bottom: 1px #4d90fe solid;
   		margin : 0 auto; 
   		margin-left: 145px;
   	}
   	
   	#pay2_table tr{
   		height: 35px;
   	}
   	
   	#pay2_home{
   		width: 150px;
	 	height: 70px;
	 	margin-top : 10px;
   		background-color : #4d90fe;
   		text-align: center;
   		margin: 0 auto;
   	}
   	
   	#pay2_home a{
   		width: 100%;
	 	height: 100%;
    	font-size: 32px;
    	color: white;
   	}
</style>
</head>
<body>
	<%@ include file="../includes/header.jsp"%>
	<div id="pay2_div">
		
		<h2 id="pay2_title">주문이 완료되었습니다.</h2>
		<br/>

		<table id="pay2_table">
			<tr>
				<td>주문번호</td>
				<td>${payment.payid }</td>
			</tr>
			<tr>
				<td>주문처리상태</td>
				<td>${payment.ostate }</td>
			</tr>
			<tr>
				<td>결제금액</td>
				<td><fmt:formatNumber value="${payment.pprice }" type="number"/></td>
			</tr>
			<tr>
				<td>결제방법</td>
				<td>${payment.pmethod }</td>
			</tr>
			<tr>
				<td>쿠폰</td>
				<c:if test="${payment.cpid ne null }">
					<td>
						 ${payment.cpid }
						 ${payment.mcid }
						 쿠폰가격 ${cpprice }
					</td>
				</c:if>
				<c:if test="${payment.cpid eq null }">
					<td>쿠폰미사용</td>
				</c:if>
			</tr>
			
			<tr>
				<td>사용포인트</td>
				<td>${payment.point}</td>
			</tr>
			<tr>
				<td>적립포인트</td>
				<td><fmt:formatNumber value="${payment.pprice*0.01 }" type="number" maxFractionDigits="0"/></td>
			</tr>
			<tr>
				<td>구매목록</td>
				<td>
					<table>
						<c:forEach var="pd" items="${pdlist }" varStatus="status">
							<tr>
								<td>
								<a href="view.do?no=${product[status.index].pid }"><img src="${product[status.index].path }" alt="" /></a>	
								</td>
								<td>
									<ul class="orderul">
										<li>상품상세번호 : ${pd.pdid }</li>
										<li>상품명 : ${product[status.index].pname }</li>
										<li>가격 : ${pd.price }</li>
									</ul>
								</td>
							</tr>
						</c:forEach>
					</table>
				</td>
			</tr>
		</table>
		<div id="pay2_home"><a class="paybtn_atag" href="/pandago"><span>홈으로</span></a></div>
	</div>
	
	<%@ include file="../includes/footer.jsp"%>
</body>
</html>
				
