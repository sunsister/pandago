<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="/pandago/resources/css/header_footer/header.css?1">
<link rel="stylesheet" type="text/css" href="/pandago/resources/css/mypage/mypage.css?5">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/mypage.js"></script>
<style>
	#userdiv, #shopdiv, #qnadiv,#reviewdiv{
		width : 48%;
		height : 250px;
		float : left;
		margin-right: 10px;
		margin-bottom : 10px;
	}
	
/* 	#shopdiv{
		background : green;
	}
	
	#qnadiv{
		background : red;
	}
	
	#reviewdiv{
		background : blue;
	} */
	
	#orderdiv{
		width : 97%;
		height : 250px;
		/* background : pink; */
		float : left;
		margin-bottom : 10px;
	}
	
	table{
		width:95%;
		text-align: center;
		border-top : 1px rgb(77,144,254) solid;
		border-bottom : 1px rgb(77,144,254) solid;
		margin:0 auto;
	}
	tr:first-child{
		background : rgba(77,144,254,0.2);
	}
	
	h3 {
		width:80%;
		margin-left : 20px;
		float:left;
	}
	
	.more {
		text-align : right;
		margin-top: 32px;
    	margin-left: 15px;
		font-size: 12px;
	}
	
	.more2 {
		margin-left: 100px;
   		margin-top: 33px;
		font-size: 12px;
	}
	
</style>
</head>
<body>
<%@ include file="../includes/header.jsp" %>

		<div class="mypage_contanier">
			<div class="mypage_box">
				<%@ include file="../includes/mypage_sidemenu.jsp" %>
	
				<div class="mypage_menu_contents">
					<div class="mypage_menu_contents_loc">
						<%-- <div id="userdiv">
							<h3>${customer.name } 님</h3>
							<a class="more" href="/pandago/umodify.do">수정</a>
							<table>
								<tr>
									<td>이메일</td>
									<td>${customer.cid }</td>
								</tr>
								<tr>
									<td>회원등급</td>
									<td>${customer.grade }</td>
								</tr>
								<tr>
									<td>총구매금액</td>
									<td><fmt:formatNumber value="${customer.tmoney }" type="number"/></td>
								</tr>
								<tr>
									<td>주소</td>
									<td>(${customer.pnum })<br/>${customer.addr }</td>
								</tr>
							</table>
						</div> --%>
	<!-- 					<div id="shopdiv">
							
						</div> -->
						
						<div id="orderdiv">
							<h3>Order</h3>
							<a class="more2" href="/pandago/purchase.do">더보기</a>
							<table>
								<tr>
									<th>주문번호</th>
									<th>주문금액</th>
									<th>주문처리상태</th>
									<th>구매일</th>
								</tr>
								<c:if test="${empty order}">
									<tr>
										<td colspan="4">구매한 상품이 없습니다.</td>
									</tr>
								</c:if>
								<c:if test="${not empty order}">
									<c:forEach begin="0" end="4" var="i" >
										
										<c:if test="${order[i].payid ne null}">
											<tr>
												<td>${order[i].payid }</td>
												<td><fmt:formatNumber value="${order[i].pprice }" type="number"/></td>
												<td>${order[i].ostate }</td>
												<td>${order[i].pdate }</td>
											</tr>
										</c:if>
										
										
									</c:forEach>
								</c:if>
							</table>
						</div>
						
						<div id="qnadiv">
							<h3>Q&A</h3>
							<a class="more" href="/pandago/qna.do?action=list">더보기</a>
							<table>
								<tr>
									<th>제목</th>
									<th>작성일</th>
									<th>답변여부</th>
								</tr>
								<c:if test="${empty qna}">
									<tr>
										<td colspan="3">작성한 문의글이 없습니다.</td>
									</tr>
								</c:if>
								<c:if test="${not empty qna }">
									<c:forEach begin="0" end="2" var="i" >
										<tr>
											<td>${qna[i].title }</td>
											<td>${qna[i].qdate }</td>
											<c:if test="${qna[i].aok == 0 }">
												<td>N</td>
											</c:if>
											<c:if test="${qna[i].aok == 1} ">
												<td>Y</td>
											</c:if>
										</tr>
									</c:forEach>
								</c:if>
							</table>
						</div>
						<div id="reviewdiv">
							<h3>Review</h3>
							<a class="more" href="/pandago/myreview.do">더보기</a>
							<table>
								<tr>
									<th>제목</th>
									<th>작성일</th>
								</tr>
								<c:if test="${empty review}">
									<tr>
										<td colspan="2">작성한 리뷰가 없습니다.</td>
									</tr>
								</c:if>
								<c:if test="${not empty review}">
									<c:forEach begin="0" end="2" var="i">
										<tr>
											<td>${review[i].title }</td>
											<td>${review[i].wdate }</td>
										</tr>
									</c:forEach>
								</c:if>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>		
	<%@ include file="../includes/footer.jsp"%>	
</body>
</html>