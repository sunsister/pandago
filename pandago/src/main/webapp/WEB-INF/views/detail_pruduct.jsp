<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page trimDirectiveWhitespaces="true" %> 
<!DOCTYPE>
<html>
<head>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/detail.js"></script>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/header_footer/header.css?1">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/detail/detail.css?5">

</head>
<body>
	<%@ include file="../includes/current_view.jsp" %>
	<%@ include file="../includes/header.jsp"%>
	
	<div class="detail_container">
		<!-- 옵션 / 결제  -->
		<div class="deatil_header">
			<div class="title_image">
				<img src="${detail.path }" alt="" id="mainimg"/>
				<div class="selectmainimg">
					 <c:forEach items="${mainimg }" var="mainimg">
						 <a href="javascript:;" onclick="changeimg('${mainimg.path}');"><img alt="" src="${mainimg.path}" style="width: 50px; height: 50px; margin: 0"></a>
					</c:forEach> 
				</div><!-- DIV .selectmainimg end  -->				
			</div><!-- DIV .title_image end  -->
			<div class="option">
				<form action="pay.do" name="order">
					<input type="hidden" name="action" value="step1now" />
					<div class="navi">
						<ul>
							<li><a href="/pandago">home > </a></li>
							<c:choose>
								<c:when test="${detail.lcode eq 'CODE00000001'}">
									<li>
										<a href="category.do?lcode=CODE00000001&currentPage=1&lcodename=Smart+%26+Play">Smart And Play</a>
									</li>
								</c:when>
								<c:when test="${detail.lcode eq 'CODE00000002'}">
									<li>
										<a href="category.do?lcode=CODE00000002&currentPage=1&lcodename=Aesthetic+%26+Interior">Aesthetic And Interior</a>
									</li>
								</c:when>
								<c:when test="${detail.lcode eq 'CODE00000003'}">
									<li>
										<a href="category.do?lcode=CODE00000003&currentPage=1&lcodename=Coll+%26+Care">Coll And Care</a>
									</li>
								</c:when>
								<c:when test="${detail.lcode eq 'CODE00000004'}">
									<li>
										<a href="category.do?lcode=CODE00000004&currentPage=1&lcodename=Good+Relation+%26+GIft">Good Relation And GIft</a>
									</li>
								</c:when>
							</c:choose>
						</ul>
						<span>상품번호 : ${detail.pid }</span> 
						<input type="hidden" name="pid" id="pid" value="${detail.pid }" />
					</div><!-- DIV .navi end  -->

					<div class="select">
						<input type="hidden" name="dimg" value="${detail.path }" />
						<div class="title">
							<h2>${detail.pname }</h2>
							<input type="hidden" name="pname" value="${detail.pname }" />
							<c:choose>
								<c:when test="${detail.sale == 0 }">
									<span> <fmt:formatNumber value="${detail.price }" type="number"/> 원</span>
									<input type="hidden" name="price" id="price" value="${detail.sprice }" />
								</c:when>
								<c:otherwise>
									<span style="text-decoration: line-through;"><fmt:formatNumber value="${detail.price }" type="number"/> 원</span>
									<input type="hidden" name="price" id="price" value="${detail.sprice }" />
									<span> ${detail.sprice } 원 (${detail.sale }% DC) </span>
								</c:otherwise>
							</c:choose>
						</div>

						<div class="info">
							<ul class="ul_info">
								<li>
									<h4>무이자 할부</h4> <span>자세히</span>
								</li>

								<li>
									<h4>상품정보</h4> <span>전자상거래 상품정보 제공 고시 정보</span>
								</li>

								<li>
									<h4>브랜드</h4> <span>${detail.factory }</span>
								</li>

								<li>
									<c:if test="${option.get(0).color ne null or option.get(0).psize ne null }">
										<c:choose>
											<c:when test="${option.get(0).color ne null and option.get(0).psize eq null }">
												<h4>색상</h4>
												<span> 
													<select name="pdoption" id="pdoption">
														<option value="0">--색상 선택--</option>
											</c:when>

											<c:when test="${option.get(0).color eq null and option.get(0).psize ne null }">
												<h4>사이즈</h4>
												<span> 
													<select name="pdoption" id="pdoption">
														<option value="0">--사이즈 선택--</option>
											</c:when>
											<c:when test="${option.get(0).color ne null and option.get(0).psize ne null }">
												<h4>옵션</h4>
												<span> 
													<select name="pdoption" id="pdoption">
														<option value="0">-- 선택--</option>
											</c:when>
										</c:choose>

										<c:forEach items="${option}" var="option">
											<c:choose>
												<c:when test="${option.color ne null and option.psize ne null }">
													<c:choose>
														<c:when test="${option.stock eq 0}">
															<option disabled="disabled">${option.color} ${option.psize} - 품절</option>
														</c:when>

														<c:otherwise>
															<option value="${option.pdid},${option.color},${option.addprice}">${option.color} ${option.psize}</option>
														</c:otherwise>
													</c:choose>
												</c:when>

												<c:otherwise>
													<c:if test="${option.color ne null }">
														<c:choose>
															<c:when test="${option.stock eq '0'}">
																<option disabled="disabled">${option.color}-품절
																</option>
															</c:when>
															<c:when test="${option.stock ne '0'}">
																<option value="${option.pdid},${option.color},${option.addprice}">${option.color}</option>
															</c:when>
														</c:choose>
													</c:if>
													<c:if test="${option.psize ne null }">
														<c:choose>
															<c:when test="${option.stock eq '0'}">
																<option disabled="disabled">${option.psize}-품절</option>
															</c:when>
															<c:when test="${option.stock ne '0'}">
																<option value="${option.pdid},${option.psize},${option.addprice}">${option.psize}</option>
															</c:when>
														</c:choose>
													</c:if>
												</c:otherwise>
											</c:choose>
										</c:forEach>
										</select>
										</span>
										<select name="pdcount" id="pdcount">
											<option value="0">-- 개수선택 --</option>
											<option value="1">1</option>
											<option value="2">2</option>
											<option value="3">3</option>
											<option value="4">4</option>
											<option value="5">5</option>
										</select>
									</c:if>
									
									<c:if test="${option.get(0).color eq null and option.get(0).psize eq null }">
										<h4>개수</h4>
										<input type="hidden" name="pdoption" id="cntpdid" value="${option.get(0).pdid}" />
										<span> 
											<select name="pdcount" id="pdcount">
												<option value="0">-- 개수선택 --</option>
												<option value="1">1</option>
												<option value="2">2</option>
												<option value="3">3</option>
												<option value="4">4</option>
												<option value="5">5</option>
										</select>
										</span>
									</c:if>
								</li>
							</ul>
						</div><!-- DIV .info end  -->
					</div><!-- DIV .select end  -->

					<div class="button">
						<div class="price">
							<span> 총 상품 금액 : </span>
							<h2>0</h2>
							<span>원</span>
						</div>

						<div class="buttons">
							<a href="javascript:;" id="cartatag"> 
								<div class="basket">
									<img src="${pageContext.request.contextPath}/resources/detailImages/icon_cart.png" alt="" width="20px" height="20px" />
									<h3>장바구니</h3>
								</div>
							</a>

							<a href="javascript:;" onclick="buy('${cid}');"> 
								<div class="buy">
									<img src="${pageContext.request.contextPath}/resources/detailImages/icon_buy.png" alt="" width="20px" height="20px" />
									<h3>바로구매</h3>
								</div>
							</a>
						</div><!-- DIV .buttons end  -->
					</div><!-- DIV .button end  -->
				</form>
			</div><!-- DIV .option end  -->
		</div><!-- DIV .deatil_header end -->

		<!-- content -->
		<div class="deatil_body">
			<div>
				<!--  design : tab -->
				<div class="deatil_footer" id="top">
					<div class="tab">
						<div class="type1" onclick="tab('typeOne'); show();" style="background: lightgray">
							<span>상품정보</span>
						</div>
						<div class="type2" onclick="tab('typeTwo'); hide();">
							<span>상품후기</span>
						</div>
						<div class="type3" onclick="tab('typeThree'); hide();">
							<span>상품문의</span>
						</div>
					</div>

					<div class="area">
						<div class="typeOne hide box activity">
							<div class="one">
								<c:forEach var="dimg" items="${dimg}">
									<img class="dtinfiimg" src="${dimg.path }" alt=""
										style="margin-left: 100px;" />
								</c:forEach>
								<a href="#" style="text-align: right;">맨위로</a>
							</div>
							<!-- DIV one end  -->
						</div>
						<!-- DIV typeOne end  -->

						<div class="typeTwo hide box">
							<div class="two">		
								<ul>
								<c:if test="${fn:length(rslist) != 0}">
									<c:forEach items="${rslist }" var="rslist">
										<c:if test="${rslist!=null }">

											<li>
												<div>
													<input type="hidden" name="product_view_no" value="1" />
													<span style="text-decoration: underline; height: auto;">제목 : ${rslist.title } </span>
														<span> ${rslist.content }	
													</span>
													
  													<label class="userId"> 회원 : ${rslist.cid}</label>
													<p class="star_rating">
													<c:forEach begin="1" end="5" var="i">
    													<c:if test="${ rslist.star >=i}">
    													<a href="javascript:;" class="on">★
    														<input type="hidden" name=""  />
    													</a>
    													</c:if >
    													
    													<c:if test="${ rslist.star <i}">
    													<a href="javascript:;" >★
    														<input type="hidden" name=""   />
    													</a>
    													</c:if >
													</c:forEach>
    												</p>									
												</div>
											</li>
										</c:if>
									</c:forEach>
								</c:if>
								<c:if test="${fn:length(rslist) == 0}">							
									<h2> 사용 후기가 없습니다.! </h2>								
								</c:if>
								</ul>
							</div>
							<a href="#"> 맨위로</a>
						</div>
						<!-- DIV typeTwo end  -->

						<div class="typeThree hide box">
							<div class="_three" >		
								<div id="inquirytable" >
							
									<c:if test="${fn:length(islist) == 0}">	
										<h2>문의 내용이 없습니다.</h2>					
									</c:if>
																			
									<c:if test="${fn:length(islist) != 0}">
										<table class="p_Qna_board">
											<tr>
												<!-- <th style="width: 35px;">순서</th> -->
												<!-- <th style="width: 100px;">문의ID</th> -->
												<!-- <th style="width: 50px;">상품ID</th> -->
												<th style="width: 50px; text-align: center;">고객ID</th>
												<th id="title123" style="width: 200px;">제목</th>
												<th style="width: 320px;" >내용</th>
												<th style="width: 150px;">작성일</th>
												<th style="width: 70px;"></th>
											</tr>
											<c:forEach items="${islist}" var="islist">
												<tr>
													<%-- <td style="width: 35px;">${islist.rn}</td> --%>
													<%-- <td style="width: 100px;">${islist.inid }</td> --%>
													<%-- <td style="width: 50px;">${islist.pid}</td> --%>
													<td style="width: 80px; text-align: center;">
														<c:set var="islistcid" value="${islist.cid}"/>
														<c:set var="Subcid" value="${fn:substring(islistcid,0,3)}"/>
														${Subcid}***
													</td>
													<td>${islist.intitle}</td>
													
													<c:if test="${islist.inpw != null}">
														<td style="text-align: center;">
															<p id="${islist.inid}con">
																<input type="password" placeholder="Password & Enter" style="width: 90%;" name="" id="${islist.inid}pwinid"  onkeypress="pwcheck('${islist.inid}','${islist.inpw}','${islist.incontent}',${islist.inaok})" />
															</p>
														</td>
													</c:if>

													<c:if test="${islist.inpw == null}">

														<c:if test="${islist.incontent!=null}">
															<td style="padding-left: 10px;">${islist.incontent}</td>
														</c:if>
														
														<c:if test="${islist.incontent==null}">
															<td style="padding-left: 10px;">내용이 없습니다.</td>
														</c:if>

													</c:if>

													<td>${islist.indate}</td>
													
													<c:if test="${cid ==islist.cid }">
														<td>														
															<a href="javascript:;" onclick="deleteis('${islist.inid }','${islist.pid}');">삭제하기</a>
														</td>
													</c:if>
												</tr>
												
												<tr id="${islist.inid }acon" style="display: none;">
													<td></td>
													<td></td>
													<td style="text-align: right; font-weight: bold;">└담당자 : </td>
													<td style="font-weight: bold;">${islist.inacontent}</td>
												</tr>
												
												<c:if test="${islist.inpw == null and islist.inaok==1}">	
													<tr >
														<td></td>
														<td></td>
														<td style="text-align: right; font-weight: bold;">└담당자 : </td>
														<td style="font-weight: bold;">${islist.inacontent}</td>
													</tr>
												</c:if>
											</c:forEach>
										</table>
									</c:if>			
									<c:if test="${cid==null }">
										<br /><h2>문의 하시려면 <a href="login" style="color: blue;">로그인</a>을 하세요.</h2>
									</c:if>	
																
									<c:if test="${cid!=null }">
										<br />
										<div style="width: 1000px; height: 200px; background-color: white; margin-bottom: 30px; margin: auto;">
											<ul>
												<li style="display:block;">
													<input type="text" name="" id="titlepQna" style="width: 100%; font-size: 20px;" placeholder="제목을 입력하세요."/>
												</li>
												
												<li id="pwwrite" style="display: none; ">
													<input style="width: 100%;" type="password" placeholder="비밀번호" name="" id="pwwriteinput" />
												</li>
												
												<li style="display: block;">
													<textarea id="contentpQna" rows="6" style="width: 100%"  placeholder= "전화번호, 성명, 주소, 이메일을 남기시면 타인에 의해 도용될 수 있습니다. Q&A에 개인정보를 남기지 말아주세요"></textarea>
												</li>
												
												<li style="display: block;">
													<ul>
														<li>
															공개<input type="radio" name="pw" id="" value="0" checked="checked">
														</li>
														
														<li>
															 : 비공개<input type="radio" name="pw" id="" value="1" />
														</li>
													</ul>	
												</li>											
											</ul>
											<a href="javascript:;" id=isinsert>글쓰기</a>
										</div>
									</c:if>						
								</div><!-- DIV #inquiryTable end  -->	
							</div><!-- DIV ._three end -->
						</div><!-- DIV .typeThree end  -->
					</div><!-- DIV .area end  -->
				</div><!-- DIV .deatil_footer end  -->
			</div><!-- DIV end  -->
		</div><!-- DIV .deatil_body end  -->
	</div><!-- DIV .detail_container end -->
<%@ include file="../includes/footer.jsp"%>
</body>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.js"></script>
</html>