<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet" type="text/css"
	href="/pandago/resources/css/header_footer/header.css?1">
<link rel="stylesheet" type="text/css"
	href="/pandago/resources/css/mypage/mypage.css?1">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script type="text/javascript">
	$(function() {
		$("#btn_qna_list").on("click", function() {
			location.href = "/pandago/qna.do?action=list";
		});
	});
</script>
</head>
<body>
	<%@ include file="../includes/header.jsp"%>
	<div class="mypage_contanier">
		<div class="mypage_box">
			<%@ include file="../includes/mypage_sidemenu.jsp"%>

			<div class="mypage_menu_contents"
				style="float: left; width: 80%; padding: 10px; margin-top: 75px;">
				<c:when test="${write != null }">
					<div class="qnaWrite">
						<h3>1:1문의 작성하기</h3>
						<form action="/pandago/qna.do">
							<input type="hidden" name="action" value="writeOK" />
							<table>
								<tr>
									<td colspan="5">
										<!-- 분류 --> <input type="radio" name="qcode" value="배송관련">배송관련
										<input type="radio" name="qcode" value="취소/교환/반품">취소/교환/반품
										<input type="radio" name="qcode" value="할인쿠폰/포인트">할인쿠폰/포인트
									</td>
								</tr>
								<tr>
									<th>제목</th>
									<td colspan="4"><input type="text" name="title" /></td>
								</tr>
								<tr>
									<th>내용</th>
									<td colspan="4"><textarea name="qcontent" cols="30"
											rows="10"></textarea></td>
								</tr>
								<tr>
									<td colspan="5"><input type="submit" value="작성완료" /> <input
										type="button" value="취소" id="btn_qna_list" /></td>
								</tr>
							</table>
						</form>
					</div>
				</c:when>
			</div>
		</div>
	</div>
</body>
</html>

