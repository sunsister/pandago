<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<style type="text/css">
.mypage_menu_contents form {
	width: 100%;
	height: 506px;
	background-color: white;
	top: 60px;
	border-bottom: 1px solid #4d90fe;
}

.mypage_menu_contents form ul {
	width: 60%;
    margin: 0 auto;
}

.mypage_menu_contents form ul li {
	display: inline-block;
	margin-bottom: 10px;
}

.mypage_menu_contents form ul li label {
	display: inline-block;
	height: 25px;
	width: 200px;
}

.mypage_menu_contents form ul li input[type=text], input[type=password]
	{
	width: 200px;
	height: 35px;
	border-radius: 5px;
	outline: none;
}

.mypage_menu_contents form ul li .autherntication>#btnEmailSend {
	display: inline-block;
}

.mypage_menu_contents form ul li .idCheck {
	display: inline-block;
}

.mypage_menu_contents form ul li label {
	width: 150px;
	text-align: right;
	margin-right: 10px;
}

.mypage_menu_contents form div.buttons {
	width: 125px;
	margin: 0 auto;
}
</style>
<meta charset="UTF-8">
<link rel="stylesheet" type="text/css"
	href="/pandago/resources/css/header_footer/header.css?1">
<link rel="stylesheet" type="text/css"
	href="/pandago/resources/css/mypage/mypage.css?3">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/join.js?4"></script>
<script src="http://dmaps.daum.net/map_js_init/postcode.v2.js"></script>
<script type="text/javascript">
	$(function() {
		$("#btnReset").on("click", function() {
			location.href = "/pandago/mypage.do";
		});
	});
	function sample6_execDaumPostcode() {
		console.log("daum d/d/dd;");
		daum.postcode.load(function(){
			new daum.Postcode({
				oncomplete: function(data){
					// 팝업에서 검색결과 항목을 클릭했을때 실행할 코드를 작성하는 부분.

		            // 각 주소의 노출 규칙에 따라 주소를 조합한다.
		            // 내려오는 변수가 값이 없는 경우엔 공백('')값을 가지므로, 이를 참고하여 분기 한다.
		            var fullAddr = ''; // 최종 주소 변수
		            var extraAddr = ''; // 조합형 주소 변수

		            // 사용자가 선택한 주소 타입에 따라 해당 주소 값을 가져온다.
		            if (data.userSelectedType === 'R') { // 사용자가 도로명 주소를 선택했을 경우
		                fullAddr = data.roadAddress;

		            } else { // 사용자가 지번 주소를 선택했을 경우(J)
		                fullAddr = data.jibunAddress;
		            }

		            // 사용자가 선택한 주소가 도로명 타입일때 조합한다.
		            if(data.userSelectedType === 'R'){
		                //법정동명이 있을 경우 추가한다.
		                if(data.bname !== ''){
		                    extraAddr += data.bname;
		                }
		                // 건물명이 있을 경우 추가한다.
		                if(data.buildingName !== ''){
		                    extraAddr += (extraAddr !== '' ? ', ' + data.buildingName : data.buildingName);
		                }
		                // 조합형주소의 유무에 따라 양쪽에 괄호를 추가하여 최종 주소를 만든다.
		                fullAddr += (extraAddr !== '' ? ' ('+ extraAddr +')' : '');
		            }

		            // 우편번호와 주소 정보를 해당 필드에 넣는다.
		            document.getElementById('txtpnum').value = data.zonecode; //5자리 새우편번호 사용
		            document.getElementById('txtaddr').value = fullAddr;

		            // 커서를 상세주소 필드로 이동한다.
		            document.getElementById('txtaddr2').focus();
				}
			}).open();
		});
	}
	</script>
	<script type="text/javascript">
		$(function(){
			console.log("스트립트 로드");
			$(document).on("click", "#emailOk", emailOk);
		});
		
		function emailOk() {
			var emailKey = $("#txtcode").val();
			console.log("이메일오케이확인");
			$.ajax({
				url : 'emailOk',
				type : 'post',
				data : "emailKey=" + emailKey,
				dataType : "text",
				success : function(data) {
					if (data.trim() == 'true') {
						$(".divInputCode").remove();
						$("#appendTd").append("인증완료");
					} else if(data.trim()=='false'){
						alert("인증 번호가 올바르지 않습니다.");
					}
				}
			});
		}
</script>
</head>
<body>
	<%@ include file="../includes/header.jsp"%>
	<div class="mypage_contanier">
		<div class="mypage_box">
			<%@ include file="../includes/mypage_sidemenu.jsp"%>
			<div class="mypage_menu_contents">
				<div class="mypage_menu_contents_loc">
					<form action="/pandago/umodify.do">
						<h2 style="border-bottom: 1px solid #4d90fe; margin:0px; padding-bottom:25px;padding-top:12px; padding-right: 30px;">
						개인정보수정</h2>
						<input type="hidden" name="ac" value="userModifyOk" /> 
						<input type="hidden" name="cid" value=${dto.cid } />
	
						<ul style="padding-top: 18px;">
							<li><label for="txtemail">이메일</label><label id="userlabel">${dto.cid}</label></li>
							<li>
								<label for="txtpw">비밀번호</label> 
								<input type="password" name="pw" id="txtpw" />
							</li>
							<li>
								<label for="txtrepw">비밀번호 확인</label> 
								<input type="password" name="repw" id="txtrepw" />
							</li>
							<li>
								<label for="txtname">이름</label> 
								<input type="text" name="name" id="txtname" value=${dto.name } />
							</li>
							<li>
								<label for="txthp">휴대폰</label> 
								<input type="text" name="hp" id="txthp" value=${dto.hp } />
							</li>
	
							<li>
								<div>
									<label for="txtaddr">주소</label> 
									<input name ="pnum" type="text" id="txtpnum" placeholder="우편번호" style="width: 100px" value=${dto.pnum } />
									<input class="mypagebtn_white" type="button" onclick="sample6_execDaumPostcode();" value="우편번호 찾기" />
								</div> 
								<label for=""> </label> 
								<input type="text" id="txtaddr"
								placeholder="주소" name="addr" style="width: 250px" value=${dto.addr } /> 
								<label for=""> </label> 
								<input type="text" id="txtaddr2" placeholder="상세주소" style="margin-left: 164px;"/>
							</li>
						</ul>		
	
						<div class="buttons" style="width: 300px;margin: 0 auto;">
							<input class="mypagebtn_color btnw120" type="submit"  value="수정하기" /> 
							<input class="mypagebtn_white btnw120" type="reset" id="btnReset" value="취소하기" />
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	 <%@ include file="../includes/footer.jsp"%>
</body>
</html>

