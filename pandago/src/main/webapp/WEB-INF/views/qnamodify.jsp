<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet" type="text/css" href="/pandago/resources/css/header_footer/header.css?1">
<link rel="stylesheet" type="text/css" href="/pandago/resources/css/mypage/mypage.css?1">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script type="text/javascript">
	$(function(){
		$("#btn_qna_list").on("click", function(){
			location.href = "/pandago/qna.do?action=list";
		});
	});
</script>
</head>
<body>
	<%@ include file="../includes/header.jsp" %>
	<%@ include file="../includes/mypage_sidemenu.jsp" %>
	<div>
		<h3>1:1문의 수정하기</h3>
		<form action="/pandago/qna.do">
		<input type="hidden" name="action" value="modifyOK" />
		<input type="hidden" name="qnaid" value="${dto.qnaid }" />
		<table>
			<tr>
				<th>분류</th>
				<td>
					<c:if test="${dto.qcode eq '배송관련'}">
						<input type="radio" name="qcode" value="배송관련" checked>배송관련
						<input type="radio" name="qcode" value="취소/교환/반품">취소/교환/반품
						<input type="radio" name="qcode" value="할인쿠폰/포인트">할인쿠폰/포인트
					</c:if>
					<c:if test="${dto.qcode eq '취소/교환/반품'}">
						<input type="radio" name="qcode" value="배송관련">배송관련
						<input type="radio" name="qcode" value="취소/교환/반품" checked>취소/교환/반품
						<input type="radio" name="qcode" value="할인쿠폰/포인트">할인쿠폰/포인트
					</c:if>
					<c:if test="${dto.qcode eq '할인쿠폰/포인트'} ">
						<input type="radio" name="qcode" value="배송관련">배송관련
						<input type="radio" name="qcode" value="취소/교환/반품">취소/교환/반품
						<input type="radio" name="qcode" value="할인쿠폰/포인트" checked>할인쿠폰/포인트
					</c:if>
					
				</td>
			</tr>
			<tr>
				<th>작성일</th>
				<td>${dto.qdate }</td>
			</tr>
			<tr>
				<th>제목</th>
				<td colspan="4"><input type="text" name="title" value="${dto.title }"/></td>
			</tr>
			<tr>
				<th>내용</th>
				<td colspan="4">
					<textarea name="qcontent" cols="30" rows="10">${dto.qcontent }</textarea>
				</td>
			</tr>
						
			<tr>
				<td colspan="5">
					<input type="submit" value="수정완료" />
					<input type="button" value="문의목록" id="btn_qna_list"/>
				</td>
			</tr>
		</table>
		</form>
	</div>		
</body>
</html>
				
