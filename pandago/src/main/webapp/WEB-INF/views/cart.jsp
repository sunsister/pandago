<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/header_footer/header.css?1">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/cart/cart.css">
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/cart.js?4"></script>
<body>
	<%@ include file="../includes/header.jsp"%>
	<div class="container">
		<div id="carttable">			
			<ul>
			</ul>
			<c:forEach items="${list}" var="list">
				<ul class="dev_cartlist">
					<li>
						<input type="hidden" class="dev_cartid" value="${list.cartid }" />
						<input type="hidden" class="dev_pid" value="${list.pid }" />
						<input type="hidden" class="dev_dimg" value="${list.path }" />
						<input type="hidden" class="dev_sprice" value="${list.sprice }" />
						<input type="hidden" class="dev_pname" value="${list.pname }" />
						<input type="hidden" class="dev_pdid" value="${list.pdid }" />
						<input type="checkbox" name="cartcb" id="check"	value="${list.cartid }" />
					</li> 
						
					<li>
						<img src="${list.path }" alt="" />
					</li>
					
					<li>
						<div>
							<p>
								[Brand]  <a href="view.do?no=${list.pid }">${list.factory }</a>
							</p>
							
							<a style="display: block; width: 650px;" href="view.do?no=${list.pid }">${list.pname }</a>
						</div>
					</li>
					
					<li  style="position: relative; top: -17px;">				
						<p>수량</p>
						<input type="number" id="${list.cartid }cnt" class="dev_cnt" value="${list.cnt }" onclick="cntchange(this,'${list.cartid }',${list.sprice},'${list.cartid }tp','${list.cartid }tprice')" min="1" max="5"/>
					</li>		
					
					<li style="position: relative; top: -17px;">
						<p>옵션</p>
						<c:choose>
							<c:when test="${list.color eq null }">
								<label class="dev_psize">${list.psize }</label>
							</c:when>
							
							<c:when test="${list.psize eq null }">
								<label class="dev_color">${list.color }</label>
							</c:when>
						</c:choose>	
					</li>
				
					<li class="ppricet"  style="position: relative; top: -17px;">			
							<label id="${list.cartid }tprice" class="tprice"><fmt:formatNumber value="${list.sprice* list.cnt }" type="number"/>원</label>				
					</li> 
						
					<li>
						<input type="hidden" id="cartid" value="${list.cartid }" />
						<input type="hidden" id="sprice" value="${list.sprice}" />
					</li>
				</ul>
			</c:forEach>
		</div><!--  DIV cart table end -->
		
		<div class="buttons">			
		<!-- rgb(79,158,201)  -->
			<button id="checkall" style="background-color: #4d90fe; border: none; color: white;">전체 선택</button>	
			<button id="spay"     style="background-color: #4d90fe; border: none; color: white;">결제하기</button>
			<button id="sdelete"  style="background-color: #4d90fe; border: none; color: white;">삭제하기</button>
			<input type="hidden" name="" id="checkallv" value="0" />
		</div><!-- DIV end  -->
		
		<div id="total">
		
		</div><!-- DIV total end  -->
		
		
	</div>

	<c:forEach items="${list}" var="list">
		<input type="hidden" name="" id="${list.cartid }tp" value="${list.sprice* list.cnt }" />
	</c:forEach>
	
	<input type="hidden" id="payprice" value="1" />
	<%@ include file="../includes/footer.jsp"%>
</body>
</html>
