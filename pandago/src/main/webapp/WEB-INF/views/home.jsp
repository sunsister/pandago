<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
<meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, width=device-width, height=device-height">
<link rel="styesheet" media="" href="">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/header_footer/header.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/main/main.css?5">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/main/main_slider.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
</head>
<body>
<%@ include file="../includes/header.jsp" %>
<%@ include file="../includes/main_slider_image.jsp" %>
<div class="main_product">
	<div class="main_product01">	
		<ul class="main_product01_contents">	
			<c:forEach var="mlist" begin="0" end="4" items="${list}">
			 <li>
				<h2>
					<a href="view.do?no=${mlist.pid }">
						<img id="main_product01_contents_img" src="${mlist.path }" alt="" />
					</a>
				</h2>			
				<div>	
					<a href="view.do?no=${mlist.pid }">
						<h3>${mlist.factory}</h3>
						<span>${mlist.pname}</span>
					</a>
				</div>
			</li> 
			</c:forEach>
		</ul>
		
	</div>
	
	<div class="main_product02">
		<img src="http://quartershop.co.kr/data/skin/default/images/common/bnr_main_01.jpg" alt="" />
	</div>
	
	<h3 id="bestname">BEST 상품</h3>
	<div class="main_product03">
		<ul class="main_product03_contents">	
			<c:forEach var="mlist" begin="0" end="9" items="${bestlist}">
			 <li>
				<h2>
				<img id="main_product03_best_img" src="${pageContext.request.contextPath}/resources/main_images/best1.gif" alt="" />
					<a href="view.do?no=${mlist.pid }">
						<img id="main_product03_contents_img" src="${mlist.path }" alt="" />
					</a>
				</h2>			
				<div style="width: 100%;">	
					<a href="view.do?no=${mlist.pid }">
						<h3>${mlist.factory}</h3>
						<span>${mlist.pname}</span>
					</a>
				</div>
			</li> 
			</c:forEach>
		</ul>
	</div>
	
	<div class="main_product04">
		<img src="http://quartershop.co.kr/data/skin/default/images/common/bnr_main_02.jpg" alt="" />
	</div>
	
	<h3 id="newname">NEW 상품</h3>
	<div class="main_product05">
		<ul class="main_product05_contents">	
			<c:forEach var="mlist" begin="0" end="9" items="${newlist}">
			 <li>
				<h2>
					<a href="view.do?no=${mlist.pid }">
				<img id="main_product05_new_img" src="${pageContext.request.contextPath}/resources/main_images/new2.gif" alt="" />
						<img id="main_product05_contents_img" src="${mlist.path }" alt="" />
					</a>
				</h2>			
				<div style="width: 100%;">	
					<a href="view.do?no=${mlist.pid }">
						<h3>${mlist.factory}</h3>
						<span>${mlist.pname}</span>
					</a>
				</div>
			</li> 
			</c:forEach>
		</ul>
	</div>
<%@ include file="../includes/footer.jsp"%>
</div>
</body>
</html>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/home.js?3"></script>

