<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet" type="text/css"
	href="/pandago/resources/css/header_footer/header.css?1">
<link rel="stylesheet" type="text/css"
	href="/pandago/resources/css/mypage/mypage.css?1">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script type="text/javascript">
	$(function() {
		$("#btn_qna_list").on("click", function() {
			location.href = "/pandago/qna.do?action=list";
		});
	});
</script>
</head>
<body>
	<%@ include file="../includes/header.jsp"%>
	<div class="mypage_contanier">
		<div class="mypage_box">
			<%@ include file="../includes/mypage_sidemenu.jsp"%>

			<div class="mypage_menu_contents"
				style="float: left; width: 80%; padding: 10px; margin-top: 75px;">
				<c:when test="${Ddto != null }">
					<div class="qnaDetail">
						<div>
							<h3>1:1문의 상세보기</h3>
						</div>

						<div class="qna">
							<p>
								<input type="radio" name="qcode" value="${Ddto.qcode }" checked>${Ddto.qcode }
								&nbsp;&nbsp;&nbsp;&nbsp; ${Ddto.qdate }
							</p>

							<h4>제목 : ${Ddto.title }</h4>

							<div class="userContents">${Ddto.qcontent }</div>

							<c:if test="${Ddto.aok==0 }">
								<p>답변이 없습니다. 기다려주세요.</p>
							</c:if>
							<c:if test="${Ddto.aok==1 }">
								<p>답변일 : ${Ddto.adate }</p>
								<div class="userContents">${Ddto.acontent }</div>
								<br />
							</c:if>

							<c:if test="${Ddto.aok==0 }">
								<a href="/pandago/qna.do?action=modify&qnaid=${Ddto.qnaid}">수정</a>
							</c:if>
							<input type="button" value="문의목록" id="btn_qna_list" /> <a
								href="/pandago/qna.do?action=delete&qnaid=${Ddto.qnaid}">삭제</a>
						</div>
					</div>
				</c:when>
			</div>
		</div>
	</div>
</body>
</html>

