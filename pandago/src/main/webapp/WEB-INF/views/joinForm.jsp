<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE>
<html>
<head>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/header_footer/header.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/join/join.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/join.js?4"></script>
</head>
<body>
<%@ include file="../includes/header.jsp" %>
	<div class="container">
		<div class="formBox">
			<div>
				<img src="${pageContext.request.contextPath}/resources/joinImage/tit_join.gif" alt="" />
				<span style="margin-bottom: 30px;">회원이 되셔서 회원등급할인/할인쿠폰/적립금 등 다양한 서비스를 받으세요</span>
			</div>
			
			<img src="${pageContext.request.contextPath}/resources/joinImage/join_img_top.gif" style="margin-bottom: 15px;" />
			<span><label for="p">회원유형</label><input type="radio" name="" id="p" checked="checked"/>개인</span>
			<input type="image" src="${pageContext.request.contextPath}/resources/joinImage/sns_bt_idpw_join.gif" style="margin-bottom: 20px;" onclick="pandaoJoin();"/>
			
			<%-- <div class="snsBtn">
				<ul>
					<li><input type="image" src="${pageContext.request.contextPath}/resources/loginImages/sns_bt_facebook_login.gif" /></li>
					<li><input type="image" src="${pageContext.request.contextPath}/resources/loginImages/sns_bt_kakao_login.gif" /></li>
					<li><input type="image" src="${pageContext.request.contextPath}/resources/loginImages/sns_bt_naver_login.gif" /></li>
				</ul>
			</div> --%>
			
		<%-- 	<img src="${pageContext.request.contextPath}/resources/joinImage/join_img_benefit.gif" alt="" /> --%>
		</div>
	</div>
	<%@ include file="../includes/footer.jsp"%>
</body>
</html>