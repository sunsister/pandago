<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="cdn.jsp" %>
<header>
	<ul id="orderdown" class="dropdown-content" style="margin-top: 64px;">
		<li><a href="#!">one</a></li>
		<li><a href="#!">two</a></li>
		<li><a href="#!">three</a></li>
	</ul>
	<ul id="productdown" class="dropdown-content" style="margin-top: 64px;">
		<li><a href="product.ad">상품리스트</a></li>
		<li><a href="#!">two</a></li>
		<li><a href="#!">three</a></li>
	</ul>
	<ul id="boarddown" class="dropdown-content" style="margin-top: 64px;">
		<li><a href="inquiry.ad">상품문의</a></li>
		<li><a href="qna.ad">1:1문의</a></li>
		<li><a href="#!">상품후기</a></li>
	</ul>
	<ul id="customerdown" class="dropdown-content" style="margin-top: 64px;">
		<li><a href="#!">one</a></li>
		<li><a href="#!">two</a></li>
		<li><a href="#!">three</a></li>
	</ul>
	<ul id="coupondown" class="dropdown-content" style="margin-top: 64px;">
		<li><a href="#!">one</a></li>
		<li><a href="#!">two</a></li>
		<li><a href="#!">three</a></li>
	</ul>
	<nav>
		<div class="nav-wrapper grey darken-4">
			<a href="#!" class="brand-logo logofont">PANDAGO</a>
			<a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
			<ul class="right hide-on-med-and-down">					
				<li>
					<a class="dropdown-button" href="#!" data-activates="orderdown">주문관리
						<i class="material-icons right">arrow_drop_down</i>
					</a>
				</li>
				<li>
					<a class="dropdown-button" href="#!" data-activates="productdown">상품관리
						<i class="material-icons right">arrow_drop_down</i>
					</a>
				</li>
				<li>
					<a class="dropdown-button" href="#!" data-activates="boarddown">게시판관리
						<i class="material-icons right">arrow_drop_down</i>
					</a>
				</li>
				<li>
					<a class="dropdown-button" href="#!" data-activates="customerdown">회원관리
						<i class="material-icons right">arrow_drop_down</i>
					</a>
				</li>
				<li>
					<a class="dropdown-button" href="#!" data-activates="coupondown">쿠폰관리
						<i class="material-icons right">arrow_drop_down</i>
					</a>
				</li>
			</ul>
			<ul class="side-nav" id="mobile-demo">
				<li><a href="sass.html">Sass</a></li>
				<li><a href="badges.html">Components</a></li>
				<li><a href="collapsible.html">Javascript</a></li>
				<li><a href="mobile.html">Mobile</a></li>
			</ul>
		</div>
		
	</nav>
</header>