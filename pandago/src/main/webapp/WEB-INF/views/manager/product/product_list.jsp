<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<style>
	img {
		width : 80px;
		height : 80px;
	}
</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script type="text/javascript">
	$(function(){
		$("#dev_modal_product").modal();
		$("#dev_btn_modifyOK").hide();
		$('select').material_select();
		$("#dev_modal_product_modify").hide();
		
		$(".dev_modal_open").on("click", function(){
			var pid = $(this).parent().find(".dev_ppid").html();
			var lcode = $(this).parent().find(".dev_plcode").html();
			var mcode = $(this).parent().find(".dev_pmcode").html();
			var scode = $(this).parent().find(".dev_pscode").html();
			
			$(".dev_modal_pid").html(pid);
			$(".dev_modal_category").html(lcode+' > '+mcode + ' > ' + scode);
			$(".dev_modal_pname").html($(this).parent().find(".dev_ppname").html());
			$(".dev_modal_factory").html($(this).parent().find(".dev_pfactory").html());
			$(".dev_modal_price").html($(this).parent().find(".dev_pprice").html());
			$(".dev_modal_sprice").html($(this).parent().find(".dev_psprice").html());
			$(".dev_modal_sale").html($(this).parent().find(".dev_psale").html());
			$(".dev_modal_regdate").html($(this).parent().find(".dev_pregdate").html());
			$(".dev_modal_scnt").html($(this).parent().find(".dev_pscnt").html());
			$(".dev_modal_star").html($(this).parent().find(".dev_pstar").html());
			$(".dev_modal_img").attr("src", $(this).parent().find(".dev_pimg").attr("src"));
			
 			$.ajax({
				url: '/pandago/product.ad',
				type: 'get',
				dataType:'json',
				data:"action=detail&pid="+pid,
				success: function(data){
					console.log(data);
					
					$(".dev_modal_detail").empty();
					$.each(data, function(index, value){
						$(".dev_modal_detail").append("<tr class='center'>");
						$(".dev_modal_detail").append("<td class='dev_modal_pdid'>"+value.pdid+"</td>");
						var option = "";
						if(value.color != null)
							option += value.color + " ";
						if(value.psize != null)
							option += value.psize;
						$(".dev_modal_detail").append("<td class='dev_modal_option'>"+option+"</td>");
						$(".dev_modal_detail").append("<td class='dev_modal_addprice'>"+value.addprice+"</td>");
						$(".dev_modal_detail").append("<td class='dev_modal_stock'>"+value.stock+"</td>");
						if(value.stock < 10){
							$(".dev_modal_detail").append("<td><span class='red new badge' data-badge-caption='재고부족'></span></td>")
						}
						//$("#dev_modal_detail").append("</td>")
						$(".dev_modal_detail").append("</tr>");
					});
					$("#dev_modal_product").modal('open');
				}
			});
		});
		
		$("#dev_btn_modify").on("click", function(){
			console.log("수정하기 버튼 클릭");			
			var ppname = $("#dev_modal_product_detail").find(".dev_modal_pname").html();
			$("#dev_modal_product_modify").find(".dev_modal_pname").html("<input id='pname' type='text' class='validate' value='"+ppname+"'>")
			
			var factory = $("#dev_modal_product_detail").find(".dev_modal_factory").html();
			$("#dev_modal_product_modify").find(".dev_modal_factory").html("<input id='factory' type='text' class='validate' value='"+factory+"'>")
			
			var price = $("#dev_modal_product_detail").find(".dev_modal_price").html();
			$("#dev_modal_product_modify").find(".dev_modal_price").html("<input id='price' type='text' class='validate' value='"+price+"'>")
			
			var sale = $("#dev_modal_product_detail").find(".dev_modal_sale").html();
			$("#dev_modal_product_modify").find(".dev_modal_sale").html("<input id='sale' type='text' class='validate' value='"+sale+"'>")

			$("#dev_btn_modify").hide();
			$("#dev_btn_modifyOK").show();
			$("#dev_modal_product_detail").hide();
			$("#dev_modal_product_modify").show();
		});
		
		$("#dev_btn_modifyOK").on("click", function(){
			console.log("수정완료 버튼 클릭");
			var pid = $(".dev_modal_pid").html();
			var pname = $("#dev_modal_product_modify").find(".dev_modal_pname").find("#pname").val();
			var price = Number($("#dev_modal_product_modify").find(".dev_modal_price").find("#price").val());
			var sprice = Number($("#dev_modal_product_modify").find(".dev_modal_sprice").html());
			var sale = Number($("#dev_modal_product_modify").find(".dev_modal_sale").find("#sale").val());
			var factory =  $("#dev_modal_product_modify").find(".dev_modal_factory").find("#factory").val();
			
			var data = "pid="+pid;
			data += "&pname="+pname;
			data += "&price="+price;
			data += "&sprice="+sprice;
			data += "&sale="+sale;
			data += "&factory="+factory;
			
			$.ajax({
				url: '/pandago/product.ad',
				type: 'post',
				dataType:'text',
				data:"action=modify&"+data,
				success: function(data){
					alert("상품정보가 업데이트되었습니다.");	
					$(".dev_modal_pname").html(pname);
					$(".dev_modal_factory").html(factory);
					$(".dev_modal_price").html(price);
					$(".dev_modal_sprice").html(sprice);
					$(".dev_modal_sale").html(sale);
					$("#dev_btn_modify").show();
					$("#dev_btn_modifyOK").hide();
					$("#dev_modal_product_detail").show();
					$("#dev_modal_product_modify").hide();
				}
			}); 			
		});
		
		$("#dev_select_lcode").on("change", function(){
			console.log("대분류 변경");
			var value = $("#dev_select_lcode").val();
			if(value == 'all'){
				$("#dev_select_mcode").attr("disabled", true);
				$("#dev_select_mcode").children("option[value='all']").addClass("selected");
				
			}
			console.log($("#dev_select_lcode").val());
		});
		
		$(".dev_modal_sale").keyup(function(){
			var price = Number($("#dev_modal_product_modify").find(".dev_modal_price").children("#price").val());
			var sale = Number($(this).children("#sale").val());
			var sprice = price *(1- sale*0.01);
			$("#dev_modal_product_modify").find(".dev_modal_sprice").html(sprice); 
		});
		
		$(".dev_modal_price").keyup(function(){
			var price = Number($(this).children("#price").val());
			var sale = Number($("#dev_modal_product_modify").find(".dev_modal_sale").children("#sale").val());
			var sprice = price *(1- sale*0.01);
			$("#dev_modal_product_modify").find(".dev_modal_sprice").html(sprice); 
		});
	});
</script>
<title>PANDAGO - ADMIN</title>
</head>
<body>
<%-- 	<jsp:include page="admin_header.jsp"></jsp:include> --%>
	<%@ include file="../admin_header.jsp" %>
	<main>
		<div class="container">
			<h4>상품리스트</h4>
			<div class="row">
				<div class="right">
					<div class="input-field col s4">
						<select id="dev_select_lcode">
							<option value="all" selected>전체선택</option>
							<c:forEach var="code" items="${codelist}">
								<option value="${code.ccode }">${code.codename }</option>
							</c:forEach>

						</select><label>대분류</label>
					</div>
					<div class="input-field col s4">
						<select id="dev_select_mcode">
							<option value="all" selected>전체선택</option>
							<option>test</option>
						</select><label>중분류</label>
					</div>
					<div class="input-field col s4">
						<select id="dev_select_scode">
							<option value="all" selected>전체선택</option>
						</select><label>소분류</label>
					</div>
				</div>
			</div>
		
			
			<table class="bordered highlight centered">
				<thead>
					<tr>
						<th>상품</th>
						<th>상품ID</th>
						<th>대분류코드</th>
						<th>중분류코드</th>
						<th>소분류코드</th>
						<th>상품명</th>
						<th>원가</th>
						<th>판매가</th>
						<th>할인율</th>
						<th>제조사</th>
						<th>판매수</th>
						<th>평균별점</th>
					</tr>
				</thead>
				<tbody>				
					<c:forEach var="list" items="${list }">
						<tr>
							<td><a href="view.do?no=${list.pid }"><img class="dev_pimg" src="${list.path }" alt="" /></a></td>
							<td class="dev_modal_open dev_ppid" >${list.pid }</td>
							<td class="dev_plcode">${list.lcode }</td>
							<td class="dev_pmcode">${list.mcode }</td>
							<td class="dev_pscode">${list.scode }</td>
							<td class="dev_modal_open dev_ppname">${list.pname }</td>
							<td class="dev_pprice">${list.price }</td>
							<td class="dev_psprice">${list.sprice }</td>
							<td class="dev_psale">${list.sale }</td>
							<td class="dev_pfactory">${list.factory }</td>
							<td class="dev_pscnt">${list.scnt }</td>
							<td class="dev_pstar">${list.star }</td>
							<td class="dev_pregdate" hidden="hidden">${list.regdate }</td>
							<%-- <td><a href="inquiry.ad?action=inlist&id=${list.inid }">${list.intitle }</a></td> --%>
						</tr>
					</c:forEach>
				</tbody>
			</table>
			<ul class="pagination center">
			    <li class="waves-effect"><a href="product.ad?cp=${currentPage-1}"><i class="material-icons">chevron_left</i></a></li>
			    <c:forEach begin="${beginPageNum }" end="${endPageNum }" step="1" var ="i">
			    	 <c:if test="${currentPage == i }">
			    	 	<li class="active"><a href="product.ad?cp=${i}">${i}</a></li>
			    	 </c:if>
			    	 <c:if test="${currentPage != i }">
			    	 	<li class="waves-effect"><a href="product.ad?cp=${i}">${i}</a></li>
			    	 </c:if>
			    </c:forEach>
			    <li class="waves-effect"><a href="product.ad?cp=${currentPage+1}"><i class="material-icons">chevron_right</i></a></li>
			</ul> 
		</div>
	
		<!-- Modal Structure -->
		<div id="dev_modal_product" class="modal modal-fixed-footer">
			<div id="dev_modal_product_detail">
				<div class="modal-content">
					<div class="row">
						<div class="col s6"><h5>상품 상세보기</h5></div>
					</div>
					<div class="row">
						<div class="col s4 center">
							<a href="view.do?no="><img class="dev_modal_img" src="" alt="상품이미지" /></a>
						</div>
						<div class="col s8">
							<table class="bordered">
								<tr>
									<td class="col s4">상품번호</td>
									<td class="col s8 dev_modal_pid">PID00001</td>
								</tr>
								<tr>
									<td class="col s4">카테고리</td>
									<td class="col s8 dev_modal_category">LCODE > MCODE > SCODE</td>
								</tr>
								<tr>
									<td class="col s4">상품명</td>
									<td class="col s8 dev_modal_pname">선풍기</td>
								</tr>
								<tr>
									<td class="col s4">제조사</td>
									<td class="col s8 dev_modal_factory">회사A</td>
								</tr>
								<tr>
									<td class="col s4">원가</td>
									<td class="col s8 dev_modal_price">50000</td>
								</tr>
								<tr>
									<td class="col s4">판매가</td>
									<td class="col s8 dev_modal_sprice">45000</td>
								</tr>
								<tr>
									<td class="col s4">할인율</td>
									<td class="col s8 dev_modal_sale">10</td>
								</tr>
								<tr>
									<td class="col s4">등록일</td>
									<td class="col s8 dev_modal_regdate">2017/04/25</td>
								</tr>
								<tr>
									<td class="col s4">판매수</td>
									<td class="col s8 dev_modal_scnt">10</td>
								</tr>
								<tr>
									<td class="col s4">평균별점</td>
									<td class="col s8 dev_modal_star">
										<i class="material-icons tiny">star</i>
										<i class="material-icons tiny">star</i>
										<i class="material-icons tiny">star</i>
										<i class="material-icons tiny">star</i>
										<span> 4.3 / 5</span>
									</td>
								</tr>
							</table>
						</div>
					</div>
					<div class="container">
						<span>상품세부상품</span>
						<table class="bordered centered">
							<thead>
								<tr>
									<th>상품세부ID</th>
									<th>색상/사이즈</th>
									<th>추가가격</th>
									<th>재고수량</th>
									<th> </th>
								</tr>
							</thead>
							<tbody class="dev_modal_detail">
								<tr>
									<td class="dev_modal_pdid">PDID0005</td>
									<td class="dev_modal_option">RED</td>
									<td class="dev_modal_addprice">5000</td>
									<td class="dev_modal_stock">5<span class="red new badge" data-badge-caption="재고부족"></span></td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			
			<div id="dev_modal_product_modify">
				<div class="modal-content">
					<div class="row">
						<div class="col s6"><h5>상품 업데이트</h5></div>
					</div>
					<div class="row">
						<div class="col s4 center">
							<a href="view.do?no="><img class="dev_modal_img" src="" alt="상품이미지" /></a>
						</div>
						<div class="col s8">
							<table class="bordered">
								<tr>
									<td class="col s4">상품번호</td>
									<td class="col s8 dev_modal_pid">PID00001</td>
								</tr>
								<tr>
									<td class="col s4">카테고리</td>
									<td class="col s8 dev_modal_category">LCODE > MCODE > SCODE</td>
								</tr>
								<tr>
									<td class="col s4">상품명</td>
									<td class="col s8 dev_modal_pname"></td>
								</tr>
								<tr>
									<td class="col s4">제조사</td>
									<td class="col s8 dev_modal_factory">회사A</td>
								</tr>
								<tr>
									<td class="col s4">원가</td>
									<td class="col s8 dev_modal_price">50000</td>
								</tr>
								<tr>
									<td class="col s4">판매가</td>
									<td class="col s8 dev_modal_sprice">45000</td>
								</tr>
								<tr>
									<td class="col s4">할인율</td>
									<td class="col s8 dev_modal_sale">10</td>
								</tr>
								<tr>
									<td class="col s4">등록일</td>
									<td class="col s8 dev_modal_regdate">2017/04/25</td>
								</tr>
								<tr>
									<td class="col s4">판매수</td>
									<td class="col s8 dev_modal_scnt">10</td>
								</tr>
								<tr>
									<td class="col s4">평균별점</td>
									<td class="col s8 dev_modal_star">
										<i class="material-icons tiny">star</i>
										<i class="material-icons tiny">star</i>
										<i class="material-icons tiny">star</i>
										<i class="material-icons tiny">star</i>
										<span> 4.3 / 5</span>
									</td>
								</tr>
							</table>
						</div>
					</div>
					<div class="container">
						<span>상품세부상품</span>
						<table class="bordered centered">
							<thead>
								<tr>
									<th>상품세부ID</th>
									<th>색상/사이즈</th>
									<th>추가가격</th>
									<th>재고수량</th>
									<th> </th>
								</tr>
							</thead>
							<tbody class="dev_modal_detail">
								<tr>
									<td class="dev_modal_pdid">PDID0005</td>
									<td class="dev_modal_option">RED</td>
									<td class="dev_modal_addprice">5000</td>
									<td class="dev_modal_stock">5<span class="red new badge" data-badge-caption="재고부족"></span></td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>

			<div class="modal-footer">
				<button id="dev_btn_close" class="modal-action modal-close waves-effect waves-green btn-flat ">닫기</button>
				<button id="dev_btn_modify" class="modal-action waves-effect waves-green btn-flat ">수정하기</button>
				<button id="dev_btn_modifyOK" class="modal-action waves-effect waves-green btn-flat ">수정완료</button>
			</div>
		</div>

	</main>
</body>
</html>
				
