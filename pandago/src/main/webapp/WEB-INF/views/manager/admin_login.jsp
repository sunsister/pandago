<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>PANDAGO - ADMIN</title>
</head>
<body>
	<%@ include file="admin_header.jsp" %>
	<main>
		<div class="container">
			<form action="loginOK.ad">
				<div class="card">
					<h1 class="logofont center">PANDAGO</h1>
					<div class="input-field col s6">
						<i class="material-icons prefix">account_circle</i> 
						<input id="admin_id" type="text" class="validate" name="id"> 
						<label for="admin_id">ID</label>
					</div>
					<div class="input-field col s6">
						<i class="material-icons prefix">account_circle</i> 
						<input id="admin_pw" type="password" class="validate" name="pw"> 
						<label for="admin_pw">PASSWORD</label>
					</div>
					<div class="">
						<button class="btn waves-effect waves-light btncolor" type="submit">LOGIN</button>
					</div>
				</div>
			</form>
		</div>
	</main>
</body>
</html>