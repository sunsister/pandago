<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/manager/manager.css">
<title>MIN TEST</title>
</head>
<body>
	<div class="container">
		<div class="login_box">
			<h1>PANDAGO</h1>
			<img onclick="message('Well Come');" class="bamboo1" src="${pageContext.request.contextPath}/resources/managerImage/Bamboo.png" alt="" />
			<img onclick="message('This Page : Admin');" class="bamboo2" src="${pageContext.request.contextPath}/resources/managerImage/Bamboo.png" alt="" />
			<img onclick="message('Are you Administor?');" class="bamboo3" src="${pageContext.request.contextPath}/resources/managerImage/Bamboo.png" alt="" />
			<div id="login">
				<h2> ADMIN LOGIN PAGE </h2>		
				<div id="center">	
					<ul>
						<li>
							<label for="id">ADMIN ID</label>
						</li>
						<li>
							<input type="text" name="admin_id" id="id" />
						</li>	
						<li>
							<label for="pw">PASSWORD</label>
						</li>
						<li>
							<input type="text" name="admin_pw" id="pw" />
						</li>
						
						<li>
							<input type="image" src="${pageContext.request.contextPath}/resources/managerImage/loginbtn.jpg" id="login"/>
						</li>
					</ul>
				</div>
			</div>
			<a onclick="imageLock();">Image Password</a>
			<a onclick="option();" style="position: absolute; right: 0; ">setting</a>
		</div>
	</div>
</body>
</html>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/systemManager.js"></script>