<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<style>
	img {
		width : 200px;
		height : 200px;
	}
</style>
<title>PANDAGO - ADMIN</title>
</head>
<body>
<%-- 	<jsp:include page="admin_header.jsp"></jsp:include> --%>
	<%@ include file="../admin_header.jsp" %>
	<main>
		<div class="container">
			<h4>상품문의 답변하기</h4>
			<div class="card">
				<p>상문문의ID : ${idto.inid}</p>
				<div class="row">
					<div class="col s3">
						<img src="${product.path }" alt=""/>
					</div>
					<div class="col s9">
						<p>상품번호 : ${product.pid }</p>
						<p>상품이름 : ${product.pname }</p>
					</div>
				</div>
				<div class="row">
					<div class="col s12"><p>상품문의내용</p></div>
					<div class="col s12"><h4>${idto.intitle }</h4></div>
					<div class="col s12"><p>${idto.incontent }</p></div>
				</div>
				<form action="inquiry.ad">
				<input type="hidden" name="action" value="editask" />
				<input type="hidden" name="inid" value="${idto.inid}" />
				<div class="row">
					<div class="col s12"><p>상품답변내용</p></div>
					<div class="col s12">
						<textarea id="textarea1" name="inacontent" class="materialize-textarea">${idto.inacontent }</textarea>
					</div>
				</div>
				<div class="card-action">
					<button class="btn" type="submit">답변완료</button>
              		<a class="btn" href="/pandago/inquiry.ad">취소</a>
            	</div>
            	</form>
			</div>
		</div>
	</main>
</body>
</html>
				
