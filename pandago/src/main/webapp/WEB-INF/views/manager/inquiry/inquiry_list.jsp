<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>PANDAGO - ADMIN</title>
</head>
<body>
<%-- 	<jsp:include page="admin_header.jsp"></jsp:include> --%>
	<%@ include file="../admin_header.jsp" %>
	<main>
		<div class="container">
			<h4>상품문의리스트</h4>
			<table class="bordered highlight">
				<thead>
					<tr>
						<th>NO</th>
						<th>상품ID</th>
						<th>제목</th>
						<th>고객ID</th>
						<th>작성일</th>
					</tr>
				</thead>
				<tbody>				
					<c:forEach var="list" items="${list }">
						<c:if test="${list.inaok == 0  }">
							<tr class="teal lighten-5">
						</c:if>
						<c:if test="${list.inaok != 0  }">
							<tr>
						</c:if>
						
							<td>-</td>
							<td>${list.pid }</td>
							<td><a href="inquiry.ad?action=inlist&id=${list.inid }">${list.intitle }</a></td>
							<td>${list.cid }</td>
							<td>${list.indate }</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
			<ul class="pagination">
			    <li class="waves-effect"><a href="inquiry.ad?cp=${currentPage-1}"><i class="material-icons">chevron_left</i></a></li>
			    <c:forEach begin="${beginPageNum }" end="${endPageNum }" step="1" var ="i">
			    	 <c:if test="${currentPage == i }">
			    	 	<li class="active"><a href="inquiry.ad?cp=${i}">${i}</a></li>
			    	 </c:if>
			    	 <c:if test="${currentPage != i }">
			    	 	<li class="waves-effect"><a href="inquiry.ad?cp=${i}">${i}</a></li>
			    	 </c:if>
			    </c:forEach>
			    <li class="waves-effect"><a href="inquiry.ad?cp=${currentPage+1}"><i class="material-icons">chevron_right</i></a></li>
			</ul>
		</div>
	</main>
</body>
</html>
				
