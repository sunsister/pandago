<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<style>
	img {
		width : 200px;
		height : 200px;
	}
</style>
<title>PANDAGO - ADMIN</title>
</head>
<body>
<%-- 	<jsp:include page="admin_header.jsp"></jsp:include> --%>
	<%@ include file="../admin_header.jsp" %>
	<main>
		<div class="container">
			<h4>상품문의 답변하기</h4>
			<div class="card">
				<p>QNA ID : ${dto.qnaid}</p>
				<div class="row">
					<div class="col s9">
						<p>분류 : ${dto.qcode }</p>
						<p>문의일 : ${dto.qdate }</p>
					</div>
				</div>
				<div class="row">
					<div class="col s12"><p>문의내용</p></div>
					<div class="col s12"><h4>${dto.title }</h4></div>
					<div class="col s12"><p>${dto.qcontent }</p></div>
				</div>
				<form action="qna.ad">
				<input type="hidden" name="action" value="qnaAsk" />
				<input type="hidden" name="qnaid" value="${dto.qnaid}" />
				<div class="row">
					<div class="col s12"><p>답변내용</p></div>
					<div class="col s12">
						<textarea id="textarea1" name="acontent" class="materialize-textarea">${dto.acontent }</textarea>
					</div>
				</div>
				<div class="card-action">
					<button class="btn" type="submit">답변완료</button>
              		<a class="btn" href="/pandago/qna.ad">취소</a>
            	</div>
            	</form>
			</div>
		</div>
	</main>
</body>
</html>