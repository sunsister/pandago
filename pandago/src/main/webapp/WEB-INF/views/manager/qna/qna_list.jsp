<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>PANDAGO - ADMIN</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script type="text/javascript">
	$(function(){
		$('#dev_qselect').material_select();
		
		$("#dev_qselect").on("change", function(){
			var qcode = $("#dev_qselect").val();
			location.href="/pandago/qna.ad?qcode="+qcode;
		});
	});
</script>
</head>
<body>
<%-- 	<jsp:include page="admin_header.jsp"></jsp:include> --%>
	<%@ include file="../admin_header.jsp" %>
	<main>
		<div class="container">
			<div class="row">
				<div class="col s10"><h4>QnA리스트</h4></div>
				<div class="col s2">
					 <div class="input-field col s12">
					    <select id="dev_qselect">
					    	<option value="" selected>전체보기</option>
					    	<option value="배송관련">배송관련</option>
					    	<option value="취소/교환/반품">취소/교환/반품</option>
					    </select>
					    <label>분류</label>
					  </div>
				</div>
			</div>
			<table class="bordered highlight">
				<thead>
					<tr>
						<th>NO</th>
						<th>분류</th>
						<th>제목</th>
						<th>고객ID</th>
						<th>작성일</th>
					</tr>
				</thead>
				<tbody id="dev_listbody">				
					<c:forEach var="list" items="${list }">
						<c:if test="${list.aok == 0  }">
							<tr class="teal lighten-5">
						</c:if>
						<c:if test="${list.aok != 0  }">
							<tr>
						</c:if>
						
							<td>-</td>
							<td>${list.qcode }</td>
							<td><a href="qna.ad?action=qadetail&id=${list.qnaid }">${list.title }</a></td>
							<td>${list.cid }</td>
							<td>${list.qdate }</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
			<ul class="pagination center">
			    <li class="waves-effect"><a href="qna.ad?cp=${currentPage-1}"><i class="material-icons">chevron_left</i></a></li>
			    <c:forEach begin="${beginPageNum }" end="${endPageNum }" step="1" var ="i">
			    	 <c:if test="${currentPage == i }">
			    	 	<li class="active"><a href="qna.ad?cp=${i}">${i}</a></li>
			    	 </c:if>
			    	 <c:if test="${currentPage != i }">
			    	 	<li class="waves-effect"><a href="qna.ad?cp=${i}">${i}</a></li>
			    	 </c:if>
			    </c:forEach>
			    <li class="waves-effect"><a href="qna.ad?cp=${currentPage+1}"><i class="material-icons">chevron_right</i></a></li>
			</ul>
		</div>
	</main>
</body>
</html>
				
