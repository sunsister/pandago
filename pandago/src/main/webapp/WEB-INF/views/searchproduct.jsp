<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, width=device-width, height=device-height">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/header_footer/header.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/search/search.css?32">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/category.js?3"></script>
<script type="text/javascript">
page = 3;
</script>
</head>
<body>
<input type="hidden" name="" id="currentPage" value="${currentPage}" />
	<input type="hidden" name="" id="scntnum" value="0" />
	<%@ include file="../includes/header.jsp"%>
	<input type="hidden" name="" id="currentPage" value="${currentPage}" />
	<div class="category_container">
		<div class="cate_box">
			<div class="filterdiv">
				<input type="hidden" name="" id="lfilter" value="${lcode}" />
				<input type="hidden" name="" id="mfilter" />
				<input type="hidden" name="" id="sfilter" />
			</div>
			<div class="category_contents">
				<div class="category_contents_pruduct">
					<div class="option_box" id="option_box">
					
					<div >총 <span id="countbox">${count}</span>개 상품있다</div>
								
					<ul class="box_1">								
						<li><a href="javascript:;" id="star" >인기순</a></li>
						<li><a href="javascript:;" id="regdate" >최근등록순</a></li>
						<li><a href="javascript:;" id="scnt" >판매인기순</a></li>
						<li><a href="javascript:;" id="lowprice" >낮은가격순</a></li>
						<li><a href="javascript:;" id="hiprice" >높은가격순</a></li>
					</ul>
					<!-- <ul class="box_2">
						<li><select name="cntPerPage" id="">
								<option value="20">20개</option>
								<option value="40">40개</option>
								<option value="80">80개</option>
								<option value="160">160개</option>
						</select></li>
					</ul>
					<ul class="box_3">
						<li><button>1</button></li>
						<li><button>2</button></li>
						<li><button>3</button></li>
					</ul> -->
					</div>
					<div class="pruduct_list" id="pruduct_list_ajax">
					<c:forEach var="product" items="${list}">
							<div class="list">
								<a href="view.do?no=${product.pid}"> <img src="${product.path}" alt=""
									width=100px; height="150px" />
								</a> <a href="view.do?no=${product.pid}"> <span>${product.factory}</span>
								</a> <a href="view.do?no=${product.pid}"> <span>${product.pname}</span>
								</a> <a href="view.do?no=${product.pid}"> <span>${product.price}원</span>
								</a>
							</div>
					</c:forEach>
						
					</div>
					<!-- pruduct_list end -->
					<div id="pageDiv" style="float: left; width: 1000px; text-align: center;">
                    	<c:forEach begin="${beginpagenum}" end="${endpagenum}" var="i">
		                  	<c:choose>
			                  	<c:when test="${currentPage==i }">
			                  		<a href="javascript:;" onclick="pagechange(${i });" style="color: red" > ${i }</a>	
			                  	</c:when>
			                  	<c:otherwise>
			                  		<a href="javascript:;" onclick="pagechange(${i });" > ${i }</a>
			                  	</c:otherwise>
		                  </c:choose>
                  		</c:forEach>
                  		
               		</div><!-- DIV .pageDiv end --> 
				</div>
				<!-- category_contents_pruduct end  -->
			</div>
			<!-- category_contents end  -->
		</div>
		<!-- cate_box end  -->
	</div>
	<!-- category_container end -->
	<%@ include file="../includes/footer.jsp"%>
</body>
</html>