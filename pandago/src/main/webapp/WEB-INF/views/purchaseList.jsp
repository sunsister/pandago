<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE>
<html>
<head>
<style type="text/css">
body{ width: 100%;}
table {
	width:98%;
	margin: 0 auto;
	text-align: center;
}
table, tr{
	border-top: 1px #4d90fe solid;
   	border-bottom: 1px #4d90fe solid;
   	border-collapse: collapse;
}
img {
	width: 130px;
	height: 130px;
}
</style>
<link rel="stylesheet" type="text/css"	href="/pandago/resources/css/header_footer/header.css?1">
<link rel="stylesheet" type="text/css"	href="/pandago/resources/css/mypage/mypage.css?3">
<script	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script type="text/javascript"	src="${pageContext.request.contextPath}/resources/js/mypage.js?2"></script>
<script type="text/javascript">
	$(function() {
		$(".btnitem").on("click", function() {
			
			var payid = $(this).parent().children(".payid").val();
			console.log(payid);

			var frm = document.createElement("form");
			frm.setAttribute("method", "post");
			frm.setAttribute("action", "/pandago/purchase.do");

			document.body.appendChild(frm);

			var input_id = document.createElement("input");
			input_id.setAttribute("type", "hidden");
			input_id.setAttribute("name", "payid");
			input_id.setAttribute("value", payid);
			frm.appendChild(input_id);

			input_id = document.createElement("input");
			input_id.setAttribute("type", "hidden");
			input_id.setAttribute("name", "ac");
			input_id.setAttribute("value", "paysearch");
			frm.appendChild(input_id);

			frm.submit();

		});

		$(".btncancel").on("click", function() {
			console.log("주문취소")
			var payid = $(this).parent().children(".payid").val();
			console.log(payid);

			var frm = document.createElement("form");
			frm.setAttribute("method", "post");
			frm.setAttribute("action", "/pandago/purchase.do");

			document.body.appendChild(frm);

			var input_id = document.createElement("input");
			input_id.setAttribute("type", "hidden");
			input_id.setAttribute("name", "payid");
			input_id.setAttribute("value", payid);
			frm.appendChild(input_id);

			input_id = document.createElement("input");
			input_id.setAttribute("type", "hidden");
			input_id.setAttribute("name", "ac");
			input_id.setAttribute("value", "paycancel");
			frm.appendChild(input_id);

			frm.submit();

		});
		
		
		/* CSS */
		$(".pay_tr").mouseenter(function(){
			$(this).css("background-color", "rgba(178, 204, 255, 0.5)");
		});
		$(".pay_tr").mouseleave(function(){
			$(this).css("background", "white");
		});
	});

	
</script>
</head>
<body>
	<%@ include file="../includes/header.jsp"%>
	<div class="mypage_contanier">
		<div class="mypage_box">
			<%@ include file="../includes/mypage_sidemenu.jsp"%>
			<div class="mypage_menu_contents">
				<div class="mypage_menu_contents_loc">
					<h2>주문배송조회</h2>
					<div class="info">
	
						<table style="float: left;" class="pc_table">
							<tr>
								<th style="width: 150px;">결제 번호</th>
								<th style="width: 150px;">상품명 및 개수/가격</th>
								<th>배송확인</th>
								<th>결제방법</th>
								<th>결제일</th>
								<th>결제상태</th>
							</tr>
							<c:forEach var="pay" items="${paylist }">
								<tr class="pay_tr">
									<td>${pay.payid }</td>
									<td style="width: 150px;"><c:forEach var="list"
											items="${list }">
	
											<ul>
												<c:if test="${list.payid==pay.payid }">
	
													<li><img src="${list.path }" alt="" />
														<p>${list.pname }</p>
														<p>${list.cnt }개/<fmt:formatNumber value="${list.price }" type="number"/>원</p></li>
												</c:if>
											</ul>
										</c:forEach></td>
	
									<td rowspan="${pcnt.pay.payid }"><c:if
											test="${pay.ostate ne '주문취소' }">
	
											<p>
												<input type="button" value="배송조회" class="btnitem" /> <input
													type="button" value="주문취소" class="btncancel" /> <input
													type="hidden" class="payid" value="${pay.payid }" />
											</p>
	
										</c:if> <c:if test="${pay.ostate eq '주문취소' }">
	
											<p></p>
										</c:if></td>
									<td>${pay.pmethod }</td>
									<td>${pay.pdate }</td>
									<td>${pay.ostate }</td>
								</tr>
							</c:forEach>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	<%@ include file="../includes/footer.jsp"%>
</body>
</html>