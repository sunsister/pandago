<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet" type="text/css" href="/pandago/resources/css/header_footer/header.css?1">
<link rel="stylesheet" type="text/css" href="/pandago/resources/css/mypage/mypage.css?1">
</head>
<body>
	<%@ include file="../includes/header.jsp" %>

	<div>
		<h3>
			<a href="/pandago/mypage.do"> 
				<span>MY PAGE</span>
			</a>
		</h3>
		<ul class="side_menu_ul">
			<li>
				<a href="/pandago/mypage/qna.do?action=list"> <span>1:1 문의</span></a>
			</li>
		</ul>
	</div>
</body>
</html>
				
