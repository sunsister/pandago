<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet" type="text/css" href="/pandago/resources/css/header_footer/header.css?2">
<link rel="stylesheet" type="text/css" href="/pandago/resources/css/mypage/mypage.css?6">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script type="text/javascript">
	$(function() {
		$("#btn_qna_write").on("click", function() {
			location.href = "/pandago/qna.do?action=write";
		});
	});
</script>
</head>
<body>
	<%@ include file="../includes/header.jsp"%>
	
	<div class="mypage_contanier">
		<div class="mypage_box">
			<%@ include file="../includes/mypage_sidemenu.jsp"%>

			<div class="mypage_menu_contents"
				style="float: left; width: 80%; padding: 10px; margin-top: 75px;">
				<div class="qnaContainer">
					<c:choose>
						<c:when test="${list != null }">
							<div class="qnaList">
								<div>
									<h3>1:1문의 리스트</h3>
									<!-- 번호 / 제목 / 답변여부 / 작성일   -->
									<input type="button" value="문의글작성" id="btn_qna_write" />
								</div>
								<ul>
									<li>번호</li>
									<li class="title">제목</li>
									<li>답변여부</li>
									<li>작성일</li>
								</ul>
								<c:forEach var="dto" items="${list }">
									<ul>
										<li>${dto.qnaid}</li>
										<li class="title"><a
											href="/pandago/qna.do?action=detail&qnaid=${dto.qnaid}">${dto.title }</a></li>
										<li>${dto.qdate}</li>
										<li>${dto.aok}</li>
									</ul>
								</c:forEach>
							</div>
						</c:when>





					</c:choose>
				</div>
				<!-- DIV qnaContainer end  -->
			</div>
		</div>
	</div>

	<!--    </div>DIV contents_box end   
            </div>DIV mypage_menu_contents end
         </div> DIV mypage_box end 
      </div>  DIV mypage_contanier end -->
</body>
</html>

