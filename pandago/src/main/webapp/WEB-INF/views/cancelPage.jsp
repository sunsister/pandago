<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>

<html>
<link rel="stylesheet" type="text/css"
	href="/pandago/resources/css/header_footer/header.css?1">
<link rel="stylesheet" type="text/css"
	href="/pandago/resources/css/mypage/mypage.css?1">
<head>

<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

</head>
<body>
	<%@ include file="../includes/header.jsp"%>
	<div class="mypage_contanier">
		<div class="mypage_box">
			<%@ include file="../includes/mypage_sidemenu.jsp"%>
			<div class="mypage_menu_contents"
				style="float: left; width: 80%; padding: 10px; margin-top: 75px;">
				
				<h1>주문취소가 접수되었습니다</h1>
				<h1>귀하의 이메일 주소로 주문취소 메일을 발송 하였습니다.</h1>
				<a href="/pandago">메인으로</a>	
			</div>
		</div> 
	</div>
	<%@ include file="../includes/footer.jsp"%>
</body>
</html>