<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/header_footer/header.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/login/login.css?1">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script type="text/javascript">
function login(){	
	console.log("LOGIN");
	var email= $("#id").val();
	var password= $("#pw").val();
	$.ajax({
		url: 'loginOk',
		type: 'post',
		dataType:'text',
		data:"email="+email+"&pw="+password,
		success: function(data){
			 if(data=="false"){
				alert("이메일 혹은 비밀번호 오류입니다. ");				
			}else if(data=="true"){			
				alert("로그인 완료");
				location.href = document.referrer;
			} 
		}
	}); 
}
</script>
</head>
<body ondragstart="return false" >
<%@ include file="../includes/header.jsp" %>
<div class="container">
	<div class="login_box">
		<div class="title" style="text-align: center">
			<h2>Login-in</h2>
			<span>로그인</span>
			<span>회원 로그인을 하시면 각종 혜택 및 정보를 받으실 수 있습니다.</span>
		</div>	
		<div class="form">
		<div class="login">
			<form action="loginCheck" method="POST">
				<div class="box">
					<ul class="input_ul">
						<li><label for="id">아이디</label></li>				<li> <input type="text" name="id" id="id" placeholder="아이디"/> </li>
						<li><label for="pw">비밀번호</label></li>			<li> <input type="password" name="pw" id="pw" placeholder="비밀번호"/> </li>
					</ul>
				</div>		
					
				<div class="button">
					<input type="image" src="${pageContext.request.contextPath}/resources/loginImages/btn_login.gif" onclick="login(); return false;"/>
				</div>
					
				<ul class="option">
<!-- 					<li><input type="checkbox" name="save" id="save" /><label for="save">아이디 저장 | </label></li> -->
					<li><a href="join">회원가입 | </a></li>
					<li><a href="searchpw">아이디/비밀번호찾기</a></li>
				</ul>		
			</form>
				<%-- <div class="otherLogin">
					<ul>
						<li><input type="image" src="${pageContext.request.contextPath}/resources/loginImages/sns_bt_facebook_login.gif" /></li>
						<li><input type="image" src="${pageContext.request.contextPath}/resources/loginImages/sns_bt_kakao_login.gif" /></li>
						<li><input type="image" src="${pageContext.request.contextPath}/resources/loginImages/sns_bt_naver_login.gif" /></li>
					</ul>
				</div> --%>
			</div>						
		</div>
	</div>
</div>
<%@ include file="../includes/footer.jsp"%>
</body>
</html>





