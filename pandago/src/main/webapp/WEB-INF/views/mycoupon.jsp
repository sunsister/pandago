<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet" type="text/css" href="/pandago/resources/css/header_footer/header.css?3">
<link rel="stylesheet" type="text/css" href="/pandago/resources/css/mypage/mypage.css?2">
<style>
	table {
		border-top : 1px rgb(77,144,254) solid;
		border-bottom : 1px rgb(77,144,254) solid;
		width:98%;
		margin: 0 auto;
	}
	
	tr:first-child{
		border-bottom : 1px rgb(77,144,254) solid;
	}
	
	table, tr {
		border-collapse: collapse;
		height: 40px;
		text-align: center;
		/* 테이블 모서리 둥글게 */
		/* border-collapse: separate; 
		border-radius: 5px; */
	}
	
	td{
		padding : 10px;
	}
	
	select{
		margin-bottom: 5px;
    	height: 30px;
	}
	
	#dev_select {
		display: block;
    	float: right;
    	margin-right: 15px;
    	margin-top: 25px;
	}
	
</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script type="text/javascript">
	$(function(){
		$("#dev_select").on("change",function(){
			var used = $(this).val();
			var data = "";
			if(used == 2){
				data ="action=cplist";
			}else{
				data = "action=cpused&used="+used;
			}
			
			$.ajax({
				url : "/pandago/mycoupon.do",
				type : "GET",
				data : data,
				dataType : "json",
				success : function(data) {
					var tr = "";
 					$("#dev_cptable").empty();
 					tr += "<tr>";
					tr += "	<th>쿠폰명</th>";
					tr += "	<th>할인가격</th>";
					tr += "	<th>만료일/사용일</th>";
					tr += "	<th>결제최소금액</th>";
					tr += "</tr>";  

					data.forEach(function(dto, data){
						var edate = dto.edate;
						var cpdate = dto.cpdate
						if(edate == null)
							edate = "";
						if(cpdate == null)
							cpdate= "";
						
						if(cpdate == null || cpdate == ''){
							tr += "<tr class='coupon_item_notused'>";
							tr += "<td>"+dto.cpname+"</td>";
							tr += "<td>"+dto.cpprice+"</td>";
							tr += "<td>"+edate+"</td>";
							tr += "<td>"+dto.minprice+"</td>";
							tr += "</tr>"; 
						}else {
							tr += "<tr class='coupon_item_used'>";
							tr += "<td>"+dto.cpname+"</td>";
							tr += "<td>"+dto.cpprice+"</td>";
							tr += "<td>"+cpdate+"</td>";
							tr += "<td>"+dto.minprice+"</td>";
							tr += "</tr>"; 
						}
					});
					$("#dev_cptable").append(tr);
					$(".coupon_item_used").css("text-decoration", "line-through");
				},
				error : function(jqXHR, textStatus, errorThrown) {
					alert("에러 "+ textStatus+ " : "+ errorThrown);
				}
			});		
		});
		
		/* CSS */
		$(".coupon_item").mouseenter(function(){
			$(this).css("background-color", "rgba(178, 204, 255, 0.5)");
			
		});
		$(".coupon_item").mouseleave(function(){
			$(this).css("background", "white");
		});
		
		$(".coupon_item_used").css("text-decoration", "line-through");
	});
</script>
</head>
<body>
	<%@ include file="../includes/header.jsp" %>
	<div class="mypage_contanier">
		<div class="mypage_box">
			<%@ include file="../includes/mypage_sidemenu.jsp" %>

			<div class="mypage_menu_contents">
				<div class="mypage_menu_contents_loc">
					<div>
						<h2 style="width:80%; float: left;">MY COUPON</h2>
						<select id="dev_select">
							<option value="2">전체보기</option>
							<option value="0">유효쿠폰</option>
							<option value="1">사용/기간만료쿠폰</option>
						</select>
					</div>
						
					<table id="dev_cptable">
						<tr>
							<th>쿠폰명</th>
							<th>할인가격</th>
							<th>만료일/사용일</th>
							<th>결제최소금액</th>
						</tr>
							<c:forEach var="dto" items="${list }">
								<c:if test="${dto.cpdate eq null}">
									<tr class="coupon_item_notused">
										<td>${dto.cpname }</td>
										<td>${dto.cpprice }</td>
										<td>${dto.edate }</td>
										<td>${dto.minprice }</td>
									</tr>
								</c:if>
								<c:if test="${dto.cpdate ne null}">
									<tr class="coupon_item_used">
										<td>${dto.cpname }</td>
										<td>${dto.cpprice }</td>
										<td>${dto.cpdate }</td>
										<td>${dto.minprice }</td>
									</tr>
								</c:if>
							
	<%-- 							<tr class="coupon_item">
									<td>${dto.cpname }</td>
									<td>${dto.cpprice }</td>
									<c:if test="${dto.cpdate eq null}">
										<td>${dto.edate }</td>
									</c:if>
									<c:if test="${dto.cpdate ne null}">
										<td>${dto.cpdate }</td>
									</c:if>
									<td>${dto.edate}${dto.cpdate }</td>
									<td>${dto.minprice }</td>
								</tr> --%>
							</c:forEach>
					</table>
				</div>
			</div>
		</div>
	</div>
	<%@ include file="../includes/footer.jsp"%>
</body>
</html>
				
