<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<link rel="stylesheet" type="text/css"
	href="/pandago/resources/css/header_footer/header.css?1">
<link rel="stylesheet" type="text/css"
	href="/pandago/resources/css/mypage/mypage.css?1">
<style type="text/css">
table {
	border-top: 1px rgb(77, 144, 254) solid;
	border-bottom: 1px rgb(77, 144, 254) solid;
	width: 98%;
	margin: 0 auto;
}

table, tr {
	border-collapse: collapse;
	height: 40px;
	text-align: center;
	/* 테이블 모서리 둥글게 */
	/* border-collapse: separate; 
		border-radius: 5px; */
}

td {
	padding: 10px;
}
</style>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
</head>
<body>
	<%@ include file="../includes/header.jsp"%>
	<div class="mypage_contanier">
		<div class="mypage_box">
			<%@ include file="../includes/mypage_sidemenu.jsp"%>
			<div class="mypage_menu_contents"
				style="float: left; width: 80%; padding: 10px; margin-top: 75px;">
				<h3>상품배송조회</h3>

				<div>
				<table>
				<c:forEach var="plist" items="${plist }">
					
				
						
				
						<tr> 
							<td><div class="my-ship track-head title"><h2>${plist.pdate }</h2></div></td>
						</tr>	
						<tr>	<td><div class="my-ship track-head content">고객님의 상품은 ${plist.ostate } 상태 입니다.</div></td>
						
						</tr>
						
						
						<tr>
							<td><img src="http://www.quartershop.co.kr/data/skin/default/images/design/img_order_step.gif" alt="" /></td>
						</tr>	
					
				
				</c:forEach>
				</table>		
					<table>
						<tr>
							<td>주문자 아이디</td>
							<td>주문상태</td>
							<td>주소</td>
							<td>받는사람</td>
						</tr>
						<tr>
							<c:forEach var="plist" items="${plist }">
								<td>${plist.cid }</td>
								<td>${plist.ostate }</td>
								<td>${plist.addr }</td>
								<td>${plist.name }</td>
							</c:forEach>
						</tr>
					</table>
				</div>
			</div>
		</div>
	</div>
</body>
</html>