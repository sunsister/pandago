<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %> 
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, width=device-width, height=device-height">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/header_footer/header.css?1">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/category/category.css?1">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/category.js?34123"></script>
</head>
<body>	
	<%@ include file="../includes/current_view.jsp" %>
	<%@ include file="../includes/header.jsp"%>
	
	<input type="hidden" name="" id="currentPage" value="${currentPage}" />
	<input type="hidden" name="" id="scntnum" value="0" />
	
	<div class="category_container">
		<div class="cate_box">
			<div class="category_List">
				<ul class="category_List_sector">
					<li class="category_List_title" id="top">
						<a href="#"><label>&nbsp;&nbsp;${lcodename }</label></a>
					</li>
				
					<c:forEach var="cl1" items="${clist1}">
						<c:set var="cl1name" value="${fn:replace(cl1.codename,'*', '&')}"></c:set>
							<li class="category_List_subject">
								<a href="javascript:;"><label for="${cl1.ccode }">&nbsp;&nbsp;${cl1name }</label></a>
								<button class="sub_menu" id=${cl1.ccode } type="button"	onclick="dropDown(this);">▼</button>
							</li>
					
							<li class="category_List_subject_child hide ${cl1.ccode }">
								<a href="javascript:;"><span>&nbsp;전체보기</span></a>
								<input type="hidden" name="" class="lscode" id="${lcode}name" value="${lcode}" />
								<input type="hidden" name="" class="mscode" id="${cl1.ccode }name" value="${cl1.ccode}" />
							</li>
							
							<c:forEach var="cl2" items="${clist2}">
								<c:if test="${cl1.codename eq cl2.codename}">
									<c:set var="cl2name" value="${fn:replace(cl2.lcodename,'*', '&')}"></c:set>
										<li class="category_List_subject_child hide ${cl2.ccode }">
											<a href="javascript:;" id="${cl2.lccode }"> <span>&nbsp;&nbsp;${cl2name }</span></a>
											<input type="hidden" name="" class="lscode" id="${lcode}name" value="${lcode}" />
											<input type="hidden" name="" class="mscode" id="${cl1.ccode }name" value="${cl1.ccode}" />
											<input type="hidden" name="" class="sscode" id="${cl2.lccode }name" value="${cl2.lccode }" />
										</li>
								</c:if>
							</c:forEach>
					</c:forEach>
				</ul><!-- UL .category_List_sector end -->
			</div><!-- DIV .category_List end -->
			
			<div class="filterDiv">
				<input type="hidden" name="" id="lfilter" value="${lcode}" />
				<input type="hidden" name="" id="mfilter" />
				<input type="hidden" name="" id="sfilter" />
			</div><!-- DIV .filterDiv end  -->
			
			<div class="category_contents" style="text-align: right;">
				<p class="category_contents_navy" id="category_contents_navy">
					<a href="#">HOME ></a> 
					<a href="#">${lcodename }</a> 
				</p>

				<div class="category_contents_pruduct">
					<img src="${mimg}" alt="category Image"/>
					<div class="option_box" id="option_box">  
	               		<div class="countbox">
	               			총 <span id="countbox">${count}</span>개 상품
	               		</div>
	                        
	              		 <ul class="box_1">                        
	                 		 <li><a href="javascript:;" id="star" >인기순</a></li>
	                 		 <li><a href="javascript:;" id="regdate" >최근등록순</a></li>
	                 		 <li><a href="javascript:;" id="scnt" >판매인기순</a></li>
	                 		 <li><a href="javascript:;" id="lowprice" >낮은가격순</a></li>
	                  		 <li><a href="javascript:;" id="hiprice" >높은가격순</a></li>
	               		</ul>
             		</div><!-- DIV .option_box end -->

               		<div class="pruduct_list" id="pruduct_list_ajax">
	                	<c:forEach var="product" items="${list}">
	                    	<div class="list">
	                        	<a href="view.do?no=${product.pid}"> 
	                        		<img src="${product.path}" alt="" width=100px; height="150px" />
	                        	</a>
	                        	
	                       		<a href="view.do?no=${product.pid}">
	                       			<span>${product.factory}</span>
	                       		</a>
	                       		
	                       		<a href="view.do?no=${product.pid}">
	                       			<span>${product.pname}</span>
	                       		</a>
	                       		
	                       		<a href="view.do?no=${product.pid}">
	                       			<span><fmt:formatNumber value="${product.price}" type="number"/>원</span>
	                        	</a>
	                    	</div><!-- DIV .list end  -->
	                  </c:forEach>   
              		</div><!--  DIV .pruduct_list end -->
               
					<div id="pageDiv">
                    	<c:forEach begin="${beginpagenum}" end="${endpagenum}" var="i">
		                  	<c:choose>
			                  	<c:when test="${currentPage==i }">
			                  		<a href="javascript:;" onclick="pagechange(${i });" style="color: red" > ${i }</a>	
			                  	</c:when>
			                  	<c:otherwise>
			                  		<a href="javascript:;" onclick="pagechange(${i });" > ${i }</a>
			                  	</c:otherwise>
		                  </c:choose>
                  		</c:forEach>
               		</div><!-- DIV .pageDiv end --> 
				</div><!--  DIV .category_contents_pruduct end  -->
			</div><!-- DIV .category_contents end  -->		
		</div><!-- DIV .cate_box end  -->		
	</div><!-- DIV .category_container end -->
	<%@ include file="../includes/footer.jsp"%>
</body>
</html>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.js"></script>