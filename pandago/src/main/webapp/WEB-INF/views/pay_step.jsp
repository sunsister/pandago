<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/css/header_footer/header.css?2">
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/css/pay/pay.css">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/pay.js?2"></script>
<style>
/* body { font-family: '맑은 고딕';} */
img {
	width: 100px;
	height: 100px;
}

tr {
	border: 1px solid black;
}
</style>
</head>
<body>
	<%@ include file="../includes/header.jsp"%>
	<div class="container">
		<form id="dev_payFrm">
			<h2>결제하기</h2>
			<div class="paydiv">
				<table>
					<tr>
						<th colspan="2">상품.옵션정보</th>
						<th>수량</th>
						<th>상품금액</th>
						<th>결제금액</th>
					</tr>
					<c:forEach var="order" items="${orderlist }">
						<tr class="dev_order">
							<td><a href="view.do?no=${order.pid }"> <img
									src="${order.dimg }" alt="" /> <input type="hidden" name="pid"
									class="dev_pid" value="${order.pid }" /> <input type="hidden"
									name="pdid" class="dev_pdid" value="${order.pdid }" />
							</a></td>
							<td>
								<h4>${order.pname }</h4>
								<h4>${order.pdoption }</h4>
							</td>
							<td width="100" style="text-align: center;"><label
								class="dev_cnt">${order.pdcount }</label></td>
							<td><span class="dev_iprice">${order.price+order.addprice }</span>
							</td>
							<td width="120" style="text-align: center;">
								<h3 class="dev_item_totalprice">${order.pdcount*(order.price+order.addprice) }</h3>
							</td>
						</tr>
					</c:forEach>
					<tr>
						<!-- 전체합계가격 표시하기 -->
						<td colspan="5" style="text-align: right;"><h1
								id="dev_all_price">${pdcount*(price+addprice) }</h1></td>
					</tr>
				</table>
			</div>
			<!-- -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------  -->
			<div class="paydiv">
			<h3>쿠폰/포인트사용</h3>
			<table>
				<tbody>

					<tr>
						<td>
							<table style="width:550px;">
							
								<tr>
								<td>쿠폰할인</td>
								<td colspan="2">
									<ul>
										<c:forEach var="cp" items="${cplist}">
											<li>
												<input type="hidden" class="cpid" value="${cp.cpid }" /> 
												<input type="hidden" class="cpprice" value="${cp.cpprice }" /> 
												<input type="radio" name="coupon" class="coupon" value="${cp.mcid }">${cp.cpname }
											</li>
										</c:forEach>
										<li>
											<input type="hidden" class="cpprice" value="0" /> 
											<input type="radio" name="coupon" class="coupon" value="notused"checked>사용안함
										</li>
									</ul>
								</td>
								</tr>

								<tr>
								<td>포인트</td>
								<td colspan="2"><input type="hidden" id="point" value="${customer.point }" /> 
									<input type="number" name="usedpoint" id="usedpoint" value="0" min="0" max="${customer.point }" /> / ${customer.point }P</td>
								</tr>
							</table>
						</td>
						<td id="paypricetd">
							<table >
								<tr>
									<td>주문금액 :<span id="buyprice"></span>원
									</td>
								</tr>
								<tr>
									<td>+ 배송비 :<span id="ddprice">0</span> 원
									</td>
								</tr>
								<tr>
									<td>- 쿠폰할인 : <span id="discount">0</span>원
									</td>
								</tr>
								<tr>
									<td>- 적립금사용 : <span id="cpoint">0</span>원
									</td>
								</tr>
								<tr>
									<td>= 최종결제금액 : <span id="totalprice"></span>원
									</td>
								</tr>
							</table>
						</td>


					</tr>

				</tbody>



			</table>
			</div>
			<!-- -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------  -->

			<div class="paydiv">
				<h3>배송지정보입력</h3>
				<table>
					<tr>
						<td>받으시는 분</td>
						<td>${customer.name }</td>
					</tr>
					<tr>
						<td>주소</td>
						<td>(${customer.pnum }) ${customer.addr}</td>
					</tr>
					<tr>
						<td>연락처</td>
						<td>${customer.hp }</td>
					</tr>
				</table>
			</div>
	
			<div class="paydiv">
				<h3>결제 수단 선택</h3>
				<table>
					<tr>
						<td>
							<input type="radio" name="pmethod" class="pmethod" value="신용체크카드" checked>신용/체크카드 
							<input type="radio" name="pmethod" class="pmethod" value="현금">현금
						</td>
					</tr>
				</table>
				<div id="div_card">
					<select name="card" id="card">
						<option value="선택없음">카드를 선택해주세요.</option>
						<option value="현대카드">현대카드</option>
						<option value="KB국민카드">KB국민카드</option>
						<option value="신한카드">신한카드</option>
						<option value="삼성카드">삼성카드</option>
						<option value="롯데카드">롯데카드</option>
						<option value="우리카드">우리카드</option>
						<option value="하나카드">하나카드</option>
						<option value="비씨카드">비씨카드</option>
						<option value="NH농협카드">NH농협카드</option>
						<option value="씨티카드">씨티카드</option>
					</select> 할부기간 <select name="" id="">
						<option value="일시불">일시불</option>
						<option value="1개월">1개월</option>
						<option value="2개월">2개월</option>
						<option value="3개월">3개월</option>
						<option value="4개월">4개월</option>
						<option value="5개월">5개월</option>
						<option value="6개월">6개월</option>
						<option value="7개월">7개월</option>
						<option value="8개월">8개월</option>
						<option value="9개월">9개월</option>
						<option value="10개월">10개월</option>
						<option value="11개월">11개월</option>
						<option value="12개월">12개월</option>
	
					</select>
				</div>
	
				<div id="div_cash">
					<select name="cash" id="cash">
						<option value="선택없음">은행을 선택해주세요.</option>
						<option value="농협">농협</option>
						<option value="국민은행">국민은행</option>
						<option value="우리은행">우리은행</option>
						<option value="하나은행">하나은행</option>
						<option value="신한은행">신한은행</option>
						<option value="외환은행">외환은행</option>
						<option value="씨티은행">씨티은행</option>
						<option value="기업은행">기업은행</option>
						<option value="우체국">우체국</option>
						<option value="부산은행">부산은행</option>
						<option value="SC은행">SC은행</option>
					</select>
				</div>
			</div>
			<hr />
			<!-- -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------  -->
			<div class="paybuttons">
				<input class="mypagebtn_color btnw120" type="button" id="btn_pay" value="결제하기" /> 
				<input class="mypagebtn_white btnw120" type="button" id="btn_cancle" value="취소하기" />
			</div>
		</form>
	</div>
	
	<%@ include file="../includes/footer.jsp"%>
</body>
</html>
