<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet" type="text/css" href="/pandago/resources/css/header_footer/header.css?1">
<link rel="stylesheet" type="text/css" href="/pandago/resources/css/review/review.css?2">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/mypage.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/review.js"></script>
<style>
	table {
		border-top : 1px rgb(77,144,254) solid;
		border-bottom : 1px rgb(77,144,254) solid;
		width:102%;
		margin: 0 auto;
		text-align: center;
	}
	
	tr:first-child{
		border-bottom : 1px rgb(77,144,254) solid;
	}

	h2 {
		display: inline-block;
		margin-left : 50px; 
	}
	
  	.mypage_side_menu {text-align: center;} 
	.mypage_side_menu li { height : 100%;} 
	.mypage_side_menu li a { height : 100%;}
	.mypage_side_menu li a span{ height : 100%; align-items: center; display: inline-flex;}
	
	.mypagebtn_atag{
		font-size: 15px;
    	color: #4d90fe;
    	background : white;
    	border: 1px #4d90fe solid;
    	height: 28px;
    	vertical-align: bottom;
    	text-align: center;
	}

	.mypagebtn_atag .tagspan{
		position: initial;
		height: 100%;
    	align-items: center;
    	display: inline-flex;
	}
	
	.reviewtable{
		border: 0px;
	}
	
	.reviewtable tr th{
		width: 25%;
	}
	
	.reviewtable tr:first-child{
		border: 0px;
	}
	
	.reviewtitle{
		width: 600px;
   		height: 30px;
    	border-radius: 5px;
    	border: 1px gray solid;
    	margin-right: 10px;
	}
	
	.reviewcontent{
		width: 600px;
   		height: 250px;
    	border-radius: 5px;
    	border: 1px gray solid;
    	margin-right: 10px;
    	    
	}
</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script type="text/javascript">
	$(function() {
		/* CSS */
 		$(".review_tr").mouseenter(function(){
			$(this).css("background-color", "rgba(178, 204, 255, 0.5)");
		});
		$(".review_tr").mouseleave(function(){
			$(this).css("background", "white");
		}); 
		
		$(".type1").on("click", function(){
			$(this).css("background", "rgb(178, 204, 255)");
			$(".type2").css("background", "white");
		});
		
		$(".type2").on("click", function(){
			$(this).css("background", "rgb(178, 204, 255)");
			$(".type1").css("background", "white");
		});
		
		$(".type1").css("background", "rgb(178, 204, 255)");
	});
</script>
</head>
<body>				
<%@ include file="../includes/header.jsp"%>
	<div id="detail_p" class="hide" onkeydown="move();">
		<input type="image" id="xbtn" src="${pageContext.request.contextPath}/resources/detailImages/x_btn.png" alt=""/>
		<div>
			<input type="hidden" name="product_view_no" value="" /> 
			<img style="width: 200px; height: 200px;" /> 
			<span></span>
			 <label></label>
		</div>
	</div><!-- DIV #detail_p end  -->
	
	<div class="detail_container">
		<!-- content -->
		<div class="deatil_body">		
		<!--  design : tab -->
		<div class="deatil_footer">
				<div class="mypage_side_menu" style="float: left; width: 200px; height: 1000px;">
					<%@ include file="../includes/mypage_sidemenu.jsp" %>
				</div><!-- DIV .mypage_side_menu end -->
			<div class="mypage_menu_contents_loc">
			<h2>MY REVIEW</h2>
			<div class="tab">
				<div class="type1" onclick="tab('typeOne');">
					<span>리뷰 쓰러 가기</span>
				</div>
				<div class="type2" onclick="tab('typeTwo');">
					<span>내가쓴 리뷰 보기</span>
				</div>
			</div>

			<div class="area">
				<div class="typeOne hide box activity">
					<div class="one">
						 <ul>
						 	<li>
						 		<table>
									<tr style="text-align: center;">
										<th style="width: 150px;">주문번호</th>
										<th style="width: 150px;">이미지</th>
										<th>결제상태</th>
										<th>상품명 및 개수/가격</th>
										<th>결제방법</th>
										<th>결제일</th>
										<th>리뷰</th>
									</tr>
												
									<c:forEach items="${paylist }" var="paylist">	
									 <c:if test="${paylist.ostate eq '결제완료'}"> 
										<tr class="review_tr">
											<td style="width: 150px;">${paylist.payid }</td>
											<td style="width: 150px;">
												<a href="view.do?no=${paylist.pid }">
													<img src="${paylist.path }" alt="" />
												</a>
											</td>
											<td>${paylist.ostate }</td>
											<td>
												<a href="view.do?no=${paylist.pid }">
													<p>${paylist.pname }</p>
													<p>${paylist.cnt }개/<fmt:formatNumber value="${paylist.pprice }" type="number"/>원</p>
												</a>
											</td>
											<td>${paylist.pmethod }</td>
											<td>${paylist.pdate }</td>
											<td>
												<c:if test="${fn:contains(payidlist,paylist.payid)== false}">
													<a class="mypagebtn_atag" href="javascript:;" onclick="reviewWrite('${paylist.payid }');"><span class="tagspan">리뷰쓰기</span></a>
												</c:if>
												<c:if test="${fn:contains(payidlist,paylist.payid)== true}">
													<a class="mypagebtn_atag" href="view.do?no=${paylist.pid }"><span class="tagspan">리뷰보기</span></a>
												</c:if>		
											</td>
										</tr>
										<tr style="display: none;">
											<td width="150px;">제목</td>
											<td colspan="2">내용</td>
											<td colspan="2">별점 </td>
											<td colspan="2"></td>
										</tr>
										
										<tr id="${paylist.payid }form" style="display: none;">
											<td colspan="6">
												<table class="reviewtable">
													<tr>
														<th>제목</th>
														<td><input class="reviewtitle" type="text" name="" id="${paylist.payid }title" /></td>
													</tr>
													<tr>
														<th>내용</th>
														<td><textarea class="reviewcontent" name="" id="${paylist.payid }text"></textarea></td>
													</tr>
													<tr>
														<th>별점</th>
														<td>
															<p class="star_rating">
				    											<a href="javascript:;" >★
				    												<input type="hidden" name=""  value="1" />
				    											</a>
				    											<a href="javascript:;" >★
				    												<input type="hidden" name=""  value="2" />
				    											</a>
				    											<a href="javascript:;" >★
				    												<input type="hidden" name=""  value="3" />
				    											</a>
				    											<a href="javascript:;" >★
				    												<input type="hidden" name=""  value="4" />
				    											</a>
				    											<a href="javascript:;" >★
				    												<input type="hidden" name=""  value="5" />
				    											</a>
				    											<input type="hidden" id="${paylist.payid }star" value="0" />
				    										</p>
														</td>
													</tr>
												</table>
											</td>
											<td >
												<a class="mypagebtn_atag" href="javascript:;" onclick="reviewWriteOk('${paylist.payid }','${paylist.pid}');"><span class="tagspan">등록하기</span></a>
												<a class="mypagebtn_atag" href="javascript:;" onclick="reviewWritecancle('${paylist.payid }','${paylist.pid}');"><span class="tagspan">취소하기</span></a>
											</td>
<%-- 										
											<td width="150px;">
												<input class="reviewtitle" type="text" name="" id="${paylist.payid }title" />
											</td>
											<td colspan="2">
												<textarea class="reviewcontent" name="" id="${paylist.payid }text"></textarea>
											</td>
											<td>
											
											<p class="star_rating">
    											<a href="javascript:;" >★
    												<input type="hidden" name=""  value="1" />
    											</a>
    											<a href="javascript:;" >★
    												<input type="hidden" name=""  value="2" />
    											</a>
    											<a href="javascript:;" >★
    												<input type="hidden" name=""  value="3" />
    											</a>
    											<a href="javascript:;" >★
    												<input type="hidden" name=""  value="4" />
    											</a>
    											<a href="javascript:;" >★
    												<input type="hidden" name=""  value="5" />
    											</a>
    											<input type="hidden" id="${paylist.payid }star" value="0" />
    										</p>


											</td>
											<td colspan="3">
												<a class="mypagebtn_atag" href="javascript:;" onclick="reviewWriteOk('${paylist.payid }','${paylist.pid}');"><span class="tagspan">등록하기</span></a>
												<a class="mypagebtn_atag" href="javascript:;" onclick="reviewWritecancle('${paylist.payid }','${paylist.pid}');"><span class="tagspan">취소하기</span></a>
											</td> --%>
										</tr>
									 </c:if> 
									</c:forEach>
								</table>	
								<br /><br />
								<a href="#" style="float: right;"> 맨 위로</a>
							</li>
						</ul>	
					</div><!-- DIV one end  -->
				</div><!-- DIV typeOne end  -->

				<div class="typeTwo hide box">
					<div class="two">
						<c:if test="${fn:length(mylist)!=0}">
							<ul>
								<li>
									<table>
										<tr>
											<th>이미지</th>
											<th>제목</th>
											<th>내용</th>
											<th>작성자</th>
											<th>삭제버튼</th>
										</tr>
									
										<c:forEach items="${mylist }" var="mylist">
											<c:if test="${mylist!=null }">
												<tr class="review_tr">
													<td><img src="${mylist.path}" alt="" /></td>
													<td><label>${mylist.title }</label></td>
													<td><label>${mylist.content }</label></td>
													<td><label class="userId"> 회원 : ${mylist.cid}</label></td>
													<td><a class="mypagebtn_atag" href="javascript:;" onclick="reviewdelete('${mylist.payid}')"><span class="tagspan">삭제하기</span></a></td>
												</tr>
											</c:if>
										</c:forEach>
									</table>
								</li>
							</ul>
						</c:if>
						<c:if test="${fn:length(mylist)==0}">
							<h3 class="nH_review" style="text-align: center;"> 작성한 리뷰가 없습니다.</h3>
						</c:if>
						</div>
						<a href="#" style="float: right;"> 맨 위로</a>
				</div><!-- DIV typeTwo end  -->
				
				<div class="typeThree hide box">
					<div class="_three">Type Three Hide Box Activity</div>
				</div><!-- DIV typeThree end  -->	
			</div><!-- DIV area end  -->		
		</div><!-- DIV deatil_footer end  -->
		</div>
	</div>
</div>
<%@ include file="../includes/footer.jsp"%>
</body>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/detail_open.js?3"></script>
</html>
