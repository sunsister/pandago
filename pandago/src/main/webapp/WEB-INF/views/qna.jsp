<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="/pandago/resources/css/header_footer/header.css?1">
<link rel="stylesheet" type="text/css" href="/pandago/resources/css/mypage/mypage.css?2">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/mypage.js"></script>
<style>
	table {
		width:100%;
	}

	#btn_qna_write{
		margin-left : 0px;
		float: right;
	}
	
	.qnatitle{
		width: 100%;
    	height: 30px;
    	border-radius: 5px;
    	border: 1px gray solid;
	}
	
	.qnacontent{
		width: 100%;
   		height: 250px;
    	border-radius: 5px;
    	border: 1px gray solid;
	}
	
	.qnabuttons{
		float: right;
	}
	
	.qnaspan{
		font-size: 12px;
    	color: darkblue;
	}

</style>
</head>
<body>
<%@ include file="../includes/header.jsp" %>

      <div class="mypage_contanier">
         <div class="mypage_box">

				<%@ include file="../includes/mypage_sidemenu.jsp" %>

            <div class="mypage_menu_contents">
               <div class="mypage_menu_contents_loc">      
                  <c:choose>
                     <c:when test="${hide == null }">
                        <script type="text/javascript">   $(function(){ $(".user_info").removeClass("hide"); })   </script>
                     </c:when>
                  </c:choose>
                         
                  <div class="qnaContainer">
                     <c:choose>
                        <c:when test="${list != null }" >
                           <div class="qnaList">
                              <div class="qnaheader">
                                 <h3>QnA List</h3>
                                 <!-- 번호 / 제목 / 답변여부 / 작성일   -->   
                                 <input class="mypagebtn_white btnw120" type="button" value="문의글작성" id="btn_qna_write"/>
                              </div>
                              <ul>
                                 <li>번호</li>
                                 <li class="title">제목</li>
                                 <li>작성일</li>
                                 <li>답변여부</li>
                              </ul>
                              <c:forEach var="dto" items="${list }">      
                                 <ul>      
                                    <li>${dto.qnaid}</li>
                                    <li class="title"><a href="/pandago/qna.do?action=detail&qnaid=${dto.qnaid}">${dto.title }</a></li>
                                    <li>${dto.qdate}</li>
                                    <li>${dto.aok}</li>
                                 </ul>
                              </c:forEach>      
                           </div>   
                        </c:when>
                        
                        <c:when test="${Ddto != null }">
                           <div class="qnaDetail">
                              <div class="qnaheader"><h3>QnA</h3></div>
                              
                              <div class="qna">
                              	<span class="qnaspan" style="margin-top: 5px;">문의내용 ></span>
                                 <p>분류 <input type="radio" name="qcode" value="${Ddto.qcode }" checked>${Ddto.qcode } &nbsp;&nbsp;&nbsp;&nbsp; 작성일 ${Ddto.qdate }</p>
                                 
                                 <h4>${Ddto.title }</h4>
                                 <textarea class="qnacontent" name="qcontent" cols="30" rows="10" readonly>${Ddto.qcontent }</textarea>
                                 
                              	 <span class="qnaspan" style="margin-top: 10px;">답변내용 ></span>
                                 <c:if test="${Ddto.aok==0 }">
                                    <textarea class="qnacontent" name="qcontent" cols="30" rows="10" readonly>답변이 없습니다. 기다려주세요.</textarea>
                                 </c:if>
                                 <c:if test="${Ddto.aok==1 }">
                                       <p>답변일 : ${Ddto.adate }</p>
                                       	  <textarea class="qnacontent" name="qcontent" cols="30" rows="10" readonly>${Ddto.acontent }</textarea>
                                        <br />
                                 </c:if>
								<div class="qnabuttons" style="margin-top: 5px;">
                                 	<c:if test="${Ddto.aok==0 }">
                                       <a class="mypagebtn_atag btnw120" href="/pandago/qna.do?action=modify&qnaid=${Ddto.qnaid}"><span>수정하기</span></a>
                                 	</c:if>
                                 	<a class="mypagebtn_atag btnw120" href="/pandago/qna.do?action=delete&qnaid=${Ddto.qnaid}"><span>삭제하기</span></a>            
                                 	<input class="mypagebtn_white btnw120" type="button" value="문의목록" id="btn_qna_list"/>
                              	</div>
                              </div>
                           </div>                     
                        </c:when>
                        
                        <c:when test="${Mdto != null }">         
                           <div class="qnaUpdate">
                              	<div class="qnaheader"><h3>QnA 수정</h3></div>
                                 <form action="/pandago/qna.do">
                                 <input type="hidden" name="action" value="modifyOK" />
                                 <input type="hidden" name="qnaid" value="${Mdto.qnaid }" />
                                 <table>
                                    <tr>
                                       <th>분류</th>
                                       <td>
											<c:if test="${Mdto.qcode eq '배송관련'}">
												<input type="radio" name="qcode" value="배송관련" checked>배송관련
												<input type="radio" name="qcode" value="취소/교환/반품">취소/교환/반품
												<input type="radio" name="qcode" value="할인쿠폰/포인트">할인쿠폰/포인트
											</c:if>
											<c:if test="${Mdto.qcode eq '취소/교환/반품'}">
												<input type="radio" name="qcode" value="배송관련">배송관련
												<input type="radio" name="qcode" value="취소/교환/반품" checked>취소/교환/반품
												<input type="radio" name="qcode" value="할인쿠폰/포인트">할인쿠폰/포인트
											</c:if>
											<c:if test="${Mdto.qcode eq '할인쿠폰/포인트'} ">
												<input type="radio" name="qcode" value="배송관련">배송관련
												<input type="radio" name="qcode" value="취소/교환/반품">취소/교환/반품
												<input type="radio" name="qcode" value="할인쿠폰/포인트" checked>할인쿠폰/포인트
											</c:if>
											
										</td>
										<%-- 
                                       <td>
                                          <c:if test="${Mdto.qcode eq '배송관련'}">
                                             <script type="text/javascript">
                                                $(function(){
                                                   $("#s").checked=true;
                                                })   
                                             </script>
                                          </c:if>
                                          <c:if test="${Mdto.qcode eq '취소/교환/반품'}">
                                             <script type="text/javascript">
                                                $(function(){
                                                   $("#c").checked=true;
                                                })   
                                             </script>
                                          </c:if>
                                          <c:if test="${Mdto.qcode eq '할인쿠폰/포인트'} ">
                                             <script type="text/javascript">
                                                $(function(){
                                                   $("#p").checked=true;
                                                })   
                                             </script>
                                          </c:if>
                                       <input type="radio" name="qcode"  id="s" value="배송관련">배송관련
                                       <input type="radio" name="qcode"  id="c" value="취소/교환/반품">취소/교환/반품
                                       <input type="radio" name="qcode"  id="p" value="할인쿠폰/포인트">할인쿠폰/포인트
                                    </td>
                                     --%>
                                 </tr>
                                 <tr>
                                    <th>작성일</th>
                                    <td>${Mdto.qdate }</td>
                                 </tr>
                                 <tr>
                                    <th>제목</th>
                                    <td colspan="4"><input class="qnatitle" type="text" name="title" value="${Mdto.title }"/></td>
                                 </tr>
                                 <tr>
                                    <th>내용</th>
                                    <td colspan="4">
                                     	<textarea class="qnacontent" name="qcontent" >${Mdto.qcontent }</textarea>
                                    </td>
                                 </tr>
                                          
                                 <tr>
                                    <td colspan="5">
                                       <div class="qnabuttons">
                                       		<input class="mypagebtn_color btnw120" type="submit" value="수정완료" />
                                       		<input class="mypagebtn_white btnw120" type="button" value="문의목록" id="btn_qna_list"/>
                                       </div>
                                    </td>
                                 </tr>
                              </table>
                              </form>
                           </div>                           
                        </c:when>
                        
                        <c:when test="${write != null }">         
                           <div class="qnaWrite">
                              <div class="qnaheader"><h3>QnA 작성</h3></div>
                              <form class="qnaform" action="/pandago/qna.do">
                                 <input type="hidden" name="action" value="writeOK" />   
                                 <table>
                                    <tr>
                                       <th>분류</th>
                                       <td colspan="4">
                                          <!-- 분류 -->
                                          <input type="radio" name="qcode" value="배송관련">배송관련
                                          <input type="radio" name="qcode" value="취소/교환/반품">취소/교환/반품
                                          <input type="radio" name="qcode" value="할인쿠폰/포인트">할인쿠폰/포인트
                                       </td>
                                    </tr>
                                    <tr>
                                       <th>제목</th>
                                       <td colspan="4"><input class="qnatitle" type="text" name="title" /></td>
                                    </tr>
                                    <tr>
                                       <th>내용</th>
                                       <td colspan="4">
                                          <textarea class="qnacontent" name="qcontent" cols="30" rows="10"></textarea>
                                       </td>
                                    </tr>
                                    <tr>
                                       <td colspan="5">
	                                       	<div class="qnabuttons">
	                                          <input class="mypagebtn_color btnw120" type="submit" value="작성완료" />
	                                          <input class="mypagebtn_white btnw120" type="button" value="취소" id="btn_qna_list"/>
	                                       	</div>
                                       </td>
                                    </tr>
                                 </table>
                              </form>
                           </div>                              
                        </c:when>                        
                     </c:choose>            
                  </div><!-- DIV qnaContainer end  -->   
               </div><!-- DIV contents_box end -->   
            </div><!-- DIV mypage_menu_contents end -->
         </div> <!-- DIV mypage_box end  -->
      </div>  <!-- DIV mypage_contanier end -->
      <%@ include file="../includes/footer.jsp"%>
</body>
</html>