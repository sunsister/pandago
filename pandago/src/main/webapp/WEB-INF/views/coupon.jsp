<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/header_footer/header.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/coupon/coupon.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script type="text/javascript">
	$(function(){
		$(".dev_btn_coupon").on("click" , function(){
			var cpid = $(this).parents().children().children(".dev_cpid").val();
			var cp = $(this);
			$.ajax({
				url : "coupon.do",
				type : "POST",
				data : "action=getcp&cpid="+cpid,
				dataType : "text",
				success : function(data) {
					if(data == 'success'){
						alert("쿠폰 발급 완료");
						cp.remove('dev_btn_coupon').addClass('dev_recive_coupon').val('발급 완료');
					}else{
						alert("이미 발급된 쿠폰");
					}
				},
				error : function(jqXHR, textStatus, errorThrown) {
					alert("에러 "+ textStatus+ " : "+ errorThrown);
				}

			});
		});
	});
</script>
</head>
<body>
<%@ include file="../includes/header.jsp" %>

<div class="container">
	<div class="coupon">
		<h1>Coupon Zone</h1>
		<div class="coupon_list">
			<c:forEach var="dto" items="${list }">
				<ul class="object">
					<li><input type="hidden" class="dev_cpid" name="cpid" value="${dto.cpid}"/></li>
					<li><span>${dto.cpname}</span></li>
					<li>할인금액 <fmt:formatNumber type="number" value="${dto.cpprice}" />원</li>
					<c:if test="${dto.edate ne null}">
						<li>사용기간 ${dto.sdate} ~ ${dto.edate}</li>
					</c:if>
					<c:if test="${dto.edate eq null}">
						<li>사용기간 무제한</li>
					</c:if>
					<li>최소결제금액 <fmt:formatNumber type="number" value="${dto.minprice}" />원</li>
					<li>
						<input type="button" class="get dev_btn_coupon" value="쿠폰 받기" />
					</li>
				</ul>
			</c:forEach>
		</div>
	</div>
</div>

<%@ include file="../includes/footer.jsp"%>
</body>
</html>