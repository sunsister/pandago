package kr.co.pandago.service;

import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import kr.co.pandago.dao.MyCouponDAO;
import kr.co.pandago.dto.MyCouponDTO;
@Service("mycouponService")
public class MyCouponService implements PService<MyCouponDTO>{
	
	
	@Resource(name="mycouponDAO")
	private MyCouponDAO dao;
	
	@Override
	public List<MyCouponDTO> readAll(int startNo, int endNo) {
		return null;
	}

	@Override
	public MyCouponDTO readOne(String no) {
		return null;
	}

	@Override
	public void writeOne(MyCouponDTO dto) {
	}

	@Override
	public void modifyOne(MyCouponDTO dto) {
	}
	
	@Override
	public void removeOne(String no) {
		dao.deleteOne(no);
	}

	public List<MyCouponDTO> readAll(String cid){
		return dao.selectAll(cid);
	}
	
	public MyCouponDTO readOne(HashMap<String, String> hm){
		return dao.selectOne(hm);
	}
	
	public void modifyOne(String mcid){
		dao.updateOne(mcid);
	}
	
	public List<MyCouponDTO> readDetailAll(String cid){
		return dao.detailAll(cid);
	}
	
	public List<MyCouponDTO> readDetailUsed(HashMap<String, Object> hm){
		return dao.detailUsed(hm);
	}
	
	public void writeOne(HashMap<String, String> hm) {
		dao.insertOne(hm);
	}
	
	
}
