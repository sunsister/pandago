package kr.co.pandago.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import kr.co.pandago.dao.DAO;
import kr.co.pandago.dao.ReviewDAO;
import kr.co.pandago.dto.ReviewDTO;
@Service("reviewService")
public class ReviewService implements PService<ReviewDTO>{
	@Resource(name="reviewDAO")
	private ReviewDAO dao;
	@Override
	public List<ReviewDTO> readAll(int startNo, int endNo) {
		// TODO Auto-generated method stub
		return dao.selectAll(startNo, endNo);
	}

	@Override
	public ReviewDTO readOne(String no) {
		// TODO Auto-generated method stub
		return dao.selectOne(no);
	}

	@Override
	public void writeOne(ReviewDTO dto) {
		// TODO Auto-generated method stub
		dao.insertOne(dto);
	}

	@Override
	public void modifyOne(ReviewDTO dto) {
		// TODO Auto-generated method stub
		dao.updateOne(dto);
		
	}

	@Override
	public void removeOne(String no) {
		dao.deleteOne(no);
		
	}
	public List<ReviewDTO> selectAllList(String no) {
		return dao.selectAllList(no);
	}
	public List<ReviewDTO> selectreview(String no) {
		return dao.selectreview(no);
	}
	public List<String> selectPayid() {
		return dao.selectpayid();
	}
	public List<String> selectMyreview(String id) {
		return dao.selectMyreview(id);
	}

}
