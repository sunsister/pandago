package kr.co.pandago.service;

import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import kr.co.pandago.dao.InquiryDAO;
import kr.co.pandago.dto.InquiryDTO;
@Service("inquiryService")
public class InquiryService implements PService<InquiryDTO>{
	@Resource(name="inquiryDAO")
	private InquiryDAO dao;
	
	@Override
	public List<InquiryDTO> readAll(int startNo, int endNo) {
		return dao.selectAll(startNo, endNo);
	}
	
	@Override
	public InquiryDTO readOne(String no) {
		return dao.selectOne(no);
	}

	@Override
	public void writeOne(InquiryDTO dto) {
		dao.insertOne(dto);
	}

	@Override
	public void modifyOne(InquiryDTO dto) {
		dao.updateOne(dto);
	}

	@Override
	public void removeOne(String no) {
		dao.deleteOne(no);
	}
	
	public List<InquiryDTO> readlistAll(InquiryDTO t) {
		return dao.selectList(t);
	}
	
	public void askInquiry(HashMap<String, String> hm){
		dao.askInquiry(hm);
	}
	
	public int getTotal(){
		return dao.getTotal();
	}

}
