package kr.co.pandago.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import kr.co.pandago.dao.DAO;
import kr.co.pandago.dao.ProductDAO;
import kr.co.pandago.dto.CodeDTO;
import kr.co.pandago.dto.ProductDTO;

@Service("productService")
public class ProductService implements PService<ProductDTO> {

	@Resource(name = "productDAO")
	
	private ProductDAO pdao;

	@Override
	public List<ProductDTO> readAll(int startNo, int endNo) {

		return pdao.selectAll(startNo, endNo);
	}

	@Override
	public ProductDTO readOne(String no) {
		return (ProductDTO) pdao.selectOne(no);
	}

	@Override
	public void writeOne(ProductDTO dto) {
		pdao.insertOne(dto);
	}

	@Override
	public void modifyOne(ProductDTO dto) {
		pdao.updateOne(dto);
	}

	@Override
	public void removeOne(String no) {
		pdao.deleteOne(no);
	}

	public List<ProductDTO> readAll(ProductDTO dto) {

		return pdao.selectList(dto);
	}

	public int total(ProductDTO dto) {
		return pdao.Total(dto);

	}

	public List<CodeDTO> selectCode1(String lcode) {

		return pdao.selectCode1(lcode);
	}
	public List<CodeDTO> selectCode2(String lcode) {
		
		return pdao.selectCode2(lcode);
	}
	
	public List<ProductDTO> selectmain(ProductDTO dto ) {

		return pdao.selectmain(dto);
	}


	public List<ProductDTO> selectSearch(ProductDTO dto) {
		
		return pdao.selectSearch(dto);
	}
	public int searchTotal(ProductDTO dto) {
		
		return pdao.searchTotal(dto);
	}

	public String selectmainimg(String id) {
		
		return pdao.selectmainimg(id);
	}
	public void starupdate(ProductDTO dto) {
		
		pdao.starupdate(dto);
	}
	
	
}
