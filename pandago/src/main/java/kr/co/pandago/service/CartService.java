package kr.co.pandago.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import kr.co.pandago.dao.CartDAO;
import kr.co.pandago.dao.DAO;
import kr.co.pandago.dto.CartDTO;

@Service("cartService")
public class CartService implements PService<CartDTO> {

	@Resource(name="cartDAO")
	private CartDAO dao;
	
	@Override
	public List<CartDTO> readAll(int startNo, int endNo) {
		
		return null;
	}

	public List<CartDTO> readCart(String id) {
		
		return new CartDAO().selectCart(id);
	}
	
	@Override
	public CartDTO readOne(String no) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void writeOne(CartDTO dto) {
		dao.insertOne(dto);
		
	}

	@Override
	public void modifyOne(CartDTO dto) {
		dao.updateOne(dto);
		
	}

	@Override
	public void removeOne(String no) {
		dao.deleteOne(no);
		
	}
	public List<CartDTO> creadAll(String id) {
		
		return dao.selectCart(id);
	}

}
