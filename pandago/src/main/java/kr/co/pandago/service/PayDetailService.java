package kr.co.pandago.service;

import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import kr.co.pandago.dao.PayDetailDAO;
import kr.co.pandago.dto.PayDetailDTO;
import kr.co.pandago.dto.PaymentDTO;

@Service("paydetailService")
public class PayDetailService implements PService<PayDetailDTO>{

	@Resource(name="paydetailDAO")
	private PayDetailDAO dao;
	
	@Override
	public List<PayDetailDTO> readAll(int startNo, int endNo) {
		return null;
	}

	@Override
	public PayDetailDTO readOne(String no) {
		return null;
	}

	@Override
	public void writeOne(PayDetailDTO dto) {
		dao.insertOne(dto);
	}

	@Override
	public void modifyOne(PayDetailDTO dto) {

	}

	@Override 
	public void removeOne(String no) {

	} 
	
	public List<PayDetailDTO> readAll(String cid){
		return dao.selectAll(cid);
	}
	
	
	public List<PayDetailDTO> readPayDid(String payid){
		return dao.selectPurchase(payid);
	}
	

	
	
	
}
