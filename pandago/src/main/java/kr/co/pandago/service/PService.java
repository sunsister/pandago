package kr.co.pandago.service;


import java.util.List;

public interface PService<T> {
	public List<T> readAll(int startNo, int endNo);
	public T readOne(String no);
	public void writeOne(T dto);
	public void modifyOne(T dto);
	public void removeOne(String no);
}
