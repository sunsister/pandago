package kr.co.pandago.service;

import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import kr.co.pandago.dao.QnaDAO;
import kr.co.pandago.dto.QnaDTO;

@Service("qnaService")
public class QnaService implements PService<QnaDTO> {

	@Resource(name="qnaDAO")
	private QnaDAO dao;

	@Override
	public List<QnaDTO> readAll(int startNo, int endNo) {
		
		return dao.selectAll(startNo, endNo);
	}

	@Override
	public QnaDTO readOne(String no) {

		return (QnaDTO) dao.selectOne(no);
	}

	@Override
	public void writeOne(QnaDTO dto) {
		dao.insertOne(dto);
	}

	@Override
	public void modifyOne(QnaDTO dto) {
		dao.updateOne(dto);
	}

	
	@Override
	public void removeOne(String no) {
		dao.deleteOne(no);
	}
	
	public void AcontentmodifyOne(QnaDTO dto) {
		new QnaDAO().updateAcontent(dto);
	}
	
	public List<QnaDTO> readAll(HashMap<String, Object> hm) {
		
		return dao.selectAll(hm);
	}
	
	public int getTotal(){
		return dao.getTotal();
	}
	
	public void askQna(HashMap<String, String> hm){
		dao.askQna(hm);
	}
	
	public List<QnaDTO> selectAllAdmin(HashMap<String, Object> hm) {
		return dao.selectAllAdmin(hm);
	}

}
