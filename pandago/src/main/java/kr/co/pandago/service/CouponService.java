package kr.co.pandago.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import kr.co.pandago.dao.CouponDAO;
import kr.co.pandago.dto.CouponDTO;
@Service("couponService")
public class CouponService implements PService<CouponDTO>{
	
	@Resource(name="couponDAO")
	private CouponDAO dao;
	
	@Override
	public List<CouponDTO> readAll(int startNo, int endNo) {
		return null;
	}

	@Override
	public CouponDTO readOne(String no) {
		return  dao.selectOne(no);
	}

	@Override
	public void writeOne(CouponDTO dto) {
		dao.insertOne(dto);
	}
	
	@Override
	public void modifyOne(CouponDTO dto) {
		dao.updateOne(dto);
	}
	
	@Override
	public void removeOne(String no) {
		dao.deleteOne(no);
	}

	public List<CouponDTO> readAll() {
		return dao.selectAll();
	}
	
}
