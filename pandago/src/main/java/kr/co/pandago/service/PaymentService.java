package kr.co.pandago.service;

import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import kr.co.pandago.dao.PaymentDAO;
import kr.co.pandago.dto.PayDetailDTO;
import kr.co.pandago.dto.PaymentDTO;

@Service("paymentService")
public class PaymentService implements PService<PaymentDTO>{
	
	@Resource(name="paymentDAO")
	private PaymentDAO dao;

	@Override
	public List<PaymentDTO> readAll(int startNo, int endNo) {
		return null;
	}

	@Override
	public PaymentDTO readOne(String payid) {
		return dao.selectOne(payid);
	}

	@Override
	public void writeOne(PaymentDTO dto) {
		dao.insertOne(dto);
		
	}

	@Override
	public void modifyOne(PaymentDTO dto) {
		dao.updateOne(dto);
	}


	@Override
	public void removeOne(String no) {
		dao.deleteOne(no);
		
	}
	
	
	public List<PaymentDTO> readAll(String cid){
		return dao.selectAll(cid);
	}

	public String getPayId(String cid){
		return dao.getPayId(cid);
	}
	
	public List<PaymentDTO> searchInfo(HashMap<String, String> hm){
		return dao.selectDetail(hm);
	}
	
}
