package kr.co.pandago.service;

import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import kr.co.pandago.dao.CodeDAO;
import kr.co.pandago.dto.CodeDTO;

@Service("codeService")
public class CodeService {
	@Resource(name="codeDAO")
	private CodeDAO dao;
	
	public List<CodeDTO> readLevel(HashMap<String, Object> hm){
		return dao.selectLevel(hm);
	}
}
