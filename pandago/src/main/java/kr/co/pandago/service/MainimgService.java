package kr.co.pandago.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import kr.co.pandago.dao.DAO;
import kr.co.pandago.dao.MainImgDAO;
import kr.co.pandago.dto.DetailImgDTO;
import kr.co.pandago.dto.MainImgDTO;
@Service("mainImgService")
public class MainimgService implements PService<MainImgDTO>{

	@Resource(name="mainImgDAO")
	private DAO<MainImgDTO> dao;
	
	@Override
	public List<MainImgDTO> readAll(int startNo, int endNo) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public MainImgDTO readOne(String no) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void writeOne(MainImgDTO dto) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void modifyOne(MainImgDTO dto) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void removeOne(String no) {
		// TODO Auto-generated method stub
		
	}
	
	public List<MainImgDTO> selectOneimg(String no){
		MainImgDAO midao = (MainImgDAO)dao;
		return midao.selectOneimg(no);
	}
}
