package kr.co.pandago.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import kr.co.pandago.dao.DAO;
import kr.co.pandago.dao.DetailImgDAO;
import kr.co.pandago.dao.MainImgDAO;
import kr.co.pandago.dto.DetailImgDTO;
import kr.co.pandago.dto.MainImgDTO;

@Service("detailImgService")
public class DetailImgService implements PService<DetailImgDTO>{

	@Resource(name="detailImgDAO")
	private DAO<DetailImgDTO> dao;
	
	@Override
	public List<DetailImgDTO> readAll(int startNo, int endNo) {
		return dao.selectAll(startNo, endNo);
	}

	@Override
	public DetailImgDTO readOne(String no) {
		return (DetailImgDTO)dao.selectOne(no);
	}

	@Override
	public void writeOne(DetailImgDTO dto) {
		dao.insertOne(dto);
	}

	@Override
	public void modifyOne(DetailImgDTO dto) {
		dao.updateOne(dto);
	}

	@Override
	public void removeOne(String no) {
		dao.deleteOne(no);
	}
	
	public List<DetailImgDTO> readAllimg(String no){
		DetailImgDAO didao = (DetailImgDAO)dao;
		return didao.selectAllimg(no);
	}

	
	
	
}
