package kr.co.pandago.service;

import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import kr.co.pandago.dao.CustomerDAO;
import kr.co.pandago.dto.CustomerDTO;

@Service("customerService")
public class CustomerService implements PService<CustomerDTO>{

	@Resource(name="customerDAO")
	private CustomerDAO dao;
	
	
	@Override
	public List<CustomerDTO> readAll(int startNo, int endNo) {

		return dao.selectAll(startNo, endNo);
	}

	@Override
	public CustomerDTO readOne(String no) {
		
		return  dao.selectOne(no);
	}

	@Override
	public void writeOne(CustomerDTO dto) {
		dao.insertOne(dto);
	}
	@Override
	public void modifyOne(CustomerDTO dto) {
		dao.updateOne(dto);
	}

	@Override
	public void removeOne(String no) {
		dao.deleteOne(no);
	}

	public String validateEmail(String email) {
		return dao.validateEmail(email);
	}
	
	public CustomerDTO loginOk(HashMap<String, String> hm){
		return dao.loginOk(hm);
	}
	
	public void modifyPw(HashMap<String, String> hm){
		dao.modifyPw(hm);
	}

	public void modifyPoint(HashMap<String, Object> hm){
		dao.modifyPoint(hm);
	}
	
	public void raisePoint(HashMap<String, Object> hm){
		dao.raisePoint(hm);
	}
}
