package kr.co.pandago.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import kr.co.pandago.dao.DAO;
import kr.co.pandago.dao.PdetailDAO;
import kr.co.pandago.dto.PdetailDTO;

@Service("pdetailService")
public class PdetailService implements PService<PdetailDTO>{
	
	@Resource(name="pdetailDAO")
	private PdetailDAO dao;
	
	@Override
	public List<PdetailDTO> readAll(int startNo, int endNo) {
		return dao.selectAll(startNo, endNo);
	}

	@Override
	public PdetailDTO readOne(String no) {
		return (PdetailDTO) dao.selectOne(no);
	}

	@Override
	public void writeOne(PdetailDTO dto) {
		dao.insertOne(dto);
	}

	@Override
	public void modifyOne(PdetailDTO dto) {
		dao.updateOne(dto);
	}

	@Override
	public void removeOne(String no) {
		dao.deleteOne(no);
	}
	public List<PdetailDTO> readAllop(String no) {
		return dao.selectopAll(no);
	}
		
	
}
