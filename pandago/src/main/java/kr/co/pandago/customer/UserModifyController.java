package kr.co.pandago.customer;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import kr.co.pandago.dto.CustomerDTO;
import kr.co.pandago.service.CustomerService;

@Controller
@RequestMapping("/umodify.do")
public class UserModifyController {
	@Resource(name = "customerService")
	private CustomerService cs;

	public void setCs(CustomerService cs) {
		this.cs = cs;
	}

	@RequestMapping()
	public ModelAndView userModify(HttpSession session) {
		String cid = (String) session.getAttribute("cid");
		CustomerDTO dto = cs.readOne(cid);
		return new ModelAndView("userModify", "dto", dto);
	}

	@RequestMapping(params = "ac=userModifyOk")
	public ModelAndView modifyOk(@ModelAttribute("customerdto") CustomerDTO dto) {
		
		CustomerDTO cdto = cs.readOne(dto.getCid());
		cdto.setName(dto.getName());
		cdto.setPnum(dto.getPnum());
		cdto.setAddr(dto.getAddr());
		cdto.setPw(dto.getPw());
		cdto.setHp(dto.getHp());
		cs.modifyOne(cdto);
		return new ModelAndView("mypage", "dto", cdto);
	}
}
