package kr.co.pandago.customer;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import kr.co.pandago.dto.PayDetailDTO;
import kr.co.pandago.dto.PaymentDTO;
import kr.co.pandago.service.MailService;
import kr.co.pandago.service.PayDetailService;
import kr.co.pandago.service.PaymentService;

@Controller
@RequestMapping("/purchase.do")
public class MyPurchaseController {

	@Resource(name = "paymentService")
	PaymentService payservice;

	@Resource(name = "paydetailService")
	PayDetailService ps;
	
	@Resource(name="mailService")
	private MailService mailService;

	@RequestMapping()
	public ModelAndView plist(HttpSession session) {
		String cid = (String) session.getAttribute("cid");

		List<PaymentDTO> plist = payservice.readAll(cid);
		Map<String, Integer> pcnt = new HashMap<String, Integer>();
		for (int i = 0; i < plist.size(); i++) {
			String payid = plist.get(i).getPayid();
			List<PayDetailDTO> psize = ps.readPayDid(payid);
			pcnt.put(payid, psize.size());
			
			//System.out.println(psize.size());
		}
		
		List<PayDetailDTO> list = ps.readAll(cid);
		ModelAndView mav = new ModelAndView("purchaseList", "list", list);
		mav.addObject("paylist", plist);
		mav.addObject("pcnt",pcnt);
		return mav;
	}

	@RequestMapping(params = "ac=paysearch")
	public ModelAndView psearch(@RequestParam String payid,HttpSession session) {
		System.out.println(payid);
		String cid=(String)session.getAttribute("cid");
		System.out.println(cid);
		HashMap<String, String> hm = new HashMap<String,String>();
		hm.put("payid", payid);
		hm.put("cid",cid);
		List<PaymentDTO> plist =payservice.searchInfo(hm);
	
		ModelAndView mav= new ModelAndView("purchaseDetail", "plist", plist);
	
		return mav;
	}
	
	@RequestMapping(params = "ac=paycancel")
	public ModelAndView paycancel(@RequestParam String payid, HttpSession session) {
		List<PayDetailDTO> list = ps.readPayDid(payid);
		ModelAndView mav = new ModelAndView("purchaseCancel", "list", list);
		
	
		return mav;
		
	}
	
	@RequestMapping(params = "ac=cancelOk")
	public ModelAndView purchaeCancel(@RequestParam("payid")String payid ,@ModelAttribute("paymentdto") PaymentDTO dto,RedirectAttributes ra,HttpSession session){
		System.out.println(payid);
		
		PaymentDTO pdto=payservice.readOne(payid);
		pdto.setOstate("");
		payservice.modifyOne(pdto);
		String cid= (String) session.getAttribute("cid");	
		 String subject = "..";
         StringBuilder sb = new StringBuilder();
         sb.append(".");
         mailService.send(subject, sb.toString(), "lietter2@naver.com", cid, null);
         ra.addFlashAttribute("resultMsg", ".");
		
		return new ModelAndView("cancelPage");
	}
	
	
	
	

}
