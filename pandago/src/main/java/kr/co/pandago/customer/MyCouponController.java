package kr.co.pandago.customer;

import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import kr.co.pandago.dto.MyCouponDTO;
import kr.co.pandago.service.MyCouponService;

@Controller

@RequestMapping(value="/mycoupon.do")
public class MyCouponController {
	
	@Resource(name="mycouponService")
	MyCouponService service;
	
	@RequestMapping()
	public ModelAndView cplist(HttpSession session){
		String cid = (String) session.getAttribute("cid");
		List<MyCouponDTO> list = service.readDetailAll(cid);
		return new ModelAndView("mycoupon", "list", list);
	}
	
	@ResponseBody
	@RequestMapping(params="action=cpused")
	public List<MyCouponDTO> cpused(@RequestParam("used") int used, HttpSession session){
		String cid = (String) session.getAttribute("cid");
		HashMap<String, Object> hm = new HashMap<String, Object>();
		hm.put("cid", cid);
		hm.put("cpok", used);
		List<MyCouponDTO> list = service.readDetailUsed(hm);

		return list;
	}
	
	@ResponseBody
	@RequestMapping(params="action=cplist")
	public List<MyCouponDTO> cp(HttpSession session){
		String cid = (String) session.getAttribute("cid");
		List<MyCouponDTO> list = service.readDetailAll(cid);
		return list;
	}
}
