package kr.co.pandago.customer;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import kr.co.pandago.dto.InquiryDTO;
import kr.co.pandago.service.InquiryService;

@Controller
public class InquiryController {

	@Resource(name="inquiryService")
	private InquiryService is;
	
	@RequestMapping(value="iniddelete.do")
	public ModelAndView iniddelete(@RequestParam("inid")String inid, @RequestParam("pid")String pid, HttpSession session){
		System.out.println(inid);
		System.out.println(pid);
		
		ModelAndView mav = new ModelAndView();
		InquiryDTO  dto= new InquiryDTO();
		dto.setPid(pid);
		is.removeOne(inid);
		mav.setViewName("jsonView");
		mav.addObject("list", is.readlistAll(dto) );
		mav.addObject("cid", session.getAttribute("cid") );
		
		return mav;
	}
	
	@RequestMapping(value="isinsert.do")
	public ModelAndView isinsert(@RequestParam("title")String title, @RequestParam("content")String content,@RequestParam("pid")String pid, HttpSession session,
									@RequestParam("insecret")int insecret,@RequestParam("pwwriteinput")String inpw  ){
		ModelAndView mav = new ModelAndView();
		InquiryDTO  dto= new InquiryDTO();
		if ((String)session.getAttribute("cid")!=null) {
			
		
		dto.setPid(pid);
		if (title != null) {
			dto.setIntitle(title);
		}
		if (content != null) {
			dto.setIncontent(content);
		}
		
		dto.setInsecret(insecret);
		
		if (inpw!=null) {
			dto.setInpw(inpw);
		}
			dto.setCid((String)session.getAttribute("cid"));
		
		
		is.writeOne(dto);
		mav.setViewName("jsonView");
		mav.addObject("list", is.readlistAll(dto) );
		mav.addObject("cid", session.getAttribute("cid") );
		
		}else{
			mav.setViewName("jsonView");
			mav.addObject("cid", session.getAttribute("cid") );
		}
		return mav;
	}
}
