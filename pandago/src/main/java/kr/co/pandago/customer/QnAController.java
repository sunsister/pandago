package kr.co.pandago.customer;

import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import kr.co.pandago.dto.QnaDTO;
import kr.co.pandago.service.QnaService;

@Controller
@RequestMapping(value="/qna.do")
public class QnAController {
	
	@Resource(name="qnaService")
	private QnaService service;
	
	@RequestMapping(params="action=list")
	public ModelAndView list(HttpSession session){
		HashMap<String, Object> hm = new HashMap<String, Object>();
		hm.put("startNo", 0);
		hm.put("endNo", 10);
		hm.put("cid", session.getAttribute("cid"));
		List<QnaDTO> list = service.readAll(hm);
		
		ModelAndView mav = new ModelAndView();
		mav.addObject("hide", "hide");
		mav.addObject("list",list);
		mav.setViewName("qna");
		return mav;
	}
	
	@RequestMapping(params="action=write")
	public ModelAndView write(){
		ModelAndView mav = new ModelAndView();
		mav.addObject("hide","hide");
		mav.addObject("write","true");
		mav.setViewName("qna");
		return mav;
	}
	
	@RequestMapping(params="action=writeOK")
	public String writeOK(@RequestParam("qcode") String qcode, @RequestParam("title") String title, 
			@RequestParam("qcontent") String qcontent, HttpSession session){
		String cid = (String) session.getAttribute("cid");
		QnaDTO dto = new QnaDTO();
		dto.setCid(cid);
		dto.setQcode(qcode);
		dto.setTitle(title);
		dto.setQcontent(qcontent);
	
		service.writeOne(dto);
	
		return "redirect:/qna.do?action=list";
	}
	
	@RequestMapping(params="action=detail")
	public ModelAndView detail(@RequestParam("qnaid") String qnaid){
		QnaDTO dto = service.readOne(qnaid);
		ModelAndView mav = new ModelAndView();
		mav.addObject("Ddto", dto);
		mav.addObject("hide", "hide");
		mav.setViewName("qna");
		return mav; 
	}
	
	@RequestMapping(params="action=delete")
	public String delete(@RequestParam("qnaid") String qnaid){
		service.removeOne(qnaid);
		return "redirect:/qna.do?action=list";
	}
	
	@RequestMapping(params="action=modify")
	public ModelAndView modify(@RequestParam("qnaid") String qnaid){
		QnaDTO dto = service.readOne(qnaid);

		ModelAndView mav = new ModelAndView();
		mav.addObject("Mdto", dto);
		mav.addObject("hide", "hide");
		mav.setViewName("qna");
		return mav;
	}
	
	@RequestMapping(params="action=modifyOK")
	public String modifyOK(@ModelAttribute("qnadto") QnaDTO dto){
		QnaDTO ori = service.readOne(dto.getQnaid());
		ori.setQcode(dto.getQcode());
		ori.setTitle(dto.getTitle());
		ori.setQcontent(dto.getQcontent());	
		
		service.modifyOne(ori);
		
		return "redirect:/qna.do?action=list";
	}
}
