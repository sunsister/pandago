package kr.co.pandago.customer;

import java.util.HashMap;
import java.util.Random;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import kr.co.pandago.dao.CustomerDAO;
import kr.co.pandago.dto.CustomerDTO;
import kr.co.pandago.service.CustomerService;
import kr.co.pandago.service.MailService;

@Controller
public class JoinController {
	@Resource(name = "customerService")
	private CustomerService cs;

	@Resource(name = "mailService")
	private MailService mailService;

	public void setCs(CustomerService cs) {
		this.cs = cs;
	}

	public void setMailService(MailService mailService) {
		this.mailService = mailService;
	}

	// 회원가입 이메일 인증
	@RequestMapping("/sendEmailKey")
	@ResponseBody
	public boolean sendMailAuth(HttpSession session, @RequestParam String email) {
		System.out.println("email : " + email);
		int ran = new Random().nextInt(100000) + 10000; // 10000 ~ 99999
		String joinCode = String.valueOf(ran);
		System.out.println("인증코드 : " + joinCode);
		session.setAttribute("joinCode", joinCode);

		String subject = "회원가입 인증 코드 발급 안내 입니다.";
		StringBuilder sb = new StringBuilder();
		sb.append("귀하의 인증 코드는 " + joinCode + " 입니다.");
		System.out.println("mailService :" + mailService);
		return mailService.send(subject, sb.toString(), "lietter2@naver.com", email, null);
	}

	@RequestMapping("/emailOk")
	@ResponseBody
	public String emailOk(HttpSession sesstion, @RequestParam String emailKey) {
		String emailOk = (String) sesstion.getAttribute("joinCode");
		System.out.println(emailOk);
		System.out.println(emailKey);
		if (emailOk.equals(emailKey)) {
			return "true";
		}
		return "false";
	}

	// 비밀번호찾기
	@RequestMapping("passWord")
	public String sendMailPassword(HttpSession session, @RequestParam String email, RedirectAttributes ra,
			@ModelAttribute("dao") CustomerDAO dao) {
		if (cs.readOne(email) != null) {
			if (!cs.readOne(email).getCid().equals(email)) {
				ra.addFlashAttribute("resultMsg", "입력하신 이메일의 회원정보와 가입된 아이디가 일치하지 않습니다.");
				return "redirect:/searchpw";
			}
			int ran = new Random().nextInt(100000) + 10000; // 10000 ~ 99999
			String password = String.valueOf(ran);
			// cs.updateInfo(user.getNo(), "password", password); // 해당 유저의 DB정보
			// 변경
			// cs.modif(cs.readOne(email).getPw(),"password", password);
			HashMap<String, String> hm = new HashMap<String, String>();
			System.out.println("email :" + email + "password :" + password);
			hm.put("cid", email);
			hm.put("pw", password);

			cs.modifyPw(hm);

			// 이메일전송
			String subject = "임시 비밀번호 발급 안내 입니다.";
			StringBuilder sb = new StringBuilder();
			sb.append("귀하의 임시 비밀번호는 " + "\n" + password + " 입니다.");
			mailService.send(subject, sb.toString(), "lietter2@naver.com", email, null);
			ra.addFlashAttribute("resultMsg", "귀하의 이메일 주소로 새로운 임시 비밀번호를 발송 하였습니다.");
		} else {
			ra.addFlashAttribute("resultMsg", "귀하의 이메일로 가입된 아이디가 존재하지 않습니다.");
		}
		return "redirect:/searchpw";
	}

	@RequestMapping("/join")
	public String write() {
		return "joinForm";
	}

	@RequestMapping("/searchpw")
	public String searchpw() {
		return "searchPw";
	}

	@RequestMapping("/joinOk")
	public String joinOk(@ModelAttribute("dto") CustomerDTO dto, HttpServletRequest req) {

		String data = req.getParameter("join");
		System.out.println(data);
		String dataS[] = data.split("/");

		dto = new CustomerDTO(dataS[0], dataS[1], dataS[2], dataS[3], dataS[4], dataS[5]);

		cs.writeOne(dto);

		return "home";
	}

	@RequestMapping("/valiDate")
	@ResponseBody
	public String valiDate(@ModelAttribute("dto") CustomerDTO dto, HttpServletRequest req,
			@ModelAttribute("dao") CustomerDAO dao) {
		String email = req.getParameter("email");

		System.out.println(cs.validateEmail(email));
		if (cs.validateEmail(email) != null) {
			return "false";
		}
		return "true";
	}

	@RequestMapping("/logout")
	public String logout(HttpSession session, @RequestParam("referrer") String referrer) {
		session.removeAttribute("cid");

		return "redirect:" + referrer;

	}

	@RequestMapping("/loginOk")
	@ResponseBody
	public String loginOk(HttpServletRequest req, HttpSession session, HttpServletResponse response) {
		String cid = req.getParameter("email");
		String pw = req.getParameter("pw");
		HashMap<String, String> hm = new HashMap<String, String>();
		System.out.println("email :" + cid + "pw:" + pw);
		// String rere = req.getHeader("referer");

		hm.put("cid", cid);
		hm.put("pw", pw);
		if (cs.loginOk(hm) != null) {
			session.setAttribute("cid", cid);
			return "true";
		}
		return "false";
	}

}
