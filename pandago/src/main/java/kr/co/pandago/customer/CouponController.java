package kr.co.pandago.customer;

import java.util.HashMap;
import java.util.List;
import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import kr.co.pandago.dto.CouponDTO;
import kr.co.pandago.service.CouponService;
import kr.co.pandago.service.MyCouponService;

@Controller
@RequestMapping(value="/coupon.do")
public class CouponController {
	
	@Resource(name="couponService")
	CouponService service;
	
	@Resource(name="mycouponService")
	MyCouponService mservice;
	
	// 
	@RequestMapping("")
	public ModelAndView cplist(){
		List<CouponDTO> list = service.readAll();
		return new ModelAndView("coupon", "list", list);
	}
	
	// 
	@ResponseBody
	@RequestMapping(params="action=getcp")
	public String getCoupon(@RequestParam("cpid") String cpid, HttpSession session){
		String result = "fail";
		
		HashMap<String, String> hm = new HashMap<String, String>();
		hm.put("cpid", cpid);
		hm.put("cid", (String) session.getAttribute("cid"));
		
		if(mservice.readOne(hm)==null){	// 
			try{
				mservice.writeOne(hm);
				result="success";
			}catch(Exception e){
				e.printStackTrace();
			}
		}else{
			System.out.println("null");
		}
		
		return result;
	}
}
