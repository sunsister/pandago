package kr.co.pandago.customer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import kr.co.pandago.dto.CustomerDTO;
import kr.co.pandago.dto.MyCouponDTO;
import kr.co.pandago.dto.PayDetailDTO;
import kr.co.pandago.dto.PaymentDTO;
import kr.co.pandago.dto.ProductDTO;
import kr.co.pandago.service.CartService;
import kr.co.pandago.service.CustomerService;
import kr.co.pandago.service.MyCouponService;
import kr.co.pandago.service.PayDetailService;
import kr.co.pandago.service.PaymentService;
import kr.co.pandago.service.ProductService;

@Controller
@RequestMapping(value="pay.do")
public class PayController {
	
	@Resource(name="mycouponService")
	MyCouponService mservice;
	
	@Resource(name="customerService")
	CustomerService cservice;
	
	@Resource(name="paymentService")
	PaymentService pservice;
	
	@Resource(name="paydetailService")
	PayDetailService pdservice;
	
	@Resource(name="productService")
	ProductService productservice;
	
	@Resource(name="cartService")
	CartService cartservice;
	
	// 바로 결제하기
	@RequestMapping(params="action=step1now")
	public ModelAndView payStep1Now(HttpSession session ,@RequestParam("pid") String pid,
			 @RequestParam("price") int price, @RequestParam("pname") String pname,@RequestParam("dimg") String dimg,
			@RequestParam("pdoption") String pdoption, @RequestParam(value="pdcount", required=false, defaultValue="1") String pdcount){
		
		String[] option = pdoption.split(",");
		ArrayList<HashMap<String, Object>> orderlist = new ArrayList<HashMap<String, Object>>();
		HashMap<String, Object> order = new HashMap<String, Object>();
		order.put("pid", pid);
		order.put("pname", pname);
		order.put("dimg", dimg);
		order.put("price", price);
		order.put("pdid", option[0]);
		order.put("pdcount", pdcount);	// 구매 개수
		if(option.length != 1){
			order.put("pdoption", option[1]);	// 사이즈, 컬러
			order.put("addprice", option[2]);
		}
		orderlist.add(order);
		
		ModelAndView mav = new ModelAndView();
		mav.setViewName("pay_step");
		mav.addObject("orderlist", orderlist);
		
		String cid = (String) session.getAttribute("cid");
		HashMap<String, Object> hm = new HashMap<String, Object>();
		hm.put("cid", cid);
		hm.put("cpok", 0);
		
		// 쿠폰
		List<MyCouponDTO> cplist = mservice.readDetailUsed(hm);
		mav.addObject("cplist", cplist);
		
		// 포인트
		CustomerDTO customer = cservice.readOne(cid);
		mav.addObject("customer", customer);
		return mav;
	}
	
	// 장바구니에서 결제
	@RequestMapping(params="action=step1cart")
	public ModelAndView payStep1Cart(HttpSession session, @RequestParam("cartlist") String cartlist){
		ArrayList<HashMap<String, Object>> orderlist = new ArrayList<HashMap<String, Object>>();
		ArrayList<String> clist = new ArrayList<String>(); 
		
		JsonParser parser = new JsonParser();
		JsonArray jsonarr = (JsonArray) parser.parse(cartlist);
		
		for(int i = 0; i <jsonarr.size(); i++){
			HashMap<String, Object> order = new HashMap<String, Object>();
			JsonObject obj = (JsonObject) jsonarr.get(i);
			String pid = obj.get("pid").getAsString();
			order.put("pid", obj.get("pid").getAsString());
			String pname = productservice.readOne(pid).getPname();
			order.put("pname", pname);
			order.put("dimg", obj.get("dimg").getAsString());
			order.put("price", obj.get("sprice").getAsInt());
			order.put("pdid", obj.get("pdid").getAsString());
			order.put("pdcount", obj.get("cnt").getAsInt());
			clist.add(obj.get("cartid").toString());
			orderlist.add(order);
		}
				
		ModelAndView mav = new ModelAndView();
		mav.setViewName("pay_step");
		mav.addObject("orderlist", orderlist);
	
		String cid = (String) session.getAttribute("cid");
		HashMap<String, Object> hm = new HashMap<String, Object>();
		hm.put("cid", cid);
		hm.put("cpok", 0);
	
		// 쿠폰
		List<MyCouponDTO> cplist = mservice.readDetailUsed(hm);
		mav.addObject("cplist", cplist);
		
		// 포인트
		CustomerDTO customer = cservice.readOne(cid);
		mav.addObject("customer", customer);
		
		// 장바구니 삭제
		for(String id : clist){
			id=id.replaceAll("\"", "");
			cartservice.removeOne(id);
		}
		return mav;
	}
	
	@RequestMapping(params="action=step2")
	public ModelAndView payStep2(HttpSession session, @ModelAttribute(value="paymentdto") PaymentDTO dto, 
			@RequestParam("order") String order, @RequestParam(value="cpprice", required=false ) Integer cpprice){
		String cid = (String) session.getAttribute("cid");
		dto.setCid(cid);
		pservice.writeOne(dto);
		String payid = pservice.getPayId(cid);
		int usedpoint = dto.getPoint(); 
		if(usedpoint != 0){
			HashMap<String, Object> hm = new HashMap<String, Object>();
			hm.put("usedpoint", usedpoint);
			hm.put("cid", cid);
			cservice.modifyPoint(hm);
		}
		
		ModelAndView mav = new ModelAndView();
		mav.setViewName("pay_step2");
		String mcid = dto.getMcid();
		if(mcid != null){
			mav.addObject("cpprice", cpprice);
			mservice.modifyOne(mcid);
		}
		
		// 포인트 적립
		HashMap<String, Object> hm = new HashMap<String, Object>();
		hm.put("cid", cid);
		hm.put("pprice", dto.getPprice());
		cservice.raisePoint(hm);
		
		String[] orderArr = order.split(",");
		int orderSize = orderArr.length / 4;
		List<PayDetailDTO> pdlist = new ArrayList<PayDetailDTO>();
		List<ProductDTO> product = new ArrayList<ProductDTO>();
		for(int i = 0; i < orderSize; i++){
			PayDetailDTO pddto = new PayDetailDTO();
			pddto.setPdid(orderArr[i*4]);
			pddto.setPayid(payid);
			String pid = orderArr[i*4+1];
			pddto.setPid(orderArr[i*4+1]);
			pddto.setCid(cid);
			pddto.setCnt(Integer.parseInt(orderArr[i*4+2]));
			pddto.setPrice(Integer.parseInt(orderArr[i*4+3]));
			pdservice.writeOne(pddto);
			pdlist.add(pddto);
			product.add(productservice.readOne(pid));
		}
		mav.addObject("payment", pservice.readOne(payid));
		mav.addObject("pdlist", pdlist);
		mav.addObject("product", product);
		return mav;
	}
}
