package kr.co.pandago.customer;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import kr.co.pandago.dto.ProductDTO;
import kr.co.pandago.dto.ReviewDTO;
import kr.co.pandago.service.ProductService;
import kr.co.pandago.service.ReviewService;

@Controller
public class ReviewController {

	@Resource(name="reviewService")
	private ReviewService rs;
	
	@Resource(name = "productService")
	private ProductService ps;
	
	@RequestMapping(value="myreview.do")
	public ModelAndView myReview(HttpSession session){
		
		ModelAndView mav = new ModelAndView();
		mav.setViewName("review");
		mav.addObject("paylist", rs.selectreview((String)session.getAttribute("cid")));
		mav.addObject("payid", rs.selectPayid());
		mav.addObject("payidlist", rs.selectPayid());
		mav.addObject("mylist", rs.selectMyreview((String)session.getAttribute("cid")));
		
		return mav;
	}
	
	@RequestMapping(value="myreviewinsert.do")
	public String myReviewinsert(HttpSession session,@RequestParam("payid")String payid,@RequestParam("pid")String pid,@RequestParam("title")String title,@RequestParam("content")String content,
									 @RequestParam("star")int star){
		ReviewDTO dto = new ReviewDTO();
		dto.setCid((String)session.getAttribute("cid"));
		dto.setPayid(payid);
		dto.setPid(pid);
		dto.setStar(star);
		try {
			dto.setTitle(URLDecoder.decode(title,"UTF-8"));
			dto.setContent(URLDecoder.decode(content,"UTF-8"));
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		ProductDTO pdto= new ProductDTO();
		pdto.setStar(star);
		pdto.setPid(pid);
		rs.writeOne(dto);
		ps.starupdate(pdto);
		

		return "redirect:/myreview.do";
	}
	@RequestMapping(value="myreviewdelete.do")
	public String myReviewdelete(@RequestParam("payid")String payid){
		System.out.println(payid);
		rs.removeOne(payid);
		
		return "redirect:/myreview.do";
	}
	
	
	
	
	
}
