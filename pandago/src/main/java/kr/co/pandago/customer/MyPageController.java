package kr.co.pandago.customer;

import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import kr.co.pandago.dto.QnaDTO;
import kr.co.pandago.service.CustomerService;
import kr.co.pandago.service.PaymentService;
import kr.co.pandago.service.QnaService;
import kr.co.pandago.service.ReviewService;

@Controller
@RequestMapping(value="/mypage.do")
public class MyPageController {
	
	@Resource(name="customerService")
	private CustomerService cservice;
	
	@Resource(name="qnaService")
	private QnaService qservice;
	
	@Resource(name="reviewService")
	private ReviewService rservice;
	
	@Resource(name="paymentService")
	private PaymentService pservice;
	
	@RequestMapping()
	public ModelAndView list(HttpSession session){
		String cid = (String) session.getAttribute("cid");
		ModelAndView mav = new ModelAndView("mypage");
		mav.addObject("customer", cservice.readOne(cid));
		
		// Qna
		HashMap<String, Object> hm = new HashMap<String, Object>();
		hm.put("startNo", 0);
		hm.put("endNo", 3);
		hm.put("cid", cid);
		List<QnaDTO> qna = qservice.readAll(hm);
		mav.addObject("qna", qna);
		System.out.println(rservice.selectMyreview(cid));
		// Review
		mav.addObject("review", rservice.selectMyreview(cid));
		
		// Payment
		mav.addObject("order", pservice.readAll(cid));
		
		return mav;
	}
}
