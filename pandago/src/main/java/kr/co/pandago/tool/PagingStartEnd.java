package kr.co.pandago.tool;

public class PagingStartEnd {
	private int startNo;
	private int endNo;
	
	
	
	public int getStartNo() {
		return startNo;
	}
	
	
	public void setStartNo(int startNo) {
		this.startNo = startNo;
	}
	
	
	public int getEndNo() {
		return endNo;
	}
	public void setEndNo(int endNo) {
		this.endNo = endNo;
	}
	
}
