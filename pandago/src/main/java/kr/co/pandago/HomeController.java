package kr.co.pandago;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;

import javax.annotation.Resource;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import kr.co.pandago.dto.CodeDTO;
import kr.co.pandago.dto.InquiryDTO;
import kr.co.pandago.dto.ProductDTO;
import kr.co.pandago.service.DetailImgService;
import kr.co.pandago.service.InquiryService;
import kr.co.pandago.service.MainimgService;
import kr.co.pandago.service.PdetailService;
import kr.co.pandago.service.ProductService;
import kr.co.pandago.service.ReviewService;

@Controller
public class HomeController {

	@Resource(name = "productService")
	private ProductService ps;

	@Resource(name="detailImgService")
	private DetailImgService dis;

	@Resource(name="mainImgService")
	private MainimgService mis;

	@Resource(name="pdetailService")
	private PdetailService pds;

	@Resource(name="inquiryService")
	private InquiryService is;

	@Resource(name="reviewService")
	private ReviewService rs;
	
	@RequestMapping(value = "/", method = RequestMethod.GET)

	public ModelAndView home() {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("home");
		ProductDTO dto= new ProductDTO();
		dto.setScnt(1);
		mav.addObject("list", ps.selectmain(dto));
		dto.setScnt(2);
		mav.addObject("bestlist", ps.selectmain(dto));
		dto.setScnt(3);
		mav.addObject("newlist", ps.selectmain(dto));

		return mav;
	}

	@RequestMapping(value = "/login")
	public String login() {
		return "login";
	}
	
	@RequestMapping(value = "/category.do")
	@ResponseBody
	public ModelAndView singletonPage(@RequestParam("currentPage") int currentPage, @RequestParam("lcode") String lcode, @RequestParam("lcodename") String lcodename, HttpServletRequest req) {
		// String category = cate;
		ProductDTO dto = new ProductDTO();
		dto.setLcode(lcode);
		if (req.getParameter("mcode") != null) {
			dto.setMcode(req.getParameter("mcode"));
		}
		if (req.getParameter("scode") != null) {
			dto.setScode(req.getParameter("scode"));
		}

		int total = ps.total(dto);

		//int cntPerPage = 20;
		//int totalPage = total % cntPerPage == 0 ? total / cntPerPage : total / cntPerPage + 1;

		//int startNo = (currentPage - 1) * cntPerPage + 1;
		//int endNo = currentPage * cntPerPage;

		//int startPageNum = currentPage - 5 <= 0 ? 1 : currentPage - 5;
		//int endPageNum = startPageNum + 10 >= totalPage ? totalPage : startPageNum + 10;

		dto.setEndNo(paging(dto, currentPage, total).getEndNo());
		dto.setStartNo(paging(dto, currentPage, total).getStartNo());

		ModelAndView mav = new ModelAndView();

		mav.setViewName("category");
		mav.addObject("list", ps.readAll(dto));
		mav.addObject("count", total);
		mav.addObject("currentPage", currentPage);
		ArrayList<CodeDTO> clist1 = (ArrayList<CodeDTO>) ps.selectCode1(lcode);
		ArrayList<CodeDTO> clist2 = (ArrayList<CodeDTO>) ps.selectCode2(lcode);
		mav.addObject("clist1", clist1);
		mav.addObject("clist2", clist2);
		mav.addObject("lcode", lcode);
		mav.addObject("lcodename", lcodename);
		mav.addObject("mimg", ps.selectmainimg(lcode));
		mav.addObject("beginpagenum", paging(dto,currentPage,total).getBeginpagenum());
		mav.addObject("endpagenum", paging(dto,currentPage,total).getEndpagenum());

		return mav;// new ModelAndView("category","list",ps.readAll(dto));

	}

	@RequestMapping(value = "/lcodeserch.do")
	@ResponseBody
	public ModelAndView lcodeserch(HttpServletRequest req) {
		int currentPage = Integer.parseInt(req.getParameter("currentPage"));

		ProductDTO dto = new ProductDTO();

		dto.setLcode(req.getParameter("lcode"));

		if (req.getParameter("mcode") == null || req.getParameter("mcode").equals("undefined")
				|| req.getParameter("mcode") == "") {
		} else {
			dto.setMcode(req.getParameter("mcode"));
		}

		if (req.getParameter("scode") == null || req.getParameter("scode").equals("undefined")
				|| req.getParameter("scode") == "") {

		} else {
			dto.setScode(req.getParameter("scode"));
		}
		
		dto.setPname(req.getParameter("pname"));
		dto.setScnt(Integer.parseInt(req.getParameter("scnt")));

		int total = ps.total(dto);
		 	
		 dto.setEndNo(paging(dto,currentPage,total).getEndNo());
		 dto.setStartNo(paging(dto,currentPage,total).getStartNo());
		 dto.setBeginpagenum(paging(dto,currentPage,total).getBeginpagenum());
		 dto.setEndpagenum(paging(dto,currentPage,total).getEndpagenum());
		
		 
		 ArrayList<ProductDTO> alist = (ArrayList<ProductDTO>) ps.readAll(dto);
		 ModelAndView mav = new ModelAndView("jsonView");
		 mav.setViewName("jsonView");
		 mav.addObject("list", alist);
		 mav.addObject("count", total);
		 mav.addObject("currentPage", currentPage);
		 mav.addObject("beginpagenum", paging(dto,currentPage,total).getBeginpagenum());
		 mav.addObject("endpagenum", paging(dto,currentPage,total).getEndpagenum());
		 
		 return mav;
	}
	
/*	@RequestMapping(value = "/mypage", params = { "cid" })
	public ModelAndView mypage(HttpSession session) {
		String tCid = (String)session.getAttribute("cid");
		return new ModelAndView("mypage", "tCid", tCid);
	}*/
//	public ModelAndView mypage(@RequestParam("cid") String cid) {
//		String tCid = cid;
//		return new ModelAndView("mypage", "tCid", tCid);
//	}

	@RequestMapping(value = "/view.do", params = { "no" })
	public ModelAndView view(@RequestParam("no") String no, HttpServletRequest req,  HttpServletResponse resp,
			@CookieValue(value="CURPRODUCT", required=false)Cookie cookie, HttpSession session) throws UnsupportedEncodingException {
		String pno = no;
		System.out.println(pno);
		ModelAndView mav = new ModelAndView();
	
		ProductDTO dto = ps.readOne(no); 
		InquiryDTO isdto = new InquiryDTO();
		
		// 최근본상품
		if(cookie == null){
			Cookie ck = new Cookie("CURPRODUCT",pno+"*"+dto.getPath());
			
			resp.addCookie(ck);
		}else{
			String[] precoo = URLDecoder.decode(cookie.getValue(), "UTF-8").split(",");
			String newcoo = "";
			String first = pno+"*"+dto.getPath();
			newcoo += first+",";
			int last = 4;
			for(int k = 0; k < precoo.length; k++){			
				if(k < last){
					if(!first.equals(precoo[k])){
						newcoo += precoo[k];
						if(k < precoo.length-1)
							newcoo+=",";
					}
				}else{
					break;
				}
			}
			
			cookie.setValue(newcoo);
			try{
				cookie = new Cookie("CURPRODUCT", URLEncoder.encode(newcoo,"utf-8"));
				resp.addCookie(cookie);
			}catch(Exception e){
				System.out.println("쿠키에러!");
				e.printStackTrace();
			}
			
		}
		
		isdto.setPid(pno);
		mav.setViewName("detail_pruduct");
		mav.addObject("detail", dto);
		mav.addObject("mainimg", mis.selectOneimg(pno));
		mav.addObject("dimg", dis.readAllimg(no));
		mav.addObject("islist", is.readlistAll(isdto));
		mav.addObject("rslist", rs.selectAllList(no));
		mav.addObject("option", pds.readAllop(no));
		mav.addObject("cid",session.getAttribute("cid") );
		
		return mav;
	}

	
	public ProductDTO paging(ProductDTO dto, int currentPage, int total) {
		
		
		int cntPerPage = 20;
		
		//총페이지 수
		int totalpage = (total%20==0)?total/20:total/20+1;
		
		int startNo = (currentPage - 1) * cntPerPage + 1;
		int endNo = currentPage * cntPerPage;
		
		int beginpagenum = currentPage-5;
		int endpagenum = currentPage+5;
		
		if(currentPage<=5){
			beginpagenum = 1;
			endpagenum = totalpage>=10?beginpagenum+10:totalpage;
		}
		
		if(endpagenum>totalpage){
			endpagenum=totalpage;
			
			
		}
		
		
		if(totalpage-currentPage<5){
			
			endpagenum = totalpage;
			
		}
		
		
		dto.setEndNo(endNo);
		dto.setStartNo(startNo);
		dto.setBeginpagenum(beginpagenum);
		dto.setEndpagenum(endpagenum);
		return dto;
	}
	
	@RequestMapping(value = "/selectsearch.do")
	public ModelAndView selectSearch(HttpServletRequest req) {
		int currentPage = Integer.parseInt(req.getParameter("currentPage"));
		ProductDTO dto = new ProductDTO();
		dto.setPname(req.getParameter("pname"));
		
		int total = ps.searchTotal(dto);
		
		
		//System.out.println(total);
		//System.out.println(currentPage);
		//System.out.println(req.getParameter("pname"));
		
		dto.setEndNo(paging(dto,currentPage,total).getEndNo());
		dto.setStartNo(paging(dto,currentPage,total).getStartNo());
		
		ArrayList<ProductDTO> alist = (ArrayList<ProductDTO>) ps.selectSearch(dto);
		ModelAndView mav = new ModelAndView();
		mav.setViewName("searchproduct");
		mav.addObject("list", alist);
		mav.addObject("count", total);
		mav.addObject("currentPage", currentPage);
		mav.addObject("pname", req.getParameter("pname"));
		mav.addObject("beginpagenum", paging(dto,currentPage,total).getBeginpagenum());
		mav.addObject("endpagenum", paging(dto,currentPage,total).getEndpagenum());
		
		return mav;
	}
	
	
}
