package kr.co.pandago.dto;

public class PaymentDTO {
	private String payid;
	private String cid;
	private String ostate;
	private int pprice;
	private String pdate;
	private String pmethod;
	private int dprice;
	private String cpid;
	private int point;
	private String mcid;
	private String addr;
	private String name;
	
	
	public String getAddr() {
		return addr;
	}

	public void setAddr(String addr) {
		this.addr = addr;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public PaymentDTO() {
	}

	public PaymentDTO(String payid, String cid, String ostate, int pprice, String pdate, 
			String pmethod, int dprice, String cpid, int point, String mcid) {
		this.payid = payid;
		this.cid = cid;
		this.ostate = ostate;
		this.pprice = pprice;
		this.pdate = pdate;
		this.pmethod = pmethod;
		this.dprice = dprice;
		this.cpid = cpid;
		this.point = point;
		this.mcid = mcid;
	}

	public String getPayid() {
		return payid;
	}

	public void setPayid(String payid) {
		this.payid = payid;
	}

	public String getCid() {
		return cid;
	}

	public void setCid(String cid) {
		this.cid = cid;
	}

	public String getOstate() {
		return ostate;
	}

	public void setOstate(String ostate) {
		this.ostate = ostate;
	}

	public int getPprice() {
		return pprice;
	}

	public void setPprice(int pprice) {
		this.pprice = pprice;
	}

	public String getPdate() {
		return pdate;
	}

	public void setPdate(String pdate) {
		this.pdate = pdate;
	}

	public String getPmethod() {
		return pmethod;
	}

	public void setPmethod(String pmethod) {
		this.pmethod = pmethod;
	}

	public int getDprice() {
		return dprice;
	}

	public void setDprice(int dprice) {
		this.dprice = dprice;
	}

	public String getCpid() {
		return cpid;
	}

	public void setCpid(String cpid) {
		this.cpid = cpid;
	}

	public int getPoint() {
		return point;
	}

	public void setPoint(int point) {
		this.point = point;
	}

	public String getMcid() {
		return mcid;
	}

	public void setMcid(String mcid) {
		this.mcid = mcid;
	}

}
