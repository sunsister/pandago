package kr.co.pandago.dto;

public class PayDetailDTO {
	private String paydid;
	private String pdid;
	private String payid;
	private String pid;
	private String cid;
	private int cnt;
	private int price;
	private String pdate;
	private String path;
	private String pname;
	private String ostate;
	private String name;
	private String addr;
	private int pprice;
	private String pmethod;
	
	
	public int getPprice() {
		return pprice;
	}


	public void setPprice(int pprice) {
		this.pprice = pprice;
	}


	public String getPmethod() {
		return pmethod;
	}


	public void setPmethod(String pmethod) {
		this.pmethod = pmethod;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getAddr() {
		return addr;
	}


	public void setAddr(String addr) {
		this.addr = addr;
	}


	public String getPname() {
		return pname;
	}
	

	public String getOstate() {
		return ostate;
	}


	public void setOstate(String ostate) {
		this.ostate = ostate;
	}


	public void setPname(String pname) {
		this.pname = pname;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public PayDetailDTO(String paydid, String pdid, String payid, String pid, String cid, int cnt, int price,
			String pdate, String path) {
		super();
		this.paydid = paydid;
		this.pdid = pdid;
		this.payid = payid;
		this.pid = pid;
		this.cid = cid;
		this.cnt = cnt;
		this.price = price;
		this.pdate = pdate;
		this.path = path;
	}

	public PayDetailDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getPdate() {
		return pdate;
	}

	public void setPdate(String pdate) {
		this.pdate = pdate;
	}

	public PayDetailDTO(String paydid, String pdid, String payid, String pid, String cid, int cnt, int price) {
		super();
		this.paydid = paydid;
		this.pdid = pdid;
		this.payid = payid;
		this.pid = pid;
		this.cid = cid;
		this.cnt = cnt;
		this.price = price;
	}
	
	public PayDetailDTO(String paydid, String pdid, String payid, String pid, String cid, int cnt, int price,
			String pdate) {
		super();
		this.paydid = paydid;
		this.pdid = pdid;
		this.payid = payid;
		this.pid = pid;
		this.cid = cid;
		this.cnt = cnt;
		this.price = price;
		this.pdate = pdate;
	}

	public String getPaydid() {
		return paydid;
	}

	public void setPaydid(String paydid) {
		this.paydid = paydid;
	}

	public String getPdid() {
		return pdid;
	}

	public void setPdid(String pdid) {
		this.pdid = pdid;
	}

	public String getPayid() {
		return payid;
	}

	public void setPayid(String payid) {
		this.payid = payid;
	}

	public String getPid() {
		return pid;
	}

	public void setPid(String pid) {
		this.pid = pid;
	}

	public String getCid() {
		return cid;
	}

	public void setCid(String cid) {
		this.cid = cid;
	}

	public int getCnt() {
		return cnt;
	}

	public void setCnt(int cnt) {
		this.cnt = cnt;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

}
