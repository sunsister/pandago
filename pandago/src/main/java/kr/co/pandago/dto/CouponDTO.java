package kr.co.pandago.dto;

public class CouponDTO {
	private String cpid;
	private String cpname;
	private int cpprice;
	private String sdate;
	private String edate;
	private int minprice;

	public CouponDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public CouponDTO(String cpid, String cpname, int cpprice, String sdate, String edate, int minprice) {
		super();
		this.cpid = cpid;
		this.cpname = cpname;
		this.cpprice = cpprice;
		this.sdate = sdate;
		this.edate = edate;
		this.minprice = minprice;
	}

	public String getCpid() {
		return cpid;
	}

	public void setCpid(String cpid) {
		this.cpid = cpid;
	}

	public String getCpname() {
		return cpname;
	}

	public void setCpname(String cpname) {
		this.cpname = cpname;
	}

	public int getCpprice() {
		return cpprice;
	}

	public void setCpprice(int cpprice) {
		this.cpprice = cpprice;
	}

	public String getSdate() {
		return sdate;
	}

	public void setSdate(String sdate) {
		this.sdate = sdate;
	}

	public String getEdate() {
		return edate;
	}

	public void setEdate(String edate) {
		this.edate = edate;
	}

	public int getMinprice() {
		return minprice;
	}

	public void setMinprice(int minprice) {
		this.minprice = minprice;
	}

}
