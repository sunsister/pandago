package kr.co.pandago.dto;

public class MyCouponDTO {
	private String mcid;
	private String cid;
	private String cpid;
	private int cpok;
	private String cpdate;
	private String getdate;
	
	// Coupon
	private String cpname;
	private int cpprice;
	private String sdate;
	private String edate;
	private int minprice;
	
	public MyCouponDTO() {
	}

	public MyCouponDTO(String mcid, String cid, String cpid, int cpok, String cpdate, String getdate) {
		this.mcid = mcid;
		this.cid = cid;
		this.cpid = cpid;
		this.cpok = cpok;
		this.cpdate = cpdate;
		this.getdate = getdate;
	}
	
	// MyCoupon, Coupon Join
	public MyCouponDTO(String mcid, String cid, String cpid, int cpok, String cpdate, String getdate, 
			String cpname, int cpprice, String sdate, String edate, int minprice) {
		this.mcid = mcid;
		this.cid = cid;
		this.cpid = cpid;
		this.cpok = cpok;
		this.cpdate = cpdate;
		this.getdate = getdate;
		this.cpname = cpname;
		this.cpprice = cpprice;
		this.sdate = sdate;
		this.edate = edate;
		this.minprice = minprice;
	}

	public String getMcid() {
		return mcid;
	}

	public void setMcid(String mcid) {
		this.mcid = mcid;
	}

	public String getCid() {
		return cid;
	}

	public void setCid(String cid) {
		this.cid = cid;
	}

	public String getCpid() {
		return cpid;
	}

	public void setCpid(String cpid) {
		this.cpid = cpid;
	}

	public int getCpok() {
		return cpok;
	}

	public void setCpok(int cpok) {
		this.cpok = cpok;
	}

	public String getCpdate() {
		return cpdate;
	}

	public void setCpdate(String cpdate) {
		this.cpdate = cpdate;
	}

	public String getGetdate() {
		return getdate;
	}

	public void setGetdate(String getdate) {
		this.getdate = getdate;
	}

	public String getCpname() {
		return cpname;
	}

	public void setCpname(String cpname) {
		this.cpname = cpname;
	}

	public int getCpprice() {
		return cpprice;
	}

	public void setCpprice(int cpprice) {
		this.cpprice = cpprice;
	}

	public String getSdate() {
		return sdate;
	}

	public void setSdate(String sdate) {
		this.sdate = sdate;
	}

	public String getEdate() {
		return edate;
	}

	public void setEdate(String edate) {
		this.edate = edate;
	}

	public int getMinprice() {
		return minprice;
	}

	public void setMinprice(int minprice) {
		this.minprice = minprice;
	}
	
}
