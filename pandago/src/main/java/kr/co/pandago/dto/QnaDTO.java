package kr.co.pandago.dto;

public class QnaDTO {
	private String qnaid;
	private String cid;
	private String qcode;
	private String title;
	private String qcontent;
	private String qdate;
	private String acontent;
	private String adate;
	private int aok;
	
	public QnaDTO() {
		
	}
	
	public QnaDTO(String qnaid, String cid, String qcode, String title, String qcontent, String qdate, String acontent,
			String adate, int aok) {
		this.qnaid = qnaid;
		this.cid = cid;
		this.qcode = qcode;
		this.title = title;
		this.qcontent = qcontent;
		this.qdate = qdate;
		this.acontent = acontent;
		this.adate = adate;
		this.aok = aok;
	}

	// QnaDTO 생성자 추가
	public QnaDTO(String cid, String qcode, String title, String qcontent) {
		this.cid = cid;
		this.qcode = qcode;
		this.title = title;
		this.qcontent = qcontent;
	}
	

	public String getQnaid() {
		return qnaid;
	}

	public void setQnaid(String qnaid) {
		this.qnaid = qnaid;
	}

	public String getCid() {
		return cid;
	}

	public void setCid(String cid) {
		this.cid = cid;
	}

	public String getQcode() {
		return qcode;
	}

	public void setQcode(String qcode) {
		this.qcode = qcode;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getQcontent() {
		return qcontent;
	}

	public void setQcontent(String qcontent) {
		this.qcontent = qcontent;
	}

	public String getQdate() {
		return qdate;
	}

	public void setQdate(String qdate) {
		this.qdate = qdate;
	}

	public String getAcontent() {
		return acontent;
	}

	public void setAcontent(String acontent) {
		this.acontent = acontent;
	}

	public String getAdate() {
		return adate;
	}

	public void setAdate(String adate) {
		this.adate = adate;
	}

	public int getAok() {
		return aok;
	}

	public void setAok(int aok) {
		this.aok = aok;
	}

}
