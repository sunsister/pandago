package kr.co.pandago.dto;

public class ProductDTO {
	
	private String pid; 
	private String pname;
	private String lcode;
	private String mcode;
	private String scode;
	private String path;
	private int price;
	private int sprice;
	private int sale;
	private String factory;
	private int scnt;
	private String regdate;
	private int star;
	private int startNo;
	private int endNo;
	private int endpagenum;
	private int beginpagenum;
	
	public ProductDTO() {
	}

	
	
	

	public ProductDTO(String pid, String pname, String lcode, String mcode, String scode, String path, int price,
			int sprice, int sale, String factory, int scnt, String regdate, int star, int startNo, int endNo,
			int endpagenum, int beginpagenum) {
		super();
		this.pid = pid;
		this.pname = pname;
		this.lcode = lcode;
		this.mcode = mcode;
		this.scode = scode;
		this.path = path;
		this.price = price;
		this.sprice = sprice;
		this.sale = sale;
		this.factory = factory;
		this.scnt = scnt;
		this.regdate = regdate;
		this.star = star;
		this.startNo = startNo;
		this.endNo = endNo;
		this.endpagenum = endpagenum;
		this.beginpagenum = beginpagenum;
	}





	public int getEndpagenum() {
		return endpagenum;
	}

	public void setEndpagenum(int endpagenum) {
		this.endpagenum = endpagenum;
	}

	public int getBeginpagenum() {
		return beginpagenum;
	}

	public void setBeginpagenum(int beginpagenum) {
		this.beginpagenum = beginpagenum;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getPid() {
		return pid;
	}

	public void setPid(String pid) {
		this.pid = pid;
	}

	public String getPname() {
		return pname;
	}

	public void setPname(String pname) {
		this.pname = pname;
	}

	public String getLcode() {
		return lcode;
	}

	public void setLcode(String lcode) {
		this.lcode = lcode;
	}

	public String getMcode() {
		return mcode;
	}

	public void setMcode(String mcode) {
		this.mcode = mcode;
	}

	public String getScode() {
		return scode;
	}

	public void setScode(String scode) {
		this.scode = scode;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public int getSprice() {
		return sprice;
	}

	public void setSprice(int sprice) {
		this.sprice = sprice;
	}

	public int getSale() {
		return sale;
	}

	public void setSale(int sale) {
		this.sale = sale;
	}

	public String getFactory() {
		return factory;
	}

	public void setFactory(String factory) {
		this.factory = factory;
	}

	public int getScnt() {
		return scnt;
	}

	public void setScnt(int scnt) {
		this.scnt = scnt;
	}

	public String getRegdate() {
		return regdate;
	}

	public void setRegdate(String regdate) {
		this.regdate = regdate;
	}

	public int getStar() {
		return star;
	}

	
	
	public void setStar(int star) {
		this.star = star;
	}
		
	
	public int getStartNo() {
		return startNo;
	}
	
	
	public void setStartNo(int startNo) {
		this.startNo = startNo;
	}
	
	
	public int getEndNo() {
		return endNo;
	}
	public void setEndNo(int endNo) {
		this.endNo = endNo;
	}
	
	
	
	
	
	
}
