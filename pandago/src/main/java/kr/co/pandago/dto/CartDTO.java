package kr.co.pandago.dto;

public class CartDTO {
	private String cid;
	private int cnt;
	private String pdid;
	private String pid;
	private String color;
	private String psize;
	private int addprice;
	private String pname;
	private int price;
	private int sprice;
	private int sale;
	private String factory;
	private String path;
	private String cartid;

	public CartDTO() {

	}

	public CartDTO(String cid, int cnt, String pdid, String pid) {
		super();
		this.cid = cid;
		this.cnt = cnt;
		this.pdid = pdid;
		this.pid = pid;
	}
	
	
	

	public CartDTO(String cid, int cnt, String pdid, String pid, String color, String psize, int addprice, String pname,
			int price, int sprice, int sale, String factory, String path, String cartid) {
		super();
		this.cid = cid;
		this.cnt = cnt;
		this.pdid = pdid;
		this.pid = pid;
		this.color = color;
		this.psize = psize;
		this.addprice = addprice;
		this.pname = pname;
		this.price = price;
		this.sprice = sprice;
		this.sale = sale;
		this.factory = factory;
		this.path = path;
		this.cartid = cartid;
	}

	public CartDTO(String cid, int cnt, String pdid, String pid, String color, String psize, int addprice, String pname,
			int price, int sprice, int sale, String factory) {
		super();
		this.cid = cid;
		this.cnt = cnt;
		this.pdid = pdid;
		this.pid = pid;
		this.color = color;
		this.psize = psize;
		this.addprice = addprice;
		this.pname = pname;
		this.price = price;
		this.sprice = sprice;
		this.sale = sale;
		this.factory = factory;
	}

	public String getCid() {
		return cid;
	}

	public void setCid(String cid) {
		this.cid = cid;
	}

	public int getCnt() {
		return cnt;
	}

	public void setCnt(int cnt) {
		this.cnt = cnt;
	}

	public String getPdid() {
		return pdid;
	}

	public void setPdid(String pdid) {
		this.pdid = pdid;
	}

	public String getPid() {
		return pid;
	}

	public void setPid(String pid) {
		this.pid = pid;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getPsize() {
		return psize;
	}

	public void setPsize(String psize) {
		this.psize = psize;
	}

	public int getAddprice() {
		return addprice;
	}

	public void setAddprice(int addprice) {
		this.addprice = addprice;
	}

	public String getPname() {
		return pname;
	}

	public void setPname(String pname) {
		this.pname = pname;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public int getSprice() {
		return sprice;
	}

	public void setSprice(int sprice) {
		this.sprice = sprice;
	}

	public int getSale() {
		return sale;
	}

	public void setSale(int sale) {
		this.sale = sale;
	}

	public String getFactory() {
		return factory;
	}

	public void setFactory(String factory) {
		this.factory = factory;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getCartid() {
		return cartid;
	}

	public void setCartid(String cartid) {
		this.cartid = cartid;
	}
	
	

}
