package kr.co.pandago.dto;

public class InquiryDTO {

	private String inid;
	private String pid;
	private String cid;
	private String inpw;
	private String intitle;
	private String incontent;
	private String indate;
	private String inacontent;
	private int inaok;
	private int insecret;
	private int startNo;
	private int endNo;
	private int rn;
	
	
	public InquiryDTO() {
	}


	
	public InquiryDTO(String inid, String pid, String cid, String inpw, String intitle, String incontent, String indate,
			String inacontent, int inaok, int insecret, int startNo, int endNo, int rn) {
		super();
		this.inid = inid;
		this.pid = pid;
		this.cid = cid;
		this.inpw = inpw;
		this.intitle = intitle;
		this.incontent = incontent;
		this.indate = indate;
		this.inacontent = inacontent;
		this.inaok = inaok;
		this.insecret = insecret;
		this.startNo = startNo;
		this.endNo = endNo;
		this.rn = rn;
	}



	public int getStartNo() {
		return startNo;
	}


	public void setStartNo(int startNo) {
		this.startNo = startNo;
	}


	public int getEndNo() {
		return endNo;
	}


	public void setEndNo(int endNo) {
		this.endNo = endNo;
	}


	public int getRn() {
		return rn;
	}


	public void setRn(int rn) {
		this.rn = rn;
	}


	public String getInid() {
		return inid;
	}


	public void setInid(String inid) {
		this.inid = inid;
	}


	public String getPid() {
		return pid;
	}


	public void setPid(String pid) {
		this.pid = pid;
	}


	public String getCid() {
		return cid;
	}


	public void setCid(String cid) {
		this.cid = cid;
	}


	public String getInpw() {
		return inpw;
	}


	public void setInpw(String inpw) {
		this.inpw = inpw;
	}


	public String getIntitle() {
		return intitle;
	}


	public void setIntitle(String intitle) {
		this.intitle = intitle;
	}


	public String getIncontent() {
		return incontent;
	}


	public void setIncontent(String incontent) {
		this.incontent = incontent;
	}


	public String getIndate() {
		return indate;
	}


	public void setIndate(String indate) {
		this.indate = indate;
	}


	public String getInacontent() {
		return inacontent;
	}


	public void setInacontent(String inacontent) {
		this.inacontent = inacontent;
	}


	public int getInaok() {
		return inaok;
	}


	public void setInaok(int inaok) {
		this.inaok = inaok;
	}


	public int getInsecret() {
		return insecret;
	}


	public void setInsecret(int insecret) {
		this.insecret = insecret;
	}
	
	
	
	
	
	
	
}
