package kr.co.pandago.dto;

public class CodeDTO {
	
	private String ccode;
	private int clevel;
	private String codename;
	private String upcode;
	private String lcodename;
	private String lccode;
	
	
	public CodeDTO() {
	}


	

	public CodeDTO(String ccode, int clevel, String codename, String upcode, String lcodename, String lccode) {
		super();
		this.ccode = ccode;
		this.clevel = clevel;
		this.codename = codename;
		this.upcode = upcode;
		this.lcodename = lcodename;
		this.lccode = lccode;
	}




	public String getLcodename() {
		return lcodename;
	}

	public void setLcodename(String lcodename) {
		this.lcodename = lcodename;
	}

	public String getLccode() {
		return lccode;
	}




	public void setLccode(String lccode) {
		this.lccode = lccode;
	}




	public String getCcode() {
		return ccode;
	}


	public void setCcode(String ccode) {
		this.ccode = ccode;
	}


	public int getClevel() {
		return clevel;
	}


	public void setClevel(int clevel) {
		this.clevel = clevel;
	}


	public String getCodename() {
		return codename;
	}


	public void setCodename(String codename) {
		this.codename = codename;
	}


	public String getUpcode() {
		return upcode;
	}


	public void setUpcode(String upcode) {
		this.upcode = upcode;
	}
	
	
	
	
	

}
