package kr.co.pandago.dto;

public class CustomerDTO {
	private String cid;
	private String pw;
	private String name;
	private String hp;
	private String regdate;
	private String pnum;
	private String addr;
	private String grade;
	private int tmoney;
	private int point;
	

	public CustomerDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public CustomerDTO(String cid, String pw, String name, String hp, String regdate, String pnum, String addr,
			String grade, int tmoney, int point) {
		super();
		this.cid = cid;
		this.pw = pw;
		this.name = name;
		this.hp = hp;
		this.regdate = regdate;
		this.pnum = pnum;
		this.addr = addr;
		this.grade = grade;
		this.tmoney = tmoney;
		this.point = point;
	}
	
	
	public CustomerDTO(String cid, String pw, String name, String hp, String pnum, String addr) {
		super();
		this.cid = cid;
		this.pw = pw;
		this.name = name;
		this.hp = hp;
		this.pnum = pnum;
		this.addr = addr;
	}



	public String getCid() {
		return cid;
	}

	public void setCid(String cid) {
		this.cid = cid;
	}

	public String getPw() {
		return pw;
	}

	public void setPw(String pw) {
		this.pw = pw;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getHp() {
		return hp;
	}

	public void setHp(String hp) {
		this.hp = hp;
	}

	public String getRegdate() {
		return regdate;
	}

	public void setRegdate(String regdate) {
		this.regdate = regdate;
	}

	public String getPnum() {
		return pnum;
	}

	public void setPnum(String pnum) {
		this.pnum = pnum;
	}

	public String getAddr() {
		return addr;
	}

	public void setAddr(String addr) {
		this.addr = addr;
	}

	public String getGrade() {
		return grade;
	}

	public void setGrade(String grade) {
		this.grade = grade;
	}

	public int getTmoney() {
		return tmoney;
	}

	public void setTmoney(int tmoney) {
		this.tmoney = tmoney;
	}

	public int getPoint() {
		return point;
	}

	public void setPoint(int point) {
		this.point = point;
	}

}
