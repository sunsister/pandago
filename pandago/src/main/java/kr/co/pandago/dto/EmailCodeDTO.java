package kr.co.pandago.dto;

public class EmailCodeDTO {
String email;
String pcode;
int status;
public EmailCodeDTO() {
	super();
	// TODO Auto-generated constructor stub
}
public EmailCodeDTO(String email, String pcode, int status) {
	super();
	this.email = email;
	this.pcode = pcode;
	this.status = status;
}
public String getEmail() {
	return email;
}
public void setEmail(String email) {
	this.email = email;
}
public String getPcode() {
	return pcode;
}
public void setPcode(String pcode) {
	this.pcode = pcode;
}
public int getStatus() {
	return status;
}
public void setStatus(int status) {
	this.status = status;
}

}
