package kr.co.pandago.dto;

public class DetailImgDTO {

	
	private String dimg; 
	private String pid;
	private String path;
	
	public DetailImgDTO() {
	}

	public DetailImgDTO(String dimg, String pid, String path) {
		super();
		this.dimg = dimg;
		this.pid = pid;
		this.path = path;
	}

	public String getDimg() {
		return dimg;
	}

	public void setDimg(String dimg) {
		this.dimg = dimg;
	}

	public String getPid() {
		return pid;
	}

	public void setPid(String pid) {
		this.pid = pid;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}
	
	
	
	
}
