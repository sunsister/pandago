package kr.co.pandago.dto;

public class ReviewDTO {

	private String payid;
	private String pid;
	private String cid;
	private String title;
	private String content;
	private String img;
	private String reply;
	private String wdate;
	private int star;
	private String ostate;
	private int pprice;
	private String pdate;
	private String pmethod;
	private int cnt;
	private String path;
	private String pname; 

	public ReviewDTO() {
	}

	
	public ReviewDTO(String payid, String pid, String cid, String title, String content, String img, String reply,
			String wdate, int star) {
		super();
		this.payid = payid;
		this.pid = pid;
		this.cid = cid;
		this.title = title;
		this.content = content;
		this.img = img;
		this.reply = reply;
		this.wdate = wdate;
		this.star = star;
	}

	
	public String getOstate() {
		return ostate;
	}


	public void setOstate(String ostate) {
		this.ostate = ostate;
	}


	public int getPprice() {
		return pprice;
	}


	public void setPprice(int pprice) {
		this.pprice = pprice;
	}


	public String getPdate() {
		return pdate;
	}


	public void setPdate(String pdate) {
		this.pdate = pdate;
	}


	public String getPmethod() {
		return pmethod;
	}


	public void setPmethod(String pmethod) {
		this.pmethod = pmethod;
	}


	public int getCnt() {
		return cnt;
	}


	public void setCnt(int cnt) {
		this.cnt = cnt;
	}


	public String getPath() {
		return path;
	}


	public void setPath(String path) {
		this.path = path;
	}


	public String getPname() {
		return pname;
	}


	public void setPname(String pname) {
		this.pname = pname;
	}


	public String getPayid() {
		return payid;
	}

	public void setPayid(String payid) {
		this.payid = payid;
	}

	public String getPid() {
		return pid;
	}

	public void setPid(String pid) {
		this.pid = pid;
	}

	public String getCid() {
		return cid;
	}

	public void setCid(String cid) {
		this.cid = cid;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}

	public String getReply() {
		return reply;
	}

	public void setReply(String reply) {
		this.reply = reply;
	}

	public String getWdate() {
		return wdate;
	}

	public void setWdate(String wdate) {
		this.wdate = wdate;
	}

	public int getStar() {
		return star;
	}

	public void setStar(int star) {
		this.star = star;
	}

}
