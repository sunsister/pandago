package kr.co.pandago.dto;

public class GradeDTO {
	
	private String grade;
	private int goption;

	
	public GradeDTO() {
	}


	public GradeDTO(String grade, int goption) {
		super();
		this.grade = grade;
		this.goption = goption;
	}


	public String getGrade() {
		return grade;
	}


	public void setGrade(String grade) {
		this.grade = grade;
	}


	public int getGoption() {
		return goption;
	}


	public void setGoption(int goption) {
		this.goption = goption;
	}
	
	
	
	
	
	

}
