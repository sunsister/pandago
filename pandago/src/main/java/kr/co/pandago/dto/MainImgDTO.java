package kr.co.pandago.dto;

public class MainImgDTO {
	
	private String mimg; 
	private String pid;
	private String path;
	
	
	public MainImgDTO() {
	}


	public MainImgDTO(String mimg, String pid, String path) {
		super();
		this.mimg = mimg;
		this.pid = pid;
		this.path = path;
	}


	public String getMimg() {
		return mimg;
	}


	public void setMimg(String mimg) {
		this.mimg = mimg;
	}


	public String getPid() {
		return pid;
	}


	public void setPid(String pid) {
		this.pid = pid;
	}


	public String getPath() {
		return path;
	}


	public void setPath(String path) {
		this.path = path;
	}

	
	
	
	
}
