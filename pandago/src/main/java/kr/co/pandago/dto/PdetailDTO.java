package kr.co.pandago.dto;

public class PdetailDTO {
	
	private String pdid; 
	private String pid;
	private int stock;
	private String color;
	private String psize;
	private int addprice;
	
	public PdetailDTO() {
	}

	

	

	public PdetailDTO(String pdid, String pid, int stock, String color, String psize, int addprice) {
		super();
		this.pdid = pdid;
		this.pid = pid;
		this.stock = stock;
		this.color = color;
		this.psize = psize;
		this.addprice = addprice;
	}





	public String getPdid() {
		return pdid;
	}

	public void setPdid(String pdid) {
		this.pdid = pdid;
	}

	public String getPid() {
		return pid;
	}

	public void setPid(String pid) {
		this.pid = pid;
	}

	public int getStock() {
		return stock;
	}

	public void setStock(int stock) {
		this.stock = stock;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getPsize() {
		return psize;
	}

	public void setPsize(String psize) {
		this.psize = psize;
	}

	public int getAddprice() {
		return addprice;
	}

	public void setAddprice(int addprice) {
		this.addprice = addprice;
	}
	
	
	
	
	
	

}
