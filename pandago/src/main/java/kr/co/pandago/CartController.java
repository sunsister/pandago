package kr.co.pandago;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import kr.co.pandago.dto.CartDTO;
import kr.co.pandago.service.CartService;

@Controller
public class CartController {

	@Resource(name="cartService")
	private CartService cs;
	
	@RequestMapping(value = "/cartin.do")
	public ModelAndView cartin(HttpServletRequest req, @RequestParam("pid")String pid,@RequestParam("pdid")String pdid, @RequestParam("scnt")int scnt,HttpSession session) {
		if (session.getAttribute("cid")!=null) {
			
		CartDTO dto = new CartDTO();
		 dto.setCid((String)req.getSession(true).getAttribute("cid"));
		 dto.setPid(pid);
		 dto.setPdid(pdid);
		 dto.setCnt(scnt);
		 dto.setCid((String)session.getAttribute("cid"));
		
		 cs.writeOne(dto);
		}
		return new ModelAndView("jsonView", "id", (String)session.getAttribute("cid"));
	}
	
	@RequestMapping(value = "/cart.do")
	public ModelAndView cart(HttpServletRequest req, HttpSession session) {
		System.out.println(session.getAttribute("cid"));
		if (session.getAttribute("cid") == null) {
			return new ModelAndView("login");
		}
		return new ModelAndView("cart", "list", cs.creadAll((String)session.getAttribute("cid")));
		
	}
	
	@RequestMapping(value = "/cartdelete.do")
	public String cartdelete(HttpServletRequest req,HttpSession session) {
		System.out.println("adsdsadasdasd");
		String[] deleteno = req.getParameterValues("deleteno");
		String [] no= deleteno[0].split(",");
	
		for (int j = 0; j < no.length; j++) {
			System.out.println(no[j]);
			cs.removeOne(no[j]);
		}
		return "redirect:/cart.do";
	}


}
