package kr.co.pandago.dao;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import kr.co.pandago.dto.PayDetailDTO;
import kr.co.pandago.dto.PaymentDTO;

@Repository("paymentDAO")
public class PaymentDAO implements DAO<PaymentDTO> {
	
	@Autowired
	private SqlSession ss;

	@Override
	public List<PaymentDTO> selectAll(int startNo, int endNo) {
		return null;
	}

	@Override
	public PaymentDTO selectOne(String payid) {
		return ss.selectOne("kr.co.pandago.payment.selectOne", payid);
	}

	@Override
	public void insertOne(PaymentDTO t) {
		ss.insert("kr.co.pandago.payment.insertOne", t);
	}

	@Override
	public void updateOne(PaymentDTO t) {
		ss.update("kr.co.pandago.payment.updateOne", t);
	}
	
	@Override
	public void deleteOne(String id) {
		ss.delete("kr.co.pandago.payment.delteOne",id);
	}
	

	public List<PaymentDTO> selectAll(String cid){
		return ss.selectList("kr.co.pandago.payment.selectAll", cid);
	}

	public String getPayId(String cid){
		return ss.selectOne("kr.co.pandago.payment.getPayId", cid);
	}
	
	public List<PaymentDTO> selectDetail(HashMap<String, String> hm){
		return ss.selectList("kr.co.pandago.payment.selectDetail",hm);
	}
	
}
