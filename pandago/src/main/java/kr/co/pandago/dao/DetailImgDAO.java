package kr.co.pandago.dao;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import kr.co.pandago.dto.DetailImgDTO;

@Repository("detailImgDAO")
public class DetailImgDAO implements DAO<DetailImgDTO>{

	@Autowired
	private SqlSession ss;
	
	public void setSs(SqlSession ss) {
		this.ss = ss;
	}
	
	@Override
	public List<DetailImgDTO> selectAll(int startNo, int endNo) {
		return null;
	}

	@Override
	public DetailImgDTO selectOne(String id) {
		return ss.selectOne("kr.co.pandago.detailimg.selectOne", id);
	}

	@Override
	public void insertOne(DetailImgDTO t) {
		ss.insert("kr.co.pandago.detailimg.insertOne", t);
	}

	@Override
	public void updateOne(DetailImgDTO t) {
		ss.update("kr.co.pandago.detailimg.updateOne", t);
	}

	@Override
	public void deleteOne(String id) {
		ss.update("kr.co.pandago.detailimg.updateOne", id);
	}
	
	public List<DetailImgDTO> selectAllimg(String no) {
		return ss.selectList("kr.co.pandago.detailimg.selectAll", no);
	}

	
}
