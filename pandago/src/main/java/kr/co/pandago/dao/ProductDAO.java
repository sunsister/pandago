package kr.co.pandago.dao;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import kr.co.pandago.dto.CodeDTO;
import kr.co.pandago.dto.ProductDTO;
import kr.co.pandago.tool.PagingStartEnd;

@Repository("productDAO")
public class ProductDAO implements DAO<ProductDTO>{

	@Autowired
	private SqlSession ss;
	
	
	
	@Override
	public List<ProductDTO> selectAll(int startNo, int endNo) {
	/*	PagingStartEnd se = new PagingStartEnd();
		se.setStartNo(startNo);
		se.setEndNo(endNo);*/
		ProductDTO dto = new ProductDTO();
		dto.setEndNo(endNo);
		dto.setStartNo(startNo);
		
		return ss.selectList("kr.co.pandago.product.selectAll", dto);
	}

	@Override
	public ProductDTO selectOne(String id) {
		
		return ss.selectOne("kr.co.pandago.product.selectOne", id);
	}

	@Override
	public void insertOne(ProductDTO t) {
		ss.insert("kr.co.pandago.product.insertOne", t);
	}

	@Override
	public void updateOne(ProductDTO t) {
		ss.update("kr.co.pandago.product.updateOne", t);
	}

	@Override
	public void deleteOne(String id) {
		ss.delete("kr.co.pandago.product.deleteOne", id);
	}
	
	public List<ProductDTO> selectList(ProductDTO t) {
		
		return ss.selectList("kr.co.pandago.product.selectAll", t);
		
	}
	
	public int Total(ProductDTO t) {
		
		
		return ss.selectOne("kr.co.pandago.product.selectTotal", t);
	}
	public List<CodeDTO> selectCode1(String lcode) {
		
		
		return ss.selectList("kr.co.pandago.product.selectCode1",lcode);
	}
	public List<CodeDTO> selectCode2(String lcode) {
		
		
		return ss.selectList("kr.co.pandago.product.selectCode2",lcode);
	}

	public List<ProductDTO> selectmain(ProductDTO dto) {
		
		
		return ss.selectList("kr.co.pandago.product.selectmain",dto);
	}

	
	public List<ProductDTO> selectSearch(ProductDTO dto) {
		
		
		return ss.selectList("kr.co.pandago.product.selectSearch",dto);
	}
	public int searchTotal(ProductDTO dto) {
		
		
		return ss.selectOne("kr.co.pandago.product.searchTotal",dto);
	}
	public String selectmainimg(String id) {
		
		
		return ss.selectOne("kr.co.pandago.product.selectmainimg",id);
	}

	public void starupdate(ProductDTO dto) {
		
		 ss.update("kr.co.pandago.product.starupdate",dto);
	}
	
	
}
