package kr.co.pandago.dao;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import kr.co.pandago.dto.PdetailDTO;
import kr.co.pandago.tool.PagingStartEnd;

@Repository("pdetailDAO")
public class PdetailDAO implements DAO<PdetailDTO>{

	@Autowired
	private SqlSession ss;
	
	public void setSs(SqlSession ss) {
		this.ss = ss;
	}
	
	@Override
	public List<PdetailDTO> selectAll(int startNo, int endNo) {
		PagingStartEnd se = new PagingStartEnd();
		se.setStartNo(startNo);
		se.setEndNo(endNo);
		return ss.selectList("kr.co.pandago.pdetail.selectAll", se);
	}


	@Override
	public PdetailDTO selectOne(String id) {
		return ss.selectOne("kr.co.pandago.pdetail.selectOne", id);
	}
	

	@Override
	public void insertOne(PdetailDTO t) {
		ss.insert("kr.co.pandago.pdetail.insertOne", t);
	}

	@Override
	public void updateOne(PdetailDTO t) {
		ss.update("kr.co.pandago.pdetail.updateOne", t);
	}	

	@Override
	public void deleteOne(String id) {
		ss.delete("kr.co.pandago.pdetail.deleteOne", id);
	}

	public List<PdetailDTO> selectopAll(String no) {
		
		return ss.selectList("kr.co.pandago.pdetail.selectAll", no);
	}


}
