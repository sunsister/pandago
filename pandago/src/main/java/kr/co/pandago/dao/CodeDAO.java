package kr.co.pandago.dao;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import kr.co.pandago.dto.CodeDTO;

@Repository("codeDAO")
public class CodeDAO {
	@Autowired
	private SqlSession ss;
	
	public List<CodeDTO> selectLevel(HashMap<String, Object> hm){
		return ss.selectList("kr.co.pandago.code.selectLevel", hm);
	}
}
