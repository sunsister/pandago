package kr.co.pandago.dao;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import kr.co.pandago.dto.ReviewDTO;
import kr.co.pandago.tool.PagingStartEnd;
@Repository("reviewDAO")
public class ReviewDAO implements DAO<ReviewDTO>{
	@Autowired
	private SqlSession ss;
	
	public void setSs(SqlSession ss){
		this.ss=ss;
	}
	
	
	@Override
	public List<ReviewDTO> selectAll(int startNo, int endNo) {
		PagingStartEnd pse= new PagingStartEnd();
		pse.setStartNo(startNo);
		pse.setEndNo(endNo);
		return ss.selectList("kr.co.pandago.review.selectAll",pse);
	}

	@Override
	public ReviewDTO selectOne(String id) {
		// TODO Auto-generated method stub
		return ss.selectOne("kr.co.pandago.review.selectAll",id);
	}

	@Override
	public void insertOne(ReviewDTO t) {
		ss.insert("kr.co.pandago.review.insertOne",t);
		
	}

	@Override
	public void updateOne(ReviewDTO t) {
		ss.update("kr.co.pandago.review.updateOne",t);
		
	}

	@Override
	public void deleteOne(String id) {
		ss.update("kr.co.pandago.review.delteOne",id);
		
	}
	public List<ReviewDTO> selectAllList(String no) {
		return ss.selectList("kr.co.pandago.review.selectAllList",no);
	}
	
	public List<ReviewDTO> selectreview(String no) {
		return ss.selectList("kr.co.pandago.review.selectreview",no);
	}
	public List<String> selectpayid() {
		return ss.selectList("kr.co.pandago.review.selectPayid");
	}
	public List<String> selectMyreview(String id) {
		return ss.selectList("kr.co.pandago.review.selectMyreview",id);
	}
	

}
