package kr.co.pandago.dao;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import kr.co.pandago.dto.CustomerDTO;
import kr.co.pandago.tool.PagingStartEnd;

@Repository("customerDAO")
public class CustomerDAO implements DAO<CustomerDTO>{
	
	@Autowired
	private SqlSession ss;
	
	public void setSs(SqlSession ss) {
		this.ss = ss;
	}

	@Override
	public List<CustomerDTO> selectAll(int startNo, int endNo) {
		PagingStartEnd pse = new PagingStartEnd();
		pse.setStartNo(startNo);
		pse.setEndNo(endNo);
		
		return ss.selectList("kr.co.pandago.customer.selectAll", pse);
	}
	
	@Override
	public CustomerDTO selectOne(String id) {
		return ss.selectOne("kr.co.pandago.customer.selectOne", id);
	}

	@Override
	public void insertOne(CustomerDTO t) {
		System.out.println("DAO");
		System.out.println("ss : " + ss);
		System.out.println("t > " + t);
		try{
			ss.insert("kr.co.pandago.customer.insertOne", t);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	@Override
	public void updateOne(CustomerDTO t) {
		
		ss.update("kr.co.pandago.customer.updateOne", t);
		
	}

	@Override
	public void deleteOne(String id) {
		ss.delete("kr.co.pandago.customer.deleteOne", id);
	}
	
	public String validateEmail(String data) {
		
		
		return ss.selectOne("kr.co.pandago.customer.valiDate",data);
	}
	
	public CustomerDTO loginOk(HashMap<String, String> hm) {
		return ss.selectOne("kr.co.pandago.customer.logIn", hm);
	}

	public void modifyPw(HashMap<String, String> hm){
		ss.update("kr.co.pandago.customer.updatePw", hm);
	}
	
	public void modifyPoint(HashMap<String, Object> hm){
		ss.update("kr.co.pandago.customer.updatePoint", hm);
	}
	
	public void raisePoint(HashMap<String, Object> hm){
		ss.update("kr.co.pandago.customer.raisePoint", hm);
	}
}
