package kr.co.pandago.dao;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import kr.co.pandago.dto.MainImgDTO;

@Repository("mainImgDAO")
public class MainImgDAO implements DAO<MainImgDTO>{
	
	@Autowired
	private SqlSession ss;
	
	public void setSs(SqlSession ss) {
		this.ss = ss;
	}
	
	@Override
	public List<MainImgDTO> selectAll(int startNo, int endNo) {
		return selectAll();
	}

	@Override
	public MainImgDTO selectOne(String id) {
		return ss.selectOne("kr.co.pandago.mainimg.selectOne", id);
	}

	@Override
	public void insertOne(MainImgDTO t) {
		ss.insert("kr.co.pandago.mainimg.insertOne", t);
	}

	@Override
	public void updateOne(MainImgDTO t) {
		ss.update("kr.co.pandago.mainimg.updateOne", t);
	}

	@Override
	public void deleteOne(String id) {
		ss.update("kr.co.pandago.mainimg.updateOne", id);
	}
	
	public List<MainImgDTO> selectAll() {
		return ss.selectList("kr.co.pandago.detailimg.selectAll");
	}
	public List<MainImgDTO> selectOneimg(String id) {
		
		return ss.selectList("kr.co.pandago.mainimg.selectOne",id);
	}
	
}
