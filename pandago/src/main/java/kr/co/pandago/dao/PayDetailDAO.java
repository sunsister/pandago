package kr.co.pandago.dao;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import kr.co.pandago.dto.PayDetailDTO;
import kr.co.pandago.dto.PdetailDTO;

@Repository("paydetailDAO")
public class PayDetailDAO implements DAO<PayDetailDTO>{
	@Autowired
	private SqlSession ss;
	
	@Override
	public List selectAll(int startNo, int endNo) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public void deleteOne(String id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public PayDetailDTO selectOne(String id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void insertOne(PayDetailDTO t) {
		ss.insert("kr.co.pandago.paydetail.insertOne", t);
	}

	@Override
	public void updateOne(PayDetailDTO t) {
		// TODO Auto-generated method stub
		
	}
	
	public List<PayDetailDTO> selectAll(String cid){
		return ss.selectList("kr.co.pandago.paydetail.selectAll",cid);
	}
	
	public List<PayDetailDTO> selectPurchase(String payid){
		return ss.selectList("kr.co.pandago.paydetail.selectPurchase",payid);
	}
	

	
	
	

}
