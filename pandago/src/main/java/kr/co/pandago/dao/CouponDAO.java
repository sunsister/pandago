package kr.co.pandago.dao;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import kr.co.pandago.dto.CouponDTO;
import kr.co.pandago.tool.PagingStartEnd;
@Repository("couponDAO")
public class CouponDAO implements DAO<CouponDTO>  {
	@Autowired
	private SqlSession ss;
	
	@Override
	public List<CouponDTO> selectAll(int startNo, int endNo) {
		return null;
	}

	@Override
	public CouponDTO selectOne(String id) {
		
		return ss.selectOne("kr.co.pandago.coupon.selectOne",id);
	}

	@Override
	public void insertOne(CouponDTO t) {
		ss.insert("kr.co.pandago.coupon.insertOne",t);
		
	}

	@Override
	public void updateOne(CouponDTO t) {
		ss.update("kr.co.pandago.coupon.updateOne",t);
	}

	@Override
	public void deleteOne(String id) {
		ss.delete("kr.co.pandago.coupon.selectOne",id);
		
	}
	
	public List<CouponDTO> selectAll(){
		return ss.selectList("kr.co.pandago.coupon.selectAll");
	}

}
