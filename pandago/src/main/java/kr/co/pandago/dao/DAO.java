package kr.co.pandago.dao;

import java.util.List;

public interface DAO<T> {

	public List<T> selectAll(int startNo, int endNo);
	public T selectOne(String id);
	public void insertOne(T t);
	public void updateOne(T t);
	public void deleteOne(String id);
}
