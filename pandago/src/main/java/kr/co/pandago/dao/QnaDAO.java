package kr.co.pandago.dao;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import kr.co.pandago.dto.QnaDTO;
import kr.co.pandago.tool.PagingStartEnd;

@Repository("qnaDAO")
public class QnaDAO implements DAO<QnaDTO> {

	@Autowired
	private SqlSession ss;
	
	@Override
	public List<QnaDTO> selectAll(int startNo, int endNo) {
		PagingStartEnd pse = new PagingStartEnd();
		pse.setEndNo(endNo);
		pse.setStartNo(startNo);
		return ss.selectList("kr.co.pandago.qna.selectAll", pse);
	}
	
	@Override
	public QnaDTO selectOne(String id) {
		return ss.selectOne("kr.co.pandago.qna.selectOne", id);
	}

	@Override
	public void insertOne(QnaDTO t) {
		ss.selectOne("kr.co.pandago.qna.insertOne", t);
	}

	@Override
	public void updateOne(QnaDTO t) {
		ss.update("kr.co.pandago.qna.updateOne", t);
		
	}

	@Override
	public void deleteOne(String id) {
		ss.delete("kr.co.pandago.qna.deleteOne", id);
	}
	
	public void updateAcontent(QnaDTO t) {
		ss.update("kr.co.pandago.qna.updateAcontent", t);
		
	}
	
	public List<QnaDTO> selectAll(HashMap<String, Object> hm) {	
		return ss.selectList("kr.co.pandago.qna.selectAll", hm);
	}

	public int getTotal(){
		return ss.selectOne("kr.co.pandago.qna.getTotal");
	}
	
	public void askQna(HashMap<String, String> hm){
		ss.update("kr.co.pandago.qna.askQna", hm);
	}
	
	public List<QnaDTO> selectAllAdmin(HashMap<String, Object> hm) {
		return ss.selectList("kr.co.pandago.qna.adminselectAll", hm);
	}
}
