package kr.co.pandago.dao;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import kr.co.pandago.dto.CartDTO;

@Repository("cartDAO")
public class CartDAO implements DAO<CartDTO>{
	
	
	@Autowired
	private SqlSession ss;

	@Override
	public List<CartDTO> selectAll(int startNo, int endNo) {
		return null;
	}
	
	public List<CartDTO> selectCart(String id) {
		return ss.selectList("kr.co.pandago.cart.selectAll", id);
	}

	@Override
	public CartDTO selectOne(String id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void insertOne(CartDTO t) {
		ss.insert("kr.co.pandago.cart.insertOne", t);
		
	}

	@Override
	public void updateOne(CartDTO t) {
		ss.update("kr.co.pandago.cart.updateOne", t);
	}

	@Override
	public void deleteOne(String id) {
		ss.delete("kr.co.pandago.cart.deleteOne", id);
		
	}

}
