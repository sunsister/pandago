package kr.co.pandago.dao;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import kr.co.pandago.dto.InquiryDTO;
import kr.co.pandago.tool.PagingStartEnd;

@Repository("inquiryDAO")
public class InquiryDAO implements DAO<InquiryDTO> {
	@Autowired
	private SqlSession ss;

	@Override
	public List<InquiryDTO> selectAll(int startNo, int endNo) {
		PagingStartEnd pse = new PagingStartEnd();
		pse.setStartNo(startNo);
		pse.setEndNo(endNo);
		return ss.selectList("kr.co.pandago.inquiry.selectAll", pse);
	}

	@Override
	public InquiryDTO selectOne(String id) {
		return ss.selectOne("kr.co.pandago.inquiry.selectOne", id);
	}

	@Override
	public void insertOne(InquiryDTO t) {
		ss.insert("kr.co.pandago.inquiry.insertOne", t);
	}

	@Override
	public void updateOne(InquiryDTO t) {
		ss.update("kr.co.pandago.inquiry.updateOne", t);
	}

	@Override
	public void deleteOne(String id) {
		ss.delete("kr.co.pandago.inquiry.deleteOne", id);
	}

	public List<InquiryDTO> selectList(InquiryDTO t) {
		return ss.selectList("kr.co.pandago.inquiry.selectListAll", t);
	}
	
	public void askInquiry(HashMap<String, String> hm){
		ss.update("kr.co.pandago.inquiry.askInquiry", hm);
	}
	
	public int getTotal(){
		return ss.selectOne("kr.co.pandago.inquiry.getTotal");
	}
}
