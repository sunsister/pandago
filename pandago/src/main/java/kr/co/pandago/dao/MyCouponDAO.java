package kr.co.pandago.dao;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import kr.co.pandago.dto.MyCouponDTO;
@Repository("mycouponDAO")
public class MyCouponDAO implements DAO<MyCouponDTO>{
	
	@Autowired
	private SqlSession ss;
	
	@Override
	public List<MyCouponDTO> selectAll(int startNo, int endNo) {
		return null;
	}

	@Override
	public MyCouponDTO selectOne(String id) {
		return null;
	}

	@Override
	public void insertOne(MyCouponDTO t) {
		//ss.insert("kr.co.pandago.mycoupon.insertOne", t);
	}

	@Override
	public void updateOne(MyCouponDTO t) {	
	}

	@Override
	public void deleteOne(String id) {
		ss.delete("kr.co.pandago.mycoupon.deleteOne",id);
	}
	
	public List<MyCouponDTO> selectAll(String cid){
		return ss.selectList("kr.co.pandago.mycoupon.selectAll", cid);
	}
	
	public MyCouponDTO selectOne(HashMap<String, String> hm){
		return ss.selectOne("kr.co.pandago.mycoupon.selectOne", hm);
	}
	
	public void updateOne(String mcid){
		ss.update("kr.co.pandago.mycoupon.updateOne",mcid);
	}
	
	public List<MyCouponDTO> detailAll(String cid){
		return ss.selectList("kr.co.pandago.mycoupon.detailAll", cid);
	}
	
	public List<MyCouponDTO> detailUsed(HashMap<String, Object> hm){
		return ss.selectList("kr.co.pandago.mycoupon.detailUsed", hm);
	}
	
	public void insertOne(HashMap<String, String> hm) {
		ss.insert("kr.co.pandago.mycoupon.insertOne", hm);
	}
}
