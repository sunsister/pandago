package kr.co.pandago.admin;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class AMainController {
	
	@RequestMapping("/main.ad")
	public String main(){
		return "/manager/admin_main";
	}
	
	@RequestMapping("/login.ad")
	public String login(){
		return "/manager/admin_login";
	}
	
	@RequestMapping("/loginOK.ad")
	public String loginOK(HttpServletRequest req, @RequestParam("id") String id, @RequestParam("pw") String pw){
		HttpSession session = req.getSession();
		if(id.equals("panda") && pw.equals("1234")){
			session.setAttribute("adlogin", true);
			return "/manager/admin_main";
		}
		return "/manager/admin_login";
	}
	
}
