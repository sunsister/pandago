package kr.co.pandago.admin;

import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import kr.co.pandago.dto.CodeDTO;
import kr.co.pandago.dto.PdetailDTO;
import kr.co.pandago.dto.ProductDTO;
import kr.co.pandago.service.CodeService;
import kr.co.pandago.service.PdetailService;
import kr.co.pandago.service.ProductService;

@Controller
@RequestMapping("/product.ad")
public class AProductController {
	
	@Resource(name="productService")
	ProductService service;
	
	@Resource(name="pdetailService")
	PdetailService pdservice;
	
	@Resource(name="codeService")
	CodeService codeservice;
	
	@RequestMapping()
	public ModelAndView list(@RequestParam(value="cp", required=false, defaultValue="1") int cp){
		// 총 게시물 수
		int totalCount = service.total(new ProductDTO());
		
		// 한 페이지당 게시물 수
		int countPerPage = 10;
		
		// 총 페이지 수
		int totalPage = (totalCount%countPerPage == 0) ? (totalCount/countPerPage) : (totalCount/countPerPage+1);
		
		// 현재 페이지
		int currentPage = cp;
		
		// 현재 페이지 시작번호
		int startNo = (currentPage -1)*countPerPage + 1;
		
		// 현재 페이지 끝번호
		int endNo = currentPage*countPerPage;
		
		// 페이지 시작 번호
		int beginPageNum = (currentPage < 3)? 1 : currentPage - 2;
		
		// 페이지 끝 번호
		int endPageNum = (currentPage < 3) ? 5 : currentPage + 2;
		
		if(endPageNum > totalPage){
			endPageNum = totalPage;
		}
		
		HashMap<String, Object> hm = new HashMap<String, Object>();
		hm.put("clevel", 1);
		List<CodeDTO> codelist = codeservice.readLevel(hm);
		 
		List<ProductDTO> productlist = service.readAll(startNo, endNo);

		ModelAndView mav = new ModelAndView("/manager/product/product_list");
		mav.addObject("list", productlist);
		mav.addObject("currentPage", currentPage);
		mav.addObject("beginPageNum", beginPageNum);
		mav.addObject("endPageNum", endPageNum);
		mav.addObject("cp", currentPage);
		mav.addObject("codelist", codelist);
		return mav;
	}
	
	@ResponseBody
	@RequestMapping(params="action=detail")
	public List<PdetailDTO> detail(@RequestParam("pid") String pid){
		
		return pdservice.readAllop(pid);
	}
	
	@ResponseBody
	@RequestMapping(params="action=modify")
	public String modify(@RequestParam("pid") String pid, @RequestParam(name="pname", required=false) String pname,
			@RequestParam(name="price", required=false) int price, @RequestParam(name="sprice", required=false) int sprice, 
			@RequestParam(name="sale", required=false) int sale, @RequestParam(name="factory", required=false) String factory){
		try{
			ProductDTO dto = service.readOne(pid);
			if(pname != null)
				dto.setPname(pname);
			
			dto.setPrice(price);
			dto.setSprice(sprice);
			dto.setSale(sale);
			
			if(factory != null)
				dto.setFactory(factory);
			
			service.modifyOne(dto);
		}catch(Exception e){
			return "fail";
		}
		
		return "success";
	}
}
