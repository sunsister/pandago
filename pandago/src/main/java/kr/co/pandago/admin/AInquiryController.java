package kr.co.pandago.admin;

import java.util.HashMap;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import kr.co.pandago.dto.InquiryDTO;
import kr.co.pandago.service.InquiryService;
import kr.co.pandago.service.ProductService;

@Controller
@RequestMapping("/inquiry.ad")
public class AInquiryController {
	
	@Resource(name="inquiryService")
	InquiryService service;
	
	@Resource(name="productService")
	ProductService pservice;
	
	
	@RequestMapping()
	public ModelAndView list(@RequestParam(value="cp", required=false, defaultValue="1") int cp){
		
		// 총 게시물 수
		int totalCount = service.getTotal();
		
		// 한 페이지당 게시물 수
		int countPerPage = 10;
		
		// 총 페이지 수
		int totalPage = (totalCount%countPerPage == 0) ? (totalCount/countPerPage) : (totalCount/countPerPage+1);
		
		// 현재 페이지
		int currentPage = cp;
		
		// 현재 페이지 시작번호
		int startNo = (currentPage -1)*countPerPage + 1;
		
		// 현재 페이지 끝번호
		int endNo = currentPage*countPerPage;
		
		// 페이지 시작 번호
		int beginPageNum = (currentPage < 3)? 1 : currentPage - 2;
		
		// 페이지 끝 번호
		int endPageNum = (currentPage < 3) ? 5 : currentPage + 2;
		
		if(endPageNum > totalPage){
			endPageNum = totalPage;
		}
		
		
		ModelAndView mav = new ModelAndView("/manager/inquiry/inquiry_list");
		mav.addObject("list", service.readAll(startNo, endNo));
		mav.addObject("currentPage", currentPage);
		mav.addObject("beginPageNum", beginPageNum);
		mav.addObject("endPageNum", endPageNum);
		mav.addObject("cp", currentPage);
		return mav;
	}
	
	@RequestMapping(params="action=inlist")
	public ModelAndView ask(@RequestParam("id") String inid){
		ModelAndView mav = new ModelAndView("/manager/inquiry/inquiry_ask");
		InquiryDTO idto = service.readOne(inid);
		mav.addObject("idto", idto);
		mav.addObject("product", pservice.readOne(idto.getPid()));
		return mav;
	}
	
	@RequestMapping(params="action=editask")
	public String editAsk(@RequestParam("inid") String inid ,@RequestParam("inacontent") String inacontent){
		HashMap<String, String> hm = new HashMap<String, String>();
		hm.put("inid", inid);
		hm.put("inacontent", inacontent);
		service.askInquiry(hm);

		return "redirect:/inquiry.ad?action=inlist&id="+inid;
	}
}
