package kr.co.pandago.admin;

import java.util.HashMap;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import kr.co.pandago.dto.QnaDTO;
import kr.co.pandago.service.QnaService;

@Controller
@RequestMapping("/qna.ad")
public class AQnaController {
	
	@Resource(name="qnaService")
	QnaService service;
	
	@RequestMapping()
	public ModelAndView list(@RequestParam(value="cp", required=false, defaultValue="1") int cp,
			@RequestParam(value="qcode", required=false) String qcode){
		
		
		System.out.println("qcode " + qcode);
		// 총 게시물 수
		int totalCount = service.getTotal();
		
		// 한 페이지당 게시물 수
		int countPerPage = 10;
		
		// 총 페이지 수
		int totalPage = (totalCount%countPerPage == 0) ? (totalCount/countPerPage) : (totalCount/countPerPage+1);
		
		// 현재 페이지
		int currentPage = cp;
		
		// 현재 페이지 시작번호
		int startNo = (currentPage -1)*countPerPage + 1;
		
		// 현재 페이지 끝번호
		int endNo = currentPage*countPerPage;
		
		// 페이지 시작 번호
		int beginPageNum = (currentPage < 3)? 1 : currentPage - 2;
		
		// 페이지 끝 번호
		int endPageNum = (currentPage < 3) ? 5 : currentPage + 2;
		
		if(endPageNum > totalPage){
			endPageNum = totalPage;
		}
		
		HashMap<String, Object> hm = new HashMap<String, Object>();
		hm.put("startNo", startNo);
		hm.put("endNo", endNo);
		if(qcode != null && !qcode.equals("")){
			hm.put("qcode", qcode);
		}
		
		ModelAndView mav = new ModelAndView("/manager/qna/qna_list");
		mav.addObject("list", service.selectAllAdmin(hm));
		mav.addObject("currentPage", currentPage);
		mav.addObject("beginPageNum", beginPageNum);
		mav.addObject("endPageNum", endPageNum);
		mav.addObject("cp", currentPage);
		return mav;
	}
	
	@RequestMapping(params="action=qadetail")
	public ModelAndView ask(@RequestParam("id") String qnaid){
		ModelAndView mav = new ModelAndView("/manager/qna/qna_detail");
		QnaDTO dto = service.readOne(qnaid);
		mav.addObject("dto", dto);
		return mav;
	}
	
	@RequestMapping(params="action=qnaAsk")
	public String editAsk(@RequestParam("qnaid") String qnaid ,@RequestParam("acontent") String acontent){
		HashMap<String, String> hm = new HashMap<String, String>();
		hm.put("qnaid", qnaid);
		hm.put("acontent", acontent);
		service.askQna(hm);

		return "redirect:/qna.ad?action=qadatail&id="+qnaid;
	}
}
